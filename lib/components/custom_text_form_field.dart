import 'package:flutter/material.dart';

class CustomTextFormFiled extends StatelessWidget {
  final String? Function(String? value)? validator;
  String? labelText;
  Color? color;
  Color? labelColor;
  final InputBorder? border;
  final Widget? preIcon;
  final Widget? suffixIcon;
  final TextInputType? keyType;
  final TextEditingController? controller;
  final bool obscureText;
  void Function()? onTap;
  int? maxLines;
   bool readOnly;
   double? divider;
  CustomTextFormFiled(
      {super.key, this.validator,
      this.labelText,
      this.border,
      this.preIcon,
      this.suffixIcon,
      this.keyType,
      this.controller,
      required this.obscureText,
      this.onTap,
      this.maxLines,
      this.readOnly=false,
      this.divider=2.5,
        this.color=Colors.orange,
        this.labelColor=Colors.orange

      });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width / divider!,
      child: TextFormField(style: const TextStyle(color: Colors.orange),readOnly: readOnly,
        maxLines: maxLines,
        obscureText: obscureText,
        onTap: onTap,
        validator: validator,
        controller: controller,
        cursorColor: Colors.orange,
        decoration: InputDecoration(
          prefixIcon: preIcon,
          suffixIcon: suffixIcon,
          fillColor: Colors.white,
          filled: true,
          alignLabelWithHint: false,
          labelText: labelText,
          labelStyle:  TextStyle(color: labelColor),

          border: OutlineInputBorder(
            borderSide: const BorderSide(style: BorderStyle.solid,color: Colors.orange, width: 2),
            borderRadius: BorderRadius.circular(10),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide:   BorderSide(color: color!, width: 2),
            borderRadius: BorderRadius.circular(10),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:  BorderSide(color: color!, width: 2),
            borderRadius: BorderRadius.circular(10),
          ),),


      ),
    );
  }
}
