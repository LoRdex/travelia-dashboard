import 'package:flutter/material.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';

class Dialogs {
  static void showFailedDialog(BuildContext context,String content) {
    showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(
            content: SizedBox(
              height: MediaQuery
                  .of(context)
                  .size
                  .height / 6,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                     Text(
                      content,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text(
                        'Close',
                        style: TextStyle(
                            fontSize: 15,
                            color: MyColors
                                .blue), //
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
    );
  }



  static void showCustomSnackbar(BuildContext context,String title,String content,int sec) {
    final snackbar = SnackBar(backgroundColor:MyColors.scaffoldColor,
      content: Column(crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title,style: const TextStyle(color: MyColors.blue),),
          Text(content,style: const TextStyle(color: MyColors.blue),)
        ],
      ),
      duration:  Duration(seconds: sec),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackbar);
  }

  static void showsSuccessDialog(BuildContext context,String message) {
    showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(
            content: SizedBox(
              height: MediaQuery
                  .of(context)
                  .size
                  .height / 6,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                     Text(
                      message,
                      textAlign: TextAlign.center,
                      style: const TextStyle(fontSize: 20,color: MyColors.blue),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text(
                        'Close',
                        style: TextStyle(
                            fontSize: 15,
                            color: MyColors
                                .blue), //
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
    );
  }


}