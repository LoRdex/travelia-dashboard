import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';

class CustomButton extends StatelessWidget {
  void Function() onTap;
  String text;
  double? dividerW;
  double? dividerH;
  double? fontSize;
  Color? color;

  CustomButton({super.key, required this.text ,required this.onTap,this.dividerH=15,this.dividerW=9,this.fontSize=75,this.color=MyColors.blue});

  @override
  Widget build(BuildContext context) {
    return InkWell(onTap: onTap,splashColor: Colors.blueGrey.shade50,
      child: Container(
        height: MediaQuery.of(context).size.height/dividerH!,
        width: MediaQuery.of(context).size.width/dividerW!,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: color,
        ),
        child: Center(child: Text(text,style:  TextStyle(fontSize: MediaQuery.of(context).size.width/fontSize!,color: Colors.white),)),
      ),
    );
  }
}