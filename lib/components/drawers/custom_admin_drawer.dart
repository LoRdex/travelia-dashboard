import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/hotel_cubit/hotel_cubit.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/views/admin/create_tourist_area.dart';
import 'package:travelia_dashboard/views/admin/financial_request.dart';
import 'package:travelia_dashboard/views/admin/send_notification.dart';
import 'package:travelia_dashboard/views/admin/support_messages.dart';

import 'package:travelia_dashboard/views/trip%20orgnaizer/discover_area.dart';


import '../../cubits/admin_cubit/admin_cubit.dart';
import '../../cubits/main_cubit/main_cubit.dart';


class CustomAdminDrawer extends StatelessWidget {
  const CustomAdminDrawer({
    super.key,
  });


  @override
  Widget build(BuildContext context) {
    final HotelCubit hotelCubit =
    BlocProvider.of<HotelCubit>(context);
    final mainCubit = BlocProvider.of<MainCubit>(context);
    final adminCubit = BlocProvider.of<AdminCubit>(context);
    final tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);
    final screenWidth = MediaQuery.of(context).size.width;

    return Drawer(
      backgroundColor: MyColors.scaffoldColor,
      child: BlocBuilder<TripOrganizerCubit, TripOrganizerState>(
        builder: (context, state) => ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Image.asset('images/admin.png'),
            ),
            const SizedBox(height: 20),
            _buildDrawerItem(
              context,
              icon: Icons.map,
              title: 'Create Area',
              screen: const CreateTouristArea(),
              // Replace with your screen
              routeName: '/create_area',
              screenWidth: screenWidth,
            ),
            const SizedBox(height: 20),
            _buildDrawerItem(
              context,
              icon: Icons.message,
              title: 'Support Messages',
              screen: const SupportMessages(),
              routeName: '/support_messages',
              screenWidth: screenWidth,
              onTap: ()async{
                await adminCubit.getMessages();

              }
            ),

            const SizedBox(height: 20),
            _buildDrawerItem(
              context,
              icon: Icons.notification_add,
              title: 'Send Notification',
              screen: const SendNotification(),
              routeName: '/notification_send',
              screenWidth: screenWidth,
            ),
            const SizedBox(height: 20),
            _buildDrawerItem(
              context,
              icon: Icons.attach_money,
              title: 'Financial Requests ',
              screen: const FinancialRequests(),
              routeName: '/financial_requests',
              screenWidth: screenWidth,
              onTap: ()async{
                adminCubit.getFinancialRequests();
              }
            ),
            const SizedBox(height: 20),
            _buildDrawerItem(
              context,
              icon: Icons.travel_explore,
              title: 'Discover Area',
              screen: const DiscoverAreaScreen(),
              routeName: '/discover_area',
              screenWidth: screenWidth,
              onTap: ()async{
                await tripOrganizerCubit.getDiscoverTouristAreas();
              }
            ),
            const SizedBox(height: 20),
            ListTile(
              leading: Icon(Icons.logout_sharp,
                  color: MyColors.blue, size: screenWidth * 0.017),
              title: Text(
                'Sign Out',
                style: TextStyle(
                    color: MyColors.blue, fontSize: screenWidth * 0.01),
              ),
              onTap: () {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text(
                        'Are You Sure You Want to Sign Out ?',
                        style: TextStyle(
                            color: MyColors.blue,
                            fontSize: screenWidth * 0.012),
                      ),
                      content: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          TextButton(onPressed: (){Navigator.pop(context);}, child: const Text('Continue',style: TextStyle(color: Colors.black),)),
                          TextButton(onPressed: ()async{
                            await mainCubit.logout(context);


                          }, child: const Text('Yes',style: TextStyle(color: Colors.black),)),
                        ],
                      ),
                    );
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDrawerItem(BuildContext context,
      {required IconData icon,
        required String title,
        required Widget screen,
        required String routeName,
        required double screenWidth,
        Future<void> Function()? onTap}) {
    final bool isSelected = ModalRoute.of(context)?.settings.name == routeName;

    return ListTile(
      leading: Icon(icon,
          color: isSelected ? Colors.grey : MyColors.blue,
          size: screenWidth * 0.017),
      title: Text(
        title,
        style: TextStyle(
            color: isSelected ? Colors.grey : MyColors.blue,
            fontSize: screenWidth * 0.01),
      ),
      onTap: () async {
        if (!isSelected) {
          if (onTap != null) {
            await onTap();
          }
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => screen,
              settings: RouteSettings(name: routeName),
            ),
          );
        }
      },
    );
  }
}
