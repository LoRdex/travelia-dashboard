import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/hotel_cubit/hotel_cubit.dart';
import 'package:travelia_dashboard/cubits/restaurant_cubit/restaurant_cubit.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/models/profile_model.dart';
import 'package:travelia_dashboard/views/hotel/available_rooms.dart';

import 'package:travelia_dashboard/views/shared/financial_log.dart';
import 'package:travelia_dashboard/views/shared/profile.dart';
import 'package:travelia_dashboard/views/shared/support_screen.dart';


import '../../constants/api.dart';
import '../../cubits/main_cubit/main_cubit.dart';


class CustomDrawerHotel extends StatelessWidget {
  const CustomDrawerHotel({
    super.key,
  });


  @override
  Widget build(BuildContext context) {
    final HotelCubit hotelCubit =
    BlocProvider.of<HotelCubit>(context);
    final mainCubit = BlocProvider.of<MainCubit>(context);
    final screenWidth = MediaQuery.of(context).size.width;
    final ProfileModel profileModel = mainCubit.profileModel!;

    return Drawer(
      backgroundColor: MyColors.scaffoldColor,
      child: BlocBuilder<TripOrganizerCubit, TripOrganizerState>(
        builder: (context, state) => ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: InkWell(
                onTap: () {
                  if (ModalRoute.of(context)?.settings.name != '/profile') {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const ProfileView(),
                        settings: const RouteSettings(name: '/profile'),
                      ),
                    );
                  }
                },
                child: Column(
                  children: [
                    Text(
                      '${profileModel.name}',
                      style: TextStyle(
                          color: MyColors.blue, fontSize: screenWidth * 0.01),
                    ),
                    const SizedBox(height: 10),
                    CircleAvatar(
                      radius: 42,
                      backgroundColor: MyColors.blue,
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                            '${Api.imgUrl}/${profileModel.facilityPhoto}'),
                        radius: 40,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 20),
            _buildDrawerItem(
              context,
              icon: Icons.attach_money,
              title: 'Financial Logs',
              screen: const FinancialLogScreen(),
              // Replace with your screen
              routeName: '/financial_logs',
              screenWidth: screenWidth,
            ),
            const SizedBox(height: 20),
            _buildDrawerItem(
              context,
              icon: Icons.meeting_room,
              title: 'Available Rooms',
              screen: const AvailableRooms(),
              // Replace with your screen
              routeName: '/available_rooms',
              screenWidth: screenWidth,
            ),


            const SizedBox(height: 20),
            _buildDrawerItem(
              context,
              icon: Icons.message,
              title: 'Contact Travelia Admins',
              screen: const SupportScreen(),
              routeName: '/contact_admins',
              screenWidth: screenWidth,
            ),
            const SizedBox(height: 20),
            ListTile(
              leading: Icon(Icons.logout_sharp,
                  color: MyColors.blue, size: screenWidth * 0.017),
              title: Text(
                'Sign Out',
                style: TextStyle(
                    color: MyColors.blue, fontSize: screenWidth * 0.01),
              ),
              onTap: () {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text(
                        'Are You Sure You Want to Sign Out ?',
                        style: TextStyle(
                            color: MyColors.blue,
                            fontSize: screenWidth * 0.012),
                      ),
                      content: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          TextButton(onPressed: (){Navigator.pop(context);}, child: const Text('Continue',style: TextStyle(color: Colors.black),)),
                          TextButton(onPressed: ()async{
                            await mainCubit.logout(context);


                          }, child: const Text('Yes',style: TextStyle(color: Colors.black),)),
                        ],
                      ),
                    );
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDrawerItem(BuildContext context,
      {required IconData icon,
        required String title,
        required Widget screen,
        required String routeName,
        required double screenWidth,
        Future<void> Function()? onTap}) {
    final bool isSelected = ModalRoute.of(context)?.settings.name == routeName;

    return ListTile(
      leading: Icon(icon,
          color: isSelected ? Colors.grey : MyColors.blue,
          size: screenWidth * 0.017),
      title: Text(
        title,
        style: TextStyle(
            color: isSelected ? Colors.grey : MyColors.blue,
            fontSize: screenWidth * 0.01),
      ),
      onTap: () async {
        if (!isSelected) {
          if (onTap != null) {
            await onTap();
          }
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => screen,
              settings: RouteSettings(name: routeName),
            ),
          );
        }
      },
    );
  }
}
