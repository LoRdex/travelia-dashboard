import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/constants/cache_service.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/admin_cubit/admin_cubit.dart';
import 'package:travelia_dashboard/cubits/hotel_cubit/hotel_cubit.dart';
import 'package:travelia_dashboard/cubits/restaurant_cubit/restaurant_cubit.dart';
import 'package:travelia_dashboard/cubits/transporter_cubit/transporter_cubit.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/views/facility_create/hotel.dart';
import 'package:travelia_dashboard/views/home/admin_home_screen.dart';
import 'package:travelia_dashboard/views/home/hotel_home_screen.dart';
import 'package:travelia_dashboard/views/home/restaurant_home_screen.dart';
import 'package:travelia_dashboard/views/home/transporter_home_screen.dart';
import '../views/home/organizer_home_screen.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  CustomAppBar({
    super.key,
    this.title = 'Organizer',
  });

  String? title;

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    final TripOrganizerCubit tripOrganizerCubit =
    BlocProvider.of<TripOrganizerCubit>(context);
    final HotelCubit hotelCubit = BlocProvider.of<HotelCubit>(context);
    final TransporterCubit transporterCubit =
    BlocProvider.of<TransporterCubit>(context);
    final RestaurantCubit restaurantCubit =
    BlocProvider.of<RestaurantCubit>(context);
    final AdminCubit adminCubit =
    BlocProvider.of<AdminCubit>(context);

    return AppBar(
      backgroundColor: Colors.grey.withOpacity(.2),
      leading: Builder(builder: (context) {
        return IconButton(
          onPressed: () {
            Scaffold.of(context).openDrawer();
          },
          icon: Icon(
            Icons.menu,
            color: MyColors.blue,
            size: screenWidth * 0.015,
          ),
        );
      }),
      title: Text(
        '$title',
        style: TextStyle(color: MyColors.blue, fontSize: screenWidth * 0.014),
      ),
      actions: [
        IconButton(
          onPressed: () async {
            if (CacheService.getData(key: 'role_id') == 1) {
              await adminCubit.getAccountsForConfirmation();
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  settings: const RouteSettings(name: '/'),
                  builder: (context) => const AdminHomeScreen(),
                ),
                    (route) => false,
              );
            }
            if (CacheService.getData(key: 'role_id') == 2) {
              await tripOrganizerCubit.getOrganizedTrips();
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  settings: const RouteSettings(name: '/'),
                  builder: (context) => const OrganizerHomeScreen(),
                ),
                    (route) => false,
              );
            }
            if (CacheService.getData(key: 'role_id') == 4) {
              await restaurantCubit.getRestaurantReservations();
              await restaurantCubit.getRestaurantReservationsByOrganizers();
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  settings: const RouteSettings(name: '/'),
                  builder: (context) => const RestaurantHomeScreen(),
                ),
                    (route) => false,
              );
            }
            if (CacheService.getData(key: 'role_id') == 3) {
              await hotelCubit.getHotelReservations();
              await hotelCubit.getHotelReservationsByOrganizers();
              hotelCubit.endController.clear();
              hotelCubit.dateController.clear();
              hotelCubit.availableRoomsModel=null;
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  settings: const RouteSettings(name: '/'),
                  builder: (context) => const HotelHomeScreen(),
                ),
                    (route) => false,
              );
            }
            if (CacheService.getData(key: 'role_id') == 5) {
              await transporterCubit.getTransporterRoutes();
              await transporterCubit.getTransporterReservationsByOrganizers();
              transporterCubit.date=null;
              transporterCubit.transporterStartController.clear();
              transporterCubit.dateController.clear();
              transporterCubit.transporterEndController.clear();
              transporterCubit.vehicleController.clear();
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  settings: const RouteSettings(name: '/'),
                  builder: (context) => const TransporterHomeScreen(),
                ),
                    (route) => false,
              );
            }
          },
          icon: Icon(
            Icons.home_sharp,
            size: screenWidth * 0.015,
            color: MyColors.blue,
          ),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
