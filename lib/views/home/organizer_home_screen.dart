import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/main_cubit/main_cubit.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/models/organized_trips_model.dart';
import 'package:travelia_dashboard/views/shared/financial_log.dart';

import '../../components/drawers/custom_drawer_organizer.dart';
import '../../constants/api.dart';
import '../../constants/cache_service.dart';

class OrganizerHomeScreen extends StatelessWidget {
  const OrganizerHomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final TripOrganizerCubit tripOrganizerCubit =
        BlocProvider.of<TripOrganizerCubit>(context);
    final MainCubit mainCubit = BlocProvider.of<MainCubit>(context);
    final organizedTrips = tripOrganizerCubit.organizedTripsModel?.trips ?? [];
    return BlocBuilder<TripOrganizerCubit, TripOrganizerState>(
      builder: (context, state) {

        return Scaffold(
          appBar: AppBar(
            backgroundColor: MyColors.scaffoldColor.withOpacity(.7),
            leading: Builder(builder: (context) {
              return IconButton(
                  onPressed: () async {
                    Scaffold.of(context).openDrawer();
                  },
                  icon: Icon(
                    Icons.menu,
                    color: MyColors.blue,
                    size: screenWidth * 0.015,
                  ));
            }),
            title: Text(
              'Welcome Organizer',
              style:
              TextStyle(fontSize: screenWidth * 0.016, color: MyColors.blue),
            ),
            actions: [
              InkWell(
                onTap: () async {
                  await mainCubit.getFinancialLog();

                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const FinancialLogScreen(),
                      ));
                },
                child: SizedBox(
                    width: screenWidth / 13,
                    height: screenHeight/10,
                    child: Row(
                      children: [
                        Icon(
                          size: screenWidth * 0.014,
                          Icons.monetization_on_outlined,
                          color: MyColors.blue,
                        ),
                        SizedBox(
                          width: screenWidth / 110,
                        ),
                        Text(
                          '${mainCubit.financialModel?.totalBalance ?? '0'}' ,
                          style: TextStyle(
                              color: MyColors.blue,
                              fontSize: screenWidth * 0.011,
                              fontWeight: FontWeight.w300),
                        ),
                      ],
                    )),
              )
            ],
          ),
          drawer: const CustomDrawerOrganizer(),
          body: organizedTrips.isEmpty
              ? Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'No Available Trips ! Try To Create',
                  style: TextStyle(
                      color: MyColors.blue, fontSize: screenWidth * 0.018),
                ),
                const SizedBox(
                  height: 5,
                ),
                IconButton(
                    onPressed: () async {
                      await tripOrganizerCubit.getOrganizedTrips();
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                            const OrganizerHomeScreen(),
                          ));
                    },
                    icon: const Icon(
                      Icons.refresh,
                      color: MyColors.blue,
                    ))
              ],
            ),
          )
              : Stack(
            fit: StackFit.expand,
            children: [
              Container(
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.fill,
                          image: AssetImage('images/background3.jpg')))),
              GridView.builder(
                padding: EdgeInsets.only(
                    right: screenWidth / 16,
                    left: screenWidth / 16,
                    top: screenWidth / 22),
                itemCount: organizedTrips.length.clamp(1, 9),
                // Use the length of the areas list
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisSpacing: screenWidth / 25,
                  mainAxisSpacing: screenHeight / 13,
                  childAspectRatio: 3 / 2,
                  crossAxisCount: 3,
                ),
                itemBuilder: (context, index) {
                  return GridItem(trips: organizedTrips[index]);
                },
              ),
            ],
          ),
        );
      }
    );
  }
}

class GridItem extends StatelessWidget {
  final Trips trips; // Accept a TouristAreas object

  const GridItem({super.key, required this.trips});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return BlocBuilder<TripOrganizerCubit, TripOrganizerState>(
      builder: (context, state) {
        final isHovering = state is GridHover && state.index == trips.id;
        final scaleFactor = isHovering ? 1.05 : 1.0;
        final glowColor =
            isHovering ? MyColors.blue.withOpacity(0.8) : Colors.transparent;
        return MouseRegion(
          onEnter: (_) =>
              context.read<TripOrganizerCubit>().updateGrid(trips.id!, true),
          onExit: (_) =>
              context.read<TripOrganizerCubit>().updateGrid(trips.id!, false),
          child: AnimatedContainer(
            duration: const Duration(seconds: 1),
            transform: Matrix4.identity()..scale(scaleFactor),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              gradient: const LinearGradient(
                colors: [Colors.blueAccent, Colors.cyanAccent],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              ),
              boxShadow: [
                BoxShadow(
                  color: glowColor,
                  spreadRadius: 4,
                  blurRadius: 15,
                  offset: const Offset(0, 0),
                ),
                BoxShadow(
                  color: Colors.black.withOpacity(0.3),
                  spreadRadius: 2,
                  blurRadius: 8,
                  offset: const Offset(0, 4),
                ),
              ],
              border: Border.all(color: Colors.white, width: 2),
            ),
            curve: Curves.easeInOut,
            child: GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                          scrollable: true,
                          content: SizedBox(
                            height: MediaQuery.of(context).size.height / 2,
                            child: Center(
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    'Trip Details : ',
                                    style: TextStyle(
                                        color: MyColors.blue,
                                        fontSize: screenWidth * 0.012),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Tourist Area : ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        '${trips.touristArea} ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Start Location : ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        '${trips.strLocation} ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.009),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Restaurant: ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        '${trips.restaurant} ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Hotel: ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        '${trips.hotel} ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Transport Company: ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        '${trips.transporter} ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Start Date: ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        '${trips.strDate} ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'End Date: ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        '${trips.endDate}',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Cost : ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        '${trips.cost} ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Capacity : ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        '${trips.totalCapacity} ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Available Places : ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        '${trips.availablePlaces} ',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.011),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: const Text(
                                        'Close',
                                        style: TextStyle(color: Colors.black),
                                      ))
                                ],
                              ),
                            ),
                          ),
                        ));
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  image: DecorationImage(
                    image: NetworkImage('${Api.imgUrl}/${trips.img}'),
                    fit: BoxFit.cover,
                    colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(isHovering ? 0.2 : 0.5),
                      BlendMode.darken,
                    ),
                  ),
                ),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      top: 0,
                      right: 0,
                      child: Transform.rotate(
                        angle: -pi / 4,
                        child: Container(
                          decoration: BoxDecoration(
                            color: DateTime.now()
                                    .isAfter(DateTime.parse(trips.endDate!))
                                ? Colors.red
                                : Colors.green,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          height: 30,
                          width: 100,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                DateTime.now()
                                        .isAfter(DateTime.parse(trips.endDate!))
                                    ? Icons.close
                                    : Icons.check,
                                color: Colors.white,
                                size: 16,
                              ),
                              const SizedBox(width: 4),
                              Text(
                                DateTime.now()
                                        .isAfter(DateTime.parse(trips.endDate!))
                                    ? 'Ended'
                                    : 'On Going',
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 20,
                      child: Text(
                        trips.touristArea!,
                        style: TextStyle(
                          shadows: const [
                            Shadow(
                                color: Colors.black,
                                offset: Offset(-1.5, -1.5)),
                            Shadow(
                                color: Colors.black, offset: Offset(1.5, -1.5)),
                            Shadow(
                                color: Colors.black, offset: Offset(-1.5, 1.5)),
                            Shadow(
                                color: Colors.black, offset: Offset(1.5, 1.5)),
                          ],
                          fontSize: screenWidth * 0.012,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    if (!isHovering)
                      Positioned(
                        bottom: 10,
                        left: 20,
                        child: Text(
                          trips.strDate!,
                          style: TextStyle(
                            shadows: const [
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(-1.5, -1.5)),
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(1.5, -1.5)),
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(-1.5, 1.5)),
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(1.5, 1.5)),
                            ],
                            fontSize: screenWidth * 0.007,
                            color: Colors.white,
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
// ListTile(contentPadding: EdgeInsets.all(screenHeight/100),

// ),
