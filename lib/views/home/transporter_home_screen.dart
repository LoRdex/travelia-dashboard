import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/drawers/custom_transporter_drawer.dart';
import 'package:travelia_dashboard/cubits/transporter_cubit/transporter_cubit.dart';
import 'package:travelia_dashboard/views/home/widgets/transporter/reservations_transporter.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/widgets/reservation.dart';
import '../../components/custom_appbar_home.dart';
import '../../components/dialogs.dart';
import '../../constants/my_colors.dart';
import '../transporter/reservations_screen.dart';

class TransporterHomeScreen extends StatelessWidget {
  const TransporterHomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    final TransporterCubit transporterCubit =
        BlocProvider.of<TransporterCubit>(context);
    final routes = transporterCubit.transporterRoutesModel?.routes;
    return BlocConsumer<TransporterCubit, TransporterState>(
      builder: (context, state) => DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: CustomAppBarHome(
            title: 'Routes',
          ),
          drawer: const CustomDrawerTransporter(),
          body: Stack(
            fit: StackFit.expand,
            children: [
              Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/background3.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: kToolbarHeight),
                child: TabBarView(children: [Routes(),OrganizersReservations()]),
              )
            ],
          ),
        ),
      ),
      listener: (BuildContext context, TransporterState state) {},
    );
  }
}
