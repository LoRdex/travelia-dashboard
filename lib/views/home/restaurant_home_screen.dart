import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_restaurant.dart';
import 'package:travelia_dashboard/cubits/restaurant_cubit/restaurant_cubit.dart';
import 'package:pretty_animated_buttons/pretty_animated_buttons.dart';
import 'package:travelia_dashboard/views/home/widgets/restaurant/reservations_restaurant.dart';
import '../../components/custom_appbar_home.dart';
import '../../constants/api.dart';
import '../../constants/my_colors.dart';

class RestaurantHomeScreen extends StatelessWidget {
  const RestaurantHomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    final RestaurantCubit restaurantCubit =
    BlocProvider.of<RestaurantCubit>(context);
    final reservations =
        restaurantCubit.restaurantReservationModel?.reservations;
    return BlocConsumer<RestaurantCubit, RestaurantState>(
      builder: (context, state) => DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: CustomAppBarHome(
            title: 'Reservations',
          ),
          drawer: const CustomDrawerRestaurant(),
          body: Stack(
            fit: StackFit.expand,
            children: [
              Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/background3.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: kToolbarHeight),
                child: TabBarView(children: [RestaurantReservations(),RestaurantReservationsOrganizer()]),
              ) ],
          ),
        ),
      ),
      listener: (BuildContext context, RestaurantState state) {},
    );
  }
}
