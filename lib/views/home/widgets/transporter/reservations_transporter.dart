import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../components/custom_button.dart';
import '../../../../components/dialogs.dart';
import '../../../../constants/api.dart';
import '../../../../constants/my_colors.dart';
import '../../../../cubits/transporter_cubit/transporter_cubit.dart';
import '../../../transporter/reservations_screen.dart';

class Routes extends StatelessWidget {
  const Routes({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    final TransporterCubit transporterCubit =
    BlocProvider.of<TransporterCubit>(context);
    final routes = transporterCubit.transporterRoutesModel?.routes;
    return  Center(
      child: Container(
        width: screenWidth / 1.3,
        height: screenHeight / 1.2,
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.grey),
          borderRadius: BorderRadius.circular(30),
          color: Colors.white.withOpacity(0.8),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: routes == null || routes.isEmpty
              ? Center(
            child: Text(
              'No Routes Available.',
              style: TextStyle(
                color: MyColors.blue,
                fontSize: screenWidth * 0.015,
              ),
            ),
          )
              : ListView.builder(
            itemCount: routes.length,
            itemBuilder: (context, index) {
              return Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                elevation: 6,
                shadowColor: Colors.black.withOpacity(0.2),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                        width: 1, color: MyColors.blue),
                    gradient: LinearGradient(
                      colors: [
                        Colors.white,
                        Colors.grey.shade200
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                    borderRadius:
                    BorderRadius.circular(screenWidth / 100),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(screenWidth / 110),
                    child: Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceAround,
                      children: [
                        SizedBox(
                          width: screenWidth / 5,
                          child: Column(
                            children: [
                              const SizedBox(height: 5),
                              Text(
                                'From',
                                style: TextStyle(
                                  color: MyColors.blue,
                                  fontSize: screenWidth * 0.012,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const SizedBox(height: 5),
                              Icon(
                                Icons.public,
                                color: MyColors.blue,
                                size: screenWidth * 0.025,
                              ),
                              const SizedBox(height: 5),
                              Text(
                                '${routes[index].strCountry}',
                                style: TextStyle(
                                  color: MyColors.blue,
                                  fontSize: screenWidth * 0.012,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                '${routes[index].strCity}',
                                style: TextStyle(
                                  color: MyColors.blue,
                                  fontSize: screenWidth * 0.011,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: screenWidth / 8.5,
                          child: Column(
                            children: [
                              Text(
                                '${routes[index].distance?.round()} Km',
                                style: TextStyle(
                                  fontSize: screenWidth * 0.011,
                                  color: MyColors.blue,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                '${routes[index].dateTime}',
                                style: TextStyle(
                                  fontSize: screenWidth * 0.010,
                                  color: MyColors.blue,
                                ),
                              ),
                              Text(
                                '${routes[index].transportation?.toUpperCase()}',
                                style: TextStyle(
                                  fontSize: screenWidth * 0.010,
                                  color: MyColors.blue,
                                ),
                              ),
                              CustomButton(
                                text: 'Details',
                                onTap: () {
                                  showDialog(
                                    context: context,
                                    builder: (context) =>
                                        AlertDialog(
                                          content: Center(
                                            child: Column(
                                              mainAxisAlignment:
                                              MainAxisAlignment
                                                  .spaceEvenly,
                                              children: [
                                                Text(
                                                  'Route Details',
                                                  style: TextStyle(
                                                    fontWeight:
                                                    FontWeight
                                                        .bold,
                                                    color:
                                                    MyColors.blue,
                                                    fontSize:
                                                    screenWidth *
                                                        0.012,
                                                  ),
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'Start Location:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        '${routes[index].strCountry},${routes[index].strCity},${routes[index].strState}',
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'Start Country:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        routes[index]
                                                            .strCountry!,
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'Start City:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        routes[index]
                                                            .strCity!,
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'Start State:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        routes[index]
                                                            .strState!,
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'End Address:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        '${routes[index].endCountry},${routes[index].endCity},${routes[index].endState}',
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'End Country:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        routes[index]
                                                            .endCountry!,
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'End City:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        routes[index]
                                                            .endCity!,
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'End State:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        routes[index]
                                                            .endState!,
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'Vehicle:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        routes[index]
                                                            .transportation!,
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'Date:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        routes[index]
                                                            .dateTime!,
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'Total Capacity:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        routes[index]
                                                            .totalCapacity!
                                                            .toString(),
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'Available Capacity:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        routes[index]
                                                            .availableCapacity
                                                            .toString(),
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'Cost:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        routes[index]
                                                            .cost
                                                            .toString(),
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'Total Distance:',
                                                      style:
                                                      TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color:
                                                        MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.011,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        width:
                                                        screenWidth /
                                                            100),
                                                    Flexible(
                                                      child: Text(
                                                        routes[index]
                                                            .distance
                                                            .toString(),
                                                        style:
                                                        TextStyle(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceEvenly,
                                                  children: [
                                                    ElevatedButton(
                                                      style: ElevatedButton
                                                          .styleFrom(
                                                        backgroundColor:
                                                        MyColors
                                                            .blue,
                                                        shape:
                                                        RoundedRectangleBorder(
                                                          borderRadius:
                                                          BorderRadius.circular(
                                                              12),
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        Navigator.pop(
                                                            context);
                                                      },
                                                      child:
                                                      const Text(
                                                        'Close',
                                                        style:
                                                        TextStyle(
                                                          color: Colors
                                                              .white,
                                                        ),
                                                      ),
                                                    ),
                                                    ElevatedButton(
                                                      style: ElevatedButton
                                                          .styleFrom(
                                                        backgroundColor:
                                                        MyColors
                                                            .blue,
                                                        shape:
                                                        RoundedRectangleBorder(
                                                          borderRadius:
                                                          BorderRadius.circular(
                                                              12),
                                                        ),
                                                      ),
                                                      onPressed:
                                                          () async {
                                                        await transporterCubit
                                                            .getReservation(
                                                            routes[index]
                                                                .id!);

                                                        if (transporterCubit
                                                            .reservationModel
                                                            ?.reservations ==
                                                            null ||
                                                            transporterCubit
                                                                .reservationModel!
                                                                .reservations!
                                                                .isEmpty) {
                                                          Dialogs.showFailedDialog(
                                                              context,
                                                              'There is No Reservations For This Trip Yet');
                                                        } else   {
                                                          Navigator.pop(
                                                              context);
                                                          Navigator
                                                              .push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  (context) =>
                                                              const ReservationForTransporter(),
                                                            ),
                                                          );
                                                        }
                                                      },
                                                      child:
                                                      const Text(
                                                        'Show Reservations',
                                                        style:
                                                        TextStyle(
                                                          color: Colors
                                                              .white,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                  );
                                },
                                fontSize: 120,
                                dividerH: 30,
                                dividerW: 20,
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          width: screenWidth / 5,
                          child: Column(
                            children: [
                              const SizedBox(height: 5),
                              Text(
                                'To',
                                style: TextStyle(
                                  color: MyColors.blue,
                                  fontSize: screenWidth * 0.012,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const SizedBox(height: 5),
                              Icon(
                                Icons.public,
                                color: MyColors.blue,
                                size: screenWidth * 0.025,
                              ),
                              const SizedBox(height: 5),
                              Text(
                                '${routes[index].endCountry}',
                                style: TextStyle(
                                  color: MyColors.blue,
                                  fontSize: screenWidth * 0.012,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                '${routes[index].endCity}',
                                style: TextStyle(
                                  color: MyColors.blue,
                                  fontSize: screenWidth * 0.011,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}



class OrganizersReservations extends StatelessWidget {
  const OrganizersReservations({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final TransporterCubit transporterCubit =
    BlocProvider.of<TransporterCubit>(context);
    final trips = transporterCubit.transporterReservationByOrganizerModel?.trips;
    return Center(
      child: Container(
        width: screenWidth / 1.6,
        height: screenHeight / 1.3,
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.grey),
          borderRadius: BorderRadius.circular(30),
          color: Colors.white.withOpacity(0.8),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: trips == null || trips.isEmpty
              ? Center(
            child: Text(
              'No reservations available.',
              style: TextStyle(
                color: MyColors.blue,
                fontSize: screenWidth * 0.015,
              ),
            ),
          )
              : ListView.builder(
            itemCount: trips.length,
            itemBuilder: (context, index) {
              return Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                  side: BorderSide(
                    color: MyColors.blue.withOpacity(0.3),
                    width: 2,
                  ),
                ),
                elevation: 1,
                child: Container(
                  decoration: BoxDecoration(border: Border.all(width:1,color: MyColors.blue),
                    gradient: LinearGradient(
                      colors: [Colors.white, Colors.grey.shade200],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  top: screenHeight / 35,
                                  left: screenWidth / 25),
                              child: Container(
                                width: screenWidth / 15,
                                height: screenWidth / 15,
                                decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.circular(screenWidth/200),
                                  border: Border.all(
                                      color: MyColors.blue.withOpacity(.8),
                                      width: 2.6),
                                  image: DecorationImage(
                                    image: NetworkImage(
                                        '${Api.imgUrl}/${trips[index].organizerImg}'),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: screenWidth * 0.05),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${trips[index].organizer}',
                                    style: TextStyle(
                                      color: MyColors.blue,
                                      fontSize: screenWidth * 0.012,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    children: [
                                      const Icon(Icons.calendar_month,
                                          color: MyColors.blue),
                                      const SizedBox(width: 5),
                                      Text(
                                        'Start Date: ${trips[index].strDate}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize:
                                          screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    children: [
                                      const Icon(
                                          Icons.calendar_month,
                                          color: MyColors.blue),
                                      const SizedBox(width: 5),
                                      Text(
                                        'End Date: ${trips[index].endDate}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize:
                                          screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    children: [
                                      const Icon(Icons.reduce_capacity,
                                          color: MyColors.blue),
                                      const SizedBox(width: 5),
                                      Text(
                                        'Capacity: ${trips[index].capacity}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize:
                                          screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    children: [
                                      const Icon(Icons.place,
                                          color: MyColors.blue),
                                      const SizedBox(width: 5),
                                      Text(
                                        'Tourist Area: ${trips[index].touristArea}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize:
                                          screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),

                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  right: screenWidth / 100,
                                  top: screenHeight / 10),
                              child: CustomButton(
                                  dividerW: 10,
                                  dividerH: 20,
                                  fontSize: 100,
                                  text: 'Area Image',
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        builder: (context) =>
                                            AlertDialog(
                                              title: Text(
                                                'Area Image',
                                                style: TextStyle(
                                                    color:
                                                    MyColors.blue,
                                                    fontSize:
                                                    screenWidth *
                                                        0.010),
                                              ),
                                              content: Container(
                                                height: screenHeight,
                                                width: screenWidth / 2,
                                                decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                        fit:
                                                        BoxFit.fill,
                                                        image: NetworkImage(
                                                            '${Api.imgUrl}/${trips[index].touristAreaImg}'))),
                                              ),
                                            )
                                    );
                                  }),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );;
  }
}

