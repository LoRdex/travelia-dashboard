import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../components/custom_button.dart';
import '../../../../constants/api.dart';
import '../../../../constants/my_colors.dart';
import '../../../../cubits/hotel_cubit/hotel_cubit.dart';

class HotelReservations extends StatelessWidget {
  const HotelReservations({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final HotelCubit hotelCubit = BlocProvider.of<HotelCubit>(context);
    final reservations = hotelCubit.hotelReservationModel?.reservations;

    return Center(
      child: Container(
        width: screenWidth / 1.6,
        height: screenHeight / 1.3,
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.grey),
          borderRadius: BorderRadius.circular(30),
          color: Colors.white.withOpacity(0.8),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: reservations == null || reservations.isEmpty
              ? Center(
            child: Text(
              'No reservations available.',
              style: TextStyle(
                color: MyColors.blue,
                fontSize: screenWidth * 0.015,
              ),
            ),
          )
              : ListView.builder(
            itemCount: reservations.length,
            itemBuilder: (context, index) {
              return Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                  side: BorderSide(
                    color: MyColors.blue.withOpacity(0.3),
                    width: 2,
                  ),
                ),
                elevation: 1,
                child: Container(
                  decoration: BoxDecoration(border: Border.all(width:1,color: MyColors.blue),
                    gradient: LinearGradient(
                      colors: [Colors.white, Colors.grey.shade200],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  top: screenHeight / 35,
                                  left: screenWidth / 25),
                              child: Container(
                                width: screenWidth / 15,
                                height: screenWidth / 15,
                                decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.circular(screenWidth/200),
                                  border: Border.all(
                                      color: MyColors.blue.withOpacity(.8),
                                      width: 2.6),
                                  image: DecorationImage(
                                    image: NetworkImage(
                                        '${Api.imgUrl}/${reservations[index].photo}'),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: screenWidth * 0.05),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${reservations[index].name}',
                                    style: TextStyle(
                                      color: MyColors.blue,
                                      fontSize: screenWidth * 0.012,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    children: [
                                      const Icon(Icons.table_chart,
                                          color: MyColors.blue),
                                      const SizedBox(width: 5),
                                      Text(
                                        'Room: ${reservations[index].room}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize:
                                          screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    children: [
                                      const Icon(
                                          Icons.calendar_today,
                                          color: MyColors.blue),
                                      const SizedBox(width: 5),
                                      Text(
                                        'Days: ${reservations[index].daysNum}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize:
                                          screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    children: [
                                      const Icon(Icons.access_time,
                                          color: MyColors.blue),
                                      const SizedBox(width: 5),
                                      Text(
                                        'Start Date: ${reservations[index].strDate}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize:
                                          screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    children: [
                                      const Icon(Icons.av_timer_sharp,
                                          color: MyColors.blue),
                                      const SizedBox(width: 5),
                                      Text(
                                        'End Date: ${reservations[index].endDate}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize:
                                          screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    children: [
                                      const Icon(Icons.money,
                                          color: MyColors.blue),
                                      const SizedBox(width: 5),
                                      Text(
                                        'Reservation Cost: ${reservations[index].cost}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize:
                                          screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  right: screenWidth / 100,
                                  top: screenHeight / 10),
                              child: CustomButton(
                                  dividerW: 10,
                                  dividerH: 20,
                                  fontSize: 100,
                                  text: 'Details',
                                  onTap: () {
                                    showDialog(
                                      context: context,
                                      builder: (context) =>
                                          AlertDialog(
                                            content: Center(
                                              child: Column(
                                                mainAxisAlignment:
                                                MainAxisAlignment
                                                    .spaceEvenly,
                                                children: [
                                                  Text(
                                                    'User Details',
                                                    style: TextStyle(
                                                      fontWeight:
                                                      FontWeight
                                                          .bold,
                                                      color: MyColors
                                                          .blue,
                                                      fontSize:
                                                      screenWidth *
                                                          0.012,
                                                    ),
                                                  ),
                                                  Container(
                                                    width: screenWidth /
                                                        15,
                                                    height:
                                                    screenWidth /
                                                        15,
                                                    decoration:
                                                    BoxDecoration(
                                                      borderRadius:
                                                      BorderRadius
                                                          .circular(
                                                          10),
                                                      border: Border.all(
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          width: 2),
                                                      image:
                                                      DecorationImage(
                                                        image: NetworkImage(
                                                            '${Api.imgUrl}/${reservations[index].photo}'),
                                                        fit: BoxFit
                                                            .cover,
                                                      ),
                                                    ),
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                    children: [
                                                      Text(
                                                        'Name:',
                                                        style:
                                                        TextStyle(
                                                          fontWeight:
                                                          FontWeight
                                                              .bold,
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                          width:
                                                          screenWidth /
                                                              100),
                                                      Flexible(
                                                        child: Text(
                                                          reservations[
                                                          index]
                                                              .name!,
                                                          style:
                                                          TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                    children: [
                                                      Text(
                                                        'Email:',
                                                        style:
                                                        TextStyle(
                                                          fontWeight:
                                                          FontWeight
                                                              .bold,
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                          width:
                                                          screenWidth /
                                                              100),
                                                      Flexible(
                                                        child: Text(
                                                          reservations[
                                                          index]
                                                              .email!,
                                                          style:
                                                          TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                    children: [
                                                      Text(
                                                        'Phone:',
                                                        style:
                                                        TextStyle(
                                                          fontWeight:
                                                          FontWeight
                                                              .bold,
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                          width:
                                                          screenWidth /
                                                              100),
                                                      Flexible(
                                                        child: Text(
                                                          reservations[
                                                          index]
                                                              .phone
                                                              .toString(),
                                                          style:
                                                          TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                    children: [
                                                      Text(
                                                        'Age:',
                                                        style:
                                                        TextStyle(
                                                          fontWeight:
                                                          FontWeight
                                                              .bold,
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                          width:
                                                          screenWidth /
                                                              100),
                                                      Flexible(
                                                        child: Text(
                                                          reservations[
                                                          index]
                                                              .age
                                                              .toString(),
                                                          style:
                                                          TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                    children: [
                                                      Text(
                                                        'Address:',
                                                        style:
                                                        TextStyle(
                                                          fontWeight:
                                                          FontWeight
                                                              .bold,
                                                          color:
                                                          MyColors
                                                              .blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.011,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                          width:
                                                          screenWidth /
                                                              100),
                                                      Flexible(
                                                        child: Text(
                                                          reservations[
                                                          index]
                                                              .address!,
                                                          style:
                                                          TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  ElevatedButton(
                                                    style:
                                                    ElevatedButton
                                                        .styleFrom(
                                                      backgroundColor:
                                                      MyColors
                                                          .blue,
                                                      shape:
                                                      RoundedRectangleBorder(
                                                        borderRadius:
                                                        BorderRadius
                                                            .circular(
                                                            12),
                                                      ),
                                                    ),
                                                    onPressed: () {
                                                      Navigator.pop(
                                                          context);
                                                    },
                                                    child: const Text(
                                                      'Close',
                                                      style:
                                                      TextStyle(
                                                        color: Colors
                                                            .white,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                    );
                                  }),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class HotelReservationsOrganizers extends StatelessWidget {
  const HotelReservationsOrganizers({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final HotelCubit hotelCubit = BlocProvider.of<HotelCubit>(context);
    final trips = hotelCubit.hotelReservationByOrganizersModel?.trips;
    return  Center(
      child: Container(
        width: screenWidth / 1.6,
        height: screenHeight / 1.3,
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.grey),
          borderRadius: BorderRadius.circular(30),
          color: Colors.white.withOpacity(0.8),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: trips == null || trips.isEmpty
              ? Center(
            child: Text(
              'No reservations available.',
              style: TextStyle(
                color: MyColors.blue,
                fontSize: screenWidth * 0.015,
              ),
            ),
          )
              : ListView.builder(
            itemCount: trips.length,
            itemBuilder: (context, index) {
              return Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                  side: BorderSide(
                    color: MyColors.blue.withOpacity(0.3),
                    width: 2,
                  ),
                ),
                elevation: 1,
                child: Container(
                  decoration: BoxDecoration(border: Border.all(width:1,color: MyColors.blue),
                    gradient: LinearGradient(
                      colors: [Colors.white, Colors.grey.shade200],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  top: screenHeight / 35,
                                  left: screenWidth / 25),
                              child: Container(
                                width: screenWidth / 15,
                                height: screenWidth / 15,
                                decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.circular(screenWidth/200),
                                  border: Border.all(
                                      color: MyColors.blue.withOpacity(.8),
                                      width: 2.6),
                                  image: DecorationImage(
                                    image: NetworkImage(
                                        '${Api.imgUrl}/${trips[index].organizerImg}'),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: screenWidth * 0.05),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${trips[index].organizer}',
                                    style: TextStyle(
                                      color: MyColors.blue,
                                      fontSize: screenWidth * 0.012,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    children: [
                                      const Icon(Icons.calendar_month,
                                          color: MyColors.blue),
                                      const SizedBox(width: 5),
                                      Text(
                                        'Start Date: ${trips[index].strDate}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize:
                                          screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    children: [
                                      const Icon(
                                          Icons.calendar_month,
                                          color: MyColors.blue),
                                      const SizedBox(width: 5),
                                      Text(
                                        'End Date: ${trips[index].endDate}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize:
                                          screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    children: [
                                      const Icon(Icons.reduce_capacity,
                                          color: MyColors.blue),
                                      const SizedBox(width: 5),
                                      Text(
                                        'Capacity: ${trips[index].capacity}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize:
                                          screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),
                                  Row(
                                    children: [
                                      const Icon(Icons.place,
                                          color: MyColors.blue),
                                      const SizedBox(width: 5),
                                      Text(
                                        'Tourist Area: ${trips[index].touristArea}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize:
                                          screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),

                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  right: screenWidth / 100,
                                  top: screenHeight / 10),
                              child: CustomButton(
                                  dividerW: 10,
                                  dividerH: 20,
                                  fontSize: 100,
                                  text: 'Area Image',
                                  onTap: () {
                                    showDialog(
                                      context: context,
                                      builder: (context) =>
                                          AlertDialog(
                                            title: Text(
                                              'Area Image',
                                              style: TextStyle(
                                                  color:
                                                  MyColors.blue,
                                                  fontSize:
                                                  screenWidth *
                                                      0.010),
                                            ),
                                            content: Container(
                                              height: screenHeight,
                                              width: screenWidth / 2,
                                              decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                      fit:
                                                      BoxFit.fill,
                                                      image: NetworkImage(
                                                          '${Api.imgUrl}/${trips[index].touristAreaImg}'))),
                                            ),
                                          )
                                    );
                                  }),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
