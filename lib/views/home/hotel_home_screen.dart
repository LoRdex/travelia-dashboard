import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_hotel.dart';
import 'package:travelia_dashboard/cubits/hotel_cubit/hotel_cubit.dart';
import 'package:travelia_dashboard/views/home/widgets/hotel/reservations_hotel.dart';
import '../../components/custom_appbar_home.dart';

class HotelHomeScreen extends StatelessWidget {
  const HotelHomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    final HotelCubit hotelCubit =
    BlocProvider.of<HotelCubit>(context);
    final reservations =
        hotelCubit.hotelReservationModel?.reservations;
    return BlocConsumer<HotelCubit, HotelState>(
      builder: (context, state) => DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: CustomAppBarHome(
            title: 'Reservations',
          ),
          drawer: const CustomDrawerHotel(),
          body: Stack(
            fit: StackFit.expand,
            children: [
              Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/background3.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
           const Padding(
             padding: EdgeInsets.only(top: kToolbarHeight),
             child: TabBarView(children: [HotelReservations(),HotelReservationsOrganizers()]),
           ) ],
          ),
           
          
        ),
      ),
      listener: (BuildContext context, HotelState state) {},
    );
  }
}
