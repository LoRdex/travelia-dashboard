import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/components/drawers/custom_transporter_drawer.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';

import '../../cubits/transporter_cubit/transporter_cubit.dart';

class ChooseVehicle extends StatelessWidget {
  const ChooseVehicle({super.key});

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final TransporterCubit transporterCubit =
        BlocProvider.of<TransporterCubit>(context);

    return BlocConsumer<TransporterCubit, TransporterState>(
      listener: (context, state) {},
      builder: (context, state) {
        final vehicles = transporterCubit.availableVehiclesModel;

        return Scaffold(
          appBar: CustomAppBar(),
          drawer: const CustomDrawerTransporter(),
          body: Stack(
            fit: StackFit.expand,
            children: [
              Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/background3.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Center(
                child: Container(
                  height: screenHeight / 1.1,
                  width: screenWidth / 1.1,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(screenWidth * 0.07),
                      border: Border.all(color: MyColors.blue, width: .5)),
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight / 5.5,
                      ),
                      CustomTextFormFiled(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Date Field is Required";
                          } else {
                            return null;
                          }
                        },
                        divider: 6,
                        controller: transporterCubit.dateController,
                        obscureText: false,
                        readOnly: true,
                        preIcon: const Icon(
                          Icons.date_range_outlined,
                          color: MyColors.blue,
                        ),
                        labelText: 'Start Date',
                        onTap: () {
                          transporterCubit.pickStartDate(context);
                        },
                      ),
                      SizedBox(
                        height: screenHeight / 12,
                      ),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: screenWidth / 6),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            InkWell(
                              onTap: () {
                                if (vehicles?.availableV1 == 0) {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text(
                                          'Vehicle is not Available',
                                          style: TextStyle(
                                              fontSize: screenWidth * 0.010),
                                        ),
                                        content: CustomButton(
                                            dividerH: 25,
                                            dividerW: 30,
                                            fontSize: 110,
                                            text: 'Close',
                                            onTap: () {
                                              Navigator.pop(context);
                                            }),
                                      );
                                    },
                                  );
                                } else if (transporterCubit.date == null) {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text(
                                          'Select Date First !',
                                          style: TextStyle(
                                              fontSize: screenWidth * 0.010),
                                        ),
                                        content: CustomButton(
                                            dividerH: 25,
                                            dividerW: 30,
                                            fontSize: 110,
                                            text: 'Close',
                                            onTap: () {
                                              Navigator.pop(context);
                                            }),
                                      );
                                    },
                                  );
                                } else {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text(
                                          'Are You Sure You Want to Pick Pullman ?',
                                          style: TextStyle(
                                              fontSize: screenWidth * 0.010),
                                        ),
                                        content: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            CustomButton(
                                                dividerH: 25,
                                                dividerW: 20,
                                                fontSize: 110,
                                                text: 'Yes',
                                                onTap: () {
                                                  transporterCubit.vehicleController.text='pullman';
                                                  Navigator.pop(context);
                                                  Navigator.pop(context);

                                                }),
                                            CustomButton(
                                                dividerH: 25,
                                                dividerW: 20,
                                                fontSize: 110,
                                                text: 'No',
                                                onTap: () {
                                                  Navigator.pop(context);
                                                })
                                          ],
                                        ),
                                      );
                                    },
                                  );
                                }
                              },
                              child: Container(
                                width: screenWidth / 8,
                                height: screenHeight / 5,
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.circular(screenWidth * 0.01),
                                  border: Border.all(
                                      color: MyColors.blue, width: 1),
                                  gradient: LinearGradient(
                                    colors: [
                                      Colors.white,
                                      Colors.grey.shade200
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                                child: Column(
                                  children: [
                                    SizedBox(height: screenHeight / 45),
                                    Text(
                                      'Pullman',
                                      style: TextStyle(
                                        color: MyColors.blue,
                                        fontSize: screenWidth * 0.010,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Image.asset(
                                      'images/bus.png',
                                      color: MyColors.blue,
                                      width: screenWidth / 20,
                                    ),
                                    if(state is GetVehiclesLoading)
                                      const LinearProgressIndicator(color: MyColors.blue,)else
                                    Text(transporterCubit.date == null ? 'Select Date' :
                                      '${vehicles?.availableV1}/${vehicles?.v1}',
                                      style: TextStyle(
                                        color: MyColors.blue,
                                        fontSize: screenWidth * 0.011,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                if (vehicles?.availableV2 == 0) {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text(
                                          'Vehicle is not Available',
                                          style: TextStyle(
                                              fontSize: screenWidth * 0.010),
                                        ),
                                        content: CustomButton(
                                            dividerH: 25,
                                            dividerW: 30,
                                            fontSize: 110,
                                            text: 'Close',
                                            onTap: () {
                                              Navigator.pop(context);
                                            }),
                                      );
                                    },
                                  );
                                } else if (transporterCubit.date == null) {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text(
                                          'Select Date First !',
                                          style: TextStyle(
                                              fontSize: screenWidth * 0.010),
                                        ),
                                        content: CustomButton(
                                            dividerH: 25,
                                            dividerW: 30,
                                            fontSize: 110,
                                            text: 'Close',
                                            onTap: () {
                                              Navigator.pop(context);
                                            }),
                                      );
                                    },
                                  );
                                } else {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text(
                                          'Are You Sure You Want to Pick Bus ?',
                                          style: TextStyle(
                                              fontSize: screenWidth * 0.010),
                                        ),
                                        content: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            CustomButton(
                                                dividerH: 25,
                                                dividerW: 20,
                                                fontSize: 110,
                                                text: 'Yes',
                                                onTap: () {
                                                  transporterCubit.vehicleController.text='bus';
                                                  Navigator.pop(context);
                                                  Navigator.pop(context);
                                                }),
                                            CustomButton(
                                                dividerH: 25,
                                                dividerW: 20,
                                                fontSize: 110,
                                                text: 'No',
                                                onTap: () {
                                                  Navigator.pop(context);
                                                })
                                          ],
                                        ),
                                      );
                                    },
                                  );
                                }
                              },
                              child: Container(
                                width: screenWidth / 8,
                                height: screenHeight / 5,
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.circular(screenWidth * 0.01),
                                  border: Border.all(
                                      color: MyColors.blue, width: 1),
                                  gradient: LinearGradient(
                                    colors: [
                                      Colors.white,
                                      Colors.grey.shade200
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                                child: Column(
                                  children: [
                                    SizedBox(height: screenHeight / 45),
                                    Text(
                                      'Bus',
                                      style: TextStyle(
                                        color: MyColors.blue,
                                        fontSize: screenWidth * 0.010,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Image.asset(
                                      'images/smallbus.png',
                                      color: MyColors.blue,
                                      width: screenWidth / 20,
                                    ),
                                    if(state is GetVehiclesLoading)
                                      const LinearProgressIndicator(color: MyColors.blue,)else
                                    Text(transporterCubit.date == null ? 'Select Date' :
                                      '${vehicles?.availableV2}/${vehicles?.v2}',
                                      style: TextStyle(
                                        color: MyColors.blue,
                                        fontSize: screenWidth * 0.011,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                if (vehicles?.availableV3 == 0) {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text(
                                          'Vehicle is not Available',
                                          style: TextStyle(
                                              fontSize: screenWidth * 0.010),
                                        ),
                                        content: CustomButton(
                                            dividerH: 25,
                                            dividerW: 30,
                                            fontSize: 110,
                                            text: 'Close',
                                            onTap: () {
                                              Navigator.pop(context);
                                            }),
                                      );
                                    },
                                  );
                                } else if (transporterCubit.date == null) {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text(
                                          'Select Date First !',
                                          style: TextStyle(
                                              fontSize: screenWidth * 0.010),
                                        ),
                                        content: CustomButton(
                                            dividerH: 25,
                                            dividerW: 30,
                                            fontSize: 110,
                                            text: 'Close',
                                            onTap: () {
                                              Navigator.pop(context);
                                            }),
                                      );
                                    },
                                  );
                                } else {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text(
                                          'Are You Sure You Want to Pick Van ?',
                                          style: TextStyle(
                                              fontSize: screenWidth * 0.010),
                                        ),
                                        content: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            CustomButton(
                                                dividerH: 25,
                                                dividerW: 20,
                                                fontSize: 110,
                                                text: 'Yes',
                                                onTap: () {
                                                  transporterCubit.vehicleController.text='van';
                                                  Navigator.pop(context);
                                                  Navigator.pop(context);
                                                }),
                                            CustomButton(
                                                dividerH: 25,
                                                dividerW: 20,
                                                fontSize: 110,
                                                text: 'No',
                                                onTap: () {
                                                  Navigator.pop(context);
                                                })
                                          ],
                                        ),
                                      );
                                    },
                                  );
                                }
                              },
                              child: Container(
                                width: screenWidth / 8,
                                height: screenHeight / 5,
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.circular(screenWidth * 0.01),
                                  border: Border.all(
                                      color: MyColors.blue, width: 1),
                                  gradient: LinearGradient(
                                    colors: [
                                      Colors.white,
                                      Colors.grey.shade200
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                                child: Column(
                                  children: [
                                    SizedBox(height: screenHeight / 45),
                                    Text(
                                      'Van',
                                      style: TextStyle(
                                        color: MyColors.blue,
                                        fontSize: screenWidth * 0.010,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Image.asset(
                                      'images/van.png',
                                      color: MyColors.blue,
                                      width: screenWidth / 20,
                                    ),
                                    if(state is GetVehiclesLoading)
                                      const LinearProgressIndicator(color: MyColors.blue,)else
                                    Text(transporterCubit.date == null ? 'Select Date' :
                                      '${vehicles?.availableV3}/${vehicles?.v3}',
                                      style: TextStyle(
                                        color: MyColors.blue,
                                        fontSize: screenWidth * 0.011,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: screenHeight / 10,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
