import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_restaurant.dart';
import 'package:travelia_dashboard/components/drawers/custom_transporter_drawer.dart';
import 'package:travelia_dashboard/cubits/restaurant_cubit/restaurant_cubit.dart';
import 'package:pretty_animated_buttons/pretty_animated_buttons.dart';
import 'package:travelia_dashboard/cubits/transporter_cubit/transporter_cubit.dart';
import '../../constants/api.dart';
import '../../constants/my_colors.dart';

class ReservationForTransporter extends StatelessWidget {
  const ReservationForTransporter({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    final TransporterCubit transporterCubit =
    BlocProvider.of<TransporterCubit>(context);
    final reservations =
        transporterCubit.reservationModel?.reservations;
    return BlocConsumer<TransporterCubit, TransporterState>(
      builder: (context, state) => Scaffold(
        appBar: CustomAppBar(
          title: 'Reservations',
        ),
        drawer: const CustomDrawerTransporter(),
        body: Stack(
          fit: StackFit.expand,
          children: [
            Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/background3.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Center(
              child: Container(
                width: screenWidth / 1.6,
                height: screenHeight / 1.3,
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Colors.grey),
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.white.withOpacity(0.8),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: reservations == null || reservations.isEmpty
                      ? Center(
                    child: Text(
                      'No reservations available.',
                      style: TextStyle(
                        color: MyColors.blue,
                        fontSize: screenWidth * 0.015,
                      ),
                    ),
                  )
                      : ListView.builder(
                    itemCount: reservations.length,
                    itemBuilder: (context, index) {
                      return Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                          side: BorderSide(
                            color: MyColors.blue.withOpacity(0.3),
                            width: 2,
                          ),
                        ),
                        elevation: 1,
                        child: Container(decoration: BoxDecoration(border: Border.all(width:1,color: MyColors.blue),
                          gradient: LinearGradient(
                            colors: [Colors.white, Colors.grey.shade200],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                          borderRadius: BorderRadius.circular(15),
                        ),
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: screenHeight / 35,
                                          left: screenWidth / 25),
                                      child: Container(
                                        width: screenWidth / 15,
                                        height: screenWidth / 15,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                          BorderRadius.circular(screenWidth/200),
                                          border: Border.all(
                                              color: MyColors.blue.withOpacity(.8),
                                              width: 2.6),
                                          image: DecorationImage(
                                            image: NetworkImage(
                                                '${Api.imgUrl}/${reservations[index].photo}'),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: screenWidth * 0.05),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            '${reservations[index].name}',
                                            style: TextStyle(
                                              color: MyColors.blue,
                                              fontSize: screenWidth * 0.012,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          const SizedBox(height: 5),
                                          Row(
                                            children: [
                                              const Icon(Icons.phone,
                                                  color: MyColors.blue),
                                              const SizedBox(width: 5),
                                              Text(
                                                'Phone: ${reservations[index].phone}',
                                                style: TextStyle(
                                                  color: MyColors.blue,
                                                  fontSize:
                                                  screenWidth * 0.011,
                                                ),
                                              ),
                                            ],
                                          ),
                                          const SizedBox(height: 5),
                                          Row(
                                            children: [
                                              const Icon(
                                                  Icons.location_on,
                                                  color: MyColors.blue),
                                              const SizedBox(width: 5),
                                              Text(
                                                'Address: ${reservations[index].address}',
                                                style: TextStyle(
                                                  color: MyColors.blue,
                                                  fontSize:
                                                  screenWidth * 0.011,
                                                ),
                                              ),
                                            ],
                                          ),
                                          const SizedBox(height: 5),
                                          Row(
                                            children: [
                                              const Icon(Icons.money,
                                                  color: MyColors.blue),
                                              const SizedBox(width: 5),
                                              Text(
                                                'Reservation Cost: ${reservations[index].cost?.round()}',
                                                style: TextStyle(
                                                  color: MyColors.blue,
                                                  fontSize:
                                                  screenWidth * 0.011,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          right: screenWidth / 100,
                                          top: screenHeight / 15),
                                      child: CustomButton(
                                          dividerW: 10,
                                          dividerH: 20,
                                          fontSize: 100,
                                          text: 'Details',
                                          onTap: () {
                                            showDialog(
                                              context: context,
                                              builder: (context) =>
                                                  AlertDialog(
                                                    content: Center(
                                                      child: Column(
                                                        mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                        children: [
                                                          Text(
                                                            'User Details',
                                                            style: TextStyle(
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold,
                                                              color: MyColors
                                                                  .blue,
                                                              fontSize:
                                                              screenWidth *
                                                                  0.012,
                                                            ),
                                                          ),
                                                          Container(
                                                            width: screenWidth /
                                                                15,
                                                            height:
                                                            screenWidth /
                                                                15,
                                                            decoration:
                                                            BoxDecoration(
                                                              borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                  10),
                                                              border: Border.all(
                                                                  color:
                                                                  MyColors
                                                                      .blue,
                                                                  width: 2),
                                                              image:
                                                              DecorationImage(
                                                                image: NetworkImage(
                                                                    '${Api.imgUrl}/${reservations[index].photo}'),
                                                                fit: BoxFit
                                                                    .cover,
                                                              ),
                                                            ),
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                            children: [
                                                              Text(
                                                                'Name:',
                                                                style:
                                                                TextStyle(
                                                                  fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                                  color:
                                                                  MyColors
                                                                      .blue,
                                                                  fontSize:
                                                                  screenWidth *
                                                                      0.011,
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                  width:
                                                                  screenWidth /
                                                                      100),
                                                              Flexible(
                                                                child: Text(
                                                                  reservations[
                                                                  index]
                                                                      .name!,
                                                                  style:
                                                                  TextStyle(
                                                                    color: MyColors
                                                                        .blue,
                                                                    fontSize:
                                                                    screenWidth *
                                                                        0.011,
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                            children: [
                                                              Text(
                                                                'Email:',
                                                                style:
                                                                TextStyle(
                                                                  fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                                  color:
                                                                  MyColors
                                                                      .blue,
                                                                  fontSize:
                                                                  screenWidth *
                                                                      0.011,
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                  width:
                                                                  screenWidth /
                                                                      100),
                                                              Flexible(
                                                                child: Text(
                                                                  reservations[
                                                                  index]
                                                                      .email!,
                                                                  style:
                                                                  TextStyle(
                                                                    color: MyColors
                                                                        .blue,
                                                                    fontSize:
                                                                    screenWidth *
                                                                        0.011,
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                            children: [
                                                              Text(
                                                                'Phone:',
                                                                style:
                                                                TextStyle(
                                                                  fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                                  color:
                                                                  MyColors
                                                                      .blue,
                                                                  fontSize:
                                                                  screenWidth *
                                                                      0.011,
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                  width:
                                                                  screenWidth /
                                                                      100),
                                                              Flexible(
                                                                child: Text(
                                                                  reservations[
                                                                  index]
                                                                      .phone
                                                                      .toString(),
                                                                  style:
                                                                  TextStyle(
                                                                    color: MyColors
                                                                        .blue,
                                                                    fontSize:
                                                                    screenWidth *
                                                                        0.011,
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                            children: [
                                                              Text(
                                                                'Age:',
                                                                style:
                                                                TextStyle(
                                                                  fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                                  color:
                                                                  MyColors
                                                                      .blue,
                                                                  fontSize:
                                                                  screenWidth *
                                                                      0.011,
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                  width:
                                                                  screenWidth /
                                                                      100),
                                                              Flexible(
                                                                child: Text(
                                                                  reservations[
                                                                  index]
                                                                      .age
                                                                      .toString(),
                                                                  style:
                                                                  TextStyle(
                                                                    color: MyColors
                                                                        .blue,
                                                                    fontSize:
                                                                    screenWidth *
                                                                        0.011,
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                            children: [
                                                              Text(
                                                                'Address:',
                                                                style:
                                                                TextStyle(
                                                                  fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                                  color:
                                                                  MyColors
                                                                      .blue,
                                                                  fontSize:
                                                                  screenWidth *
                                                                      0.011,
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                  width:
                                                                  screenWidth /
                                                                      100),
                                                              Flexible(
                                                                child: Text(
                                                                  reservations[
                                                                  index]
                                                                      .address!,
                                                                  style:
                                                                  TextStyle(
                                                                    color: MyColors
                                                                        .blue,
                                                                    fontSize:
                                                                    screenWidth *
                                                                        0.011,
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          ElevatedButton(
                                                            style:
                                                            ElevatedButton
                                                                .styleFrom(
                                                              backgroundColor:
                                                              MyColors
                                                                  .blue,
                                                              shape:
                                                              RoundedRectangleBorder(
                                                                borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                    12),
                                                              ),
                                                            ),
                                                            onPressed: () {
                                                              Navigator.pop(
                                                                  context);
                                                            },
                                                            child: const Text(
                                                              'Close',
                                                              style:
                                                              TextStyle(
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                            );
                                          }),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      listener: (BuildContext context, TransporterState state) {},
    );
  }
}
