import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/components/dialogs.dart';
import 'package:travelia_dashboard/components/drawers/custom_transporter_drawer.dart';
import 'package:travelia_dashboard/constants/cache_service.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/transporter_cubit/transporter_cubit.dart';
import 'package:travelia_dashboard/views/home/transporter_home_screen.dart';
import 'package:travelia_dashboard/views/shared/my_map.dart';
import 'package:travelia_dashboard/views/transporter/choose_vehicle.dart';

import 'choose_vehicle_air.dart';

class CreateRoute extends StatelessWidget {
  const CreateRoute({super.key});

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final TransporterCubit transporterCubit = BlocProvider.of<TransporterCubit>(context);

    return BlocConsumer<TransporterCubit,TransporterState>(
      builder: (context, state) {
        return
        Form(
          key: transporterCubit.createRouteKey,
          child: Scaffold(
            drawer: const CustomDrawerTransporter(),
            appBar: CustomAppBar(title: 'Create Route',),
            body: Stack(
              fit: StackFit.expand,
              children: [
                Container(
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('images/background3.jpg'),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Image.asset(
                        'images/earth.png',
                        width: screenWidth / 2,
                        scale: 3,
                      ),
                    ),
                    Expanded(
                      child: Center(
                        child: Column(
                          children: [Padding(
                            padding:  EdgeInsets.only(right: screenWidth/20),
                            child: Image.asset('images/logo4.png',width: screenWidth/11,),
                          ),
                            const SizedBox(height: 10),
                            Padding(
                              padding: EdgeInsets.only(right: screenWidth / 12),
                              child: Text(
                                'Craft the perfect journey. Your next unforgettable experience starts here.',
                                style: TextStyle(
                                  fontSize: screenWidth * 0.011,
                                  color: MyColors.blue,
                                ),
                              ),
                            ),
                            const SizedBox(height: 35),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IgnorePointer(
                                  child: CustomTextFormFiled(
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Please Select The Start Location';
                                      }
                                      return null;
                                    },
                                    preIcon: const Icon(
                                      Icons.location_on_sharp,
                                      color: Colors.grey,
                                    ),
                                    obscureText: false,
                                    labelText: 'Start Location',
                                    controller: transporterCubit.transporterStartController,
                                    divider: 3.5,
                                    labelColor: Colors.grey,
                                    color: Colors.grey,
                                  ),
                                ),
                                const SizedBox(width: 20),
                                CustomButton(
                                  fontSize: 110,
                                  text: 'Set Location',
                                  onTap: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => const MyMap(access: 3),));
                                  },
                                  dividerH: 16,
                                  dividerW: 14,
                                ),
                              ],
                            ),
                             SizedBox(height:screenWidth/28),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IgnorePointer(
                                  child: CustomTextFormFiled(
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Please Select a Destination';
                                      }
                                      return null;
                                    },
                                    preIcon: const Icon(
                                      Icons.travel_explore,
                                      color: Colors.grey,
                                    ),
                                    obscureText: false,
                                    labelText: 'Destination',
                                    controller: transporterCubit.transporterEndController,
                                    divider: 3.5,
                                    labelColor: Colors.grey,
                                    color: Colors.grey,
                                  ),
                                ),
                                const SizedBox(width: 20),
                                CustomButton(
                                  fontSize: 110,
                                  text: 'Set Destination',
                                  onTap: () async {
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => const MyMap(access: 4),));

                                  },
                                  dividerH: 16,
                                  dividerW: 14,
                                ),
                              ],
                            ),
                             SizedBox(height: screenWidth/28),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IgnorePointer(
                                  child: CustomTextFormFiled(
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Please Select a Vehicle';
                                      }
                                      return null;
                                    },
                                    preIcon: const Icon(
                                      Icons.directions_bus_filled,
                                      color: Colors.grey,
                                    ),
                                    obscureText: false,
                                    controller: transporterCubit.vehicleController,
                                    labelText: 'Vehicle',
                                    divider: 3.5,
                                    labelColor: Colors.grey,
                                    color: Colors.grey,
                                  ),
                                ),
                                const SizedBox(width: 20),
                                CustomButton(
                                  fontSize: 110,
                                  text: 'Set Vehicle',
                                  onTap: ()  {
                                    if(CacheService.getData(key: 'type')=='air'&&CacheService.getData(key: 'role_id')==5)
                                      {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => const ChooseVehicleAir(),));

                                      }
                                    if(CacheService.getData(key: 'type')=='land'&&CacheService.getData(key: 'role_id')==5) {
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => const ChooseVehicle(),));
                                    }
                                  },
                                  dividerH: 16,
                                  dividerW: 14,
                                ),
                              ],
                            ),
                             SizedBox(height: screenWidth/35),
                            if(state is RouteCreateLoading)
                              const CircularProgressIndicator(color: MyColors.blue,)
                            else
                            Padding(
                              padding: EdgeInsets.only(right: screenWidth / 11),
                              child: CustomButton(fontSize: 110,
                                text: 'Create Route',
                                onTap: () async {
                                if(transporterCubit.createRouteKey.currentState!.validate())
                                  {
                                    await transporterCubit.createRoute();
                                  }

                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      }, listener: (BuildContext context, TransporterState state) {
        if(state is RouteCreateSuccess)
          {
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text(
                      'Route Created :) , Stay Safe on The Road',
                      style: TextStyle(
                          fontSize: screenWidth * 0.010),
                    ),
                    content: CustomButton(
                        dividerH: 25,
                        dividerW: 30,
                        fontSize: 110,
                        text: 'Home Page',
                        onTap: () async {
                          await transporterCubit.getTransporterRoutes();
                          await transporterCubit.getTransporterReservationsByOrganizers();
                          transporterCubit.date=null;
                          transporterCubit.transporterStartController.clear();
                          transporterCubit.dateController.clear();
                          transporterCubit.transporterEndController.clear();
                          transporterCubit.vehicleController.clear();
                          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => const TransporterHomeScreen(),), (route) => false,);

                        }),
                  );
                },
              );
            }
        if(state is RouteCreateFailure)
          {
            Dialogs.showFailedDialog(context, "SomeThing Went Wrong :(");
          }
          }

    );
  }
}
