import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';

class FullScreenImageBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        // Ensure the stack fills the whole screen
        children: <Widget>[
          // Background image with blur
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/background.jpg'), // Replace with your image
                fit: BoxFit.cover, // Cover the entire screen
              ),
            ),

          ),
          // Your widgets go here

       Center(
         child: Column(
           children: [
             Image.asset('images/logo4.png',scale: 3,),
             SizedBox(height: 30,),
             CustomTextFormFiled(obscureText: false,labelText: 'Description',)
           ],
           
         ),
       ) ],
      ),
    );
  }
}
