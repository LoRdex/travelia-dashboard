import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/components/dialogs.dart';
import 'package:travelia_dashboard/constants/cache_service.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/admin_cubit/admin_cubit.dart';
import 'package:travelia_dashboard/cubits/auth_cubit/auth_cubit.dart';
import 'package:travelia_dashboard/cubits/hotel_cubit/hotel_cubit.dart';
import 'package:travelia_dashboard/cubits/restaurant_cubit/restaurant_cubit.dart';
import 'package:travelia_dashboard/cubits/transporter_cubit/transporter_cubit.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/views/auth/register_screen.dart';
import 'package:travelia_dashboard/views/facility_create/facility_setup_screen.dart';
import 'package:travelia_dashboard/views/home/admin_home_screen.dart';
import 'package:travelia_dashboard/views/home/hotel_home_screen.dart';
import 'package:travelia_dashboard/views/home/organizer_home_screen.dart';
import 'package:travelia_dashboard/views/home/restaurant_home_screen.dart';

import '../../cubits/main_cubit/main_cubit.dart';
import '../home/transporter_home_screen.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);
    final authCubit = BlocProvider.of<AuthCubit>(context);
    final mainCubit = BlocProvider.of<MainCubit>(context);
    final transporterCubit = BlocProvider.of<TransporterCubit>(context);
    final hotelCubit = BlocProvider.of<HotelCubit>(context);
    final adminCubit = BlocProvider.of<AdminCubit>(context);
    final restaurantCubit = BlocProvider.of<RestaurantCubit>(context);

    return BlocConsumer<AuthCubit, AuthState>(
      builder: (context, state) => Form(
        key: authCubit.formKey,
        child: Scaffold(
            backgroundColor: MyColors.scaffoldColor,
            body: Stack(
              children: [
                Container(
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('images/background3.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Image.asset(
                        'images/login1.png',
                        width: MediaQuery.of(context).size.width / 2,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 12),
                            child: Text(
                              'Welcome to',
                              style: TextStyle(
                                  fontSize:
                                      MediaQuery.of(context).size.width * 0.03,
                                  color: MyColors.blue),
                            ),
                          ),
                          Image.asset(
                            'images/logo4.png',
                            width: MediaQuery.of(context).size.width / 10,
                          ),
                          const SizedBox(
                            height: 50,
                          ),
                          CustomTextFormFiled(
                            validator: (value) {
                              if (authCubit.emailValidator(value!)) {
                                return null;
                              } else {
                                return 'Please enter a valid email';
                              }
                            },
                            controller: authCubit.emailController,
                            maxLines: 1,
                            obscureText: false,
                            labelText: 'Email',
                            preIcon: const Icon(
                              Icons.email,
                              color: MyColors.blue,
                            ),
                          ),
                          const SizedBox(
                            height: 60,
                          ),
                          CustomTextFormFiled(
                            maxLines: 1,
                            validator: (value) {
                              if (authCubit.passwordValidator(value!)) {
                                return null;
                              } else {
                                return 'Password must be at least 8 characters long';
                              }
                            },

                            controller: authCubit.passwordController,
                            obscureText: authCubit.isVisible,
                            labelText: 'Password',

                            preIcon: const Icon(
                              Icons.lock,
                              color: MyColors.blue,
                            ),
                            suffixIcon: IconButton(
                              onPressed: () {
                                authCubit.showPassword();
                              },
                              icon: Icon(
                                authCubit.passwordIcon,
                                color: MyColors.blue,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 55,
                          ),
                          if (state is LoginLoadingState)
                            const CircularProgressIndicator(
                              color: MyColors.blue,
                            )
                          else
                            CustomButton(
                                text: 'Login',
                                onTap: () async {
                                  if (authCubit.formKey.currentState!
                                      .validate()) {
                                    authCubit.login(
                                        email: authCubit.emailController.text,
                                        password:
                                            authCubit.passwordController.text);
                                  }
                                }),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "don't have an Account ?",
                                style: TextStyle(
                                    fontSize:
                                        MediaQuery.of(context).size.width *
                                            0.01),
                              ),
                              TextButton(
                                  onPressed: () {
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              const RegisterScreen(),
                                        ),
                                        (route) => false);
                                  },
                                  child: Text(
                                    'Register',
                                    style: TextStyle(
                                        color: MyColors.blue,
                                        fontSize:
                                            MediaQuery.of(context).size.width *
                                                0.01),
                                  )),
                              Text(
                                'Your Company',
                                style: TextStyle(
                                    fontSize:
                                        MediaQuery.of(context).size.width *
                                            0.01),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            )),
      ),
      listener: (context, state) async {
        if (state is LoginSuccessState) {
          if (CacheService.getData(key: 'confirmation') == "1" &&
              CacheService.getData(key: 'role_id') != 1) {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    FacilitySetupScreen(CacheService.getData(key: 'role_id')),
              ),
              (route) => false,
            );
          }

          if (CacheService.getData(key: 'role_id') == 1) {
            await adminCubit.getAccountsForConfirmation();
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => const AdminHomeScreen(),
              ),
              (route) => false,
            );
          }

          if (CacheService.getData(key: 'confirmation') == "2" &&
              CacheService.getData(key: 'role_id') == 2) {
            await tripOrganizerCubit.getOrganizedTrips();
            await mainCubit.getProfile();
            await mainCubit.getFinancialLog();
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => const OrganizerHomeScreen(),
              ),
              (route) => false,
            );
          }

          if (CacheService.getData(key: 'confirmation') == "2" &&
              CacheService.getData(key: 'role_id') == 4) {
            await mainCubit.getProfile();
            await mainCubit.getFinancialLog();
            await restaurantCubit.getRestaurantReservations();
            await restaurantCubit.getRestaurantReservationsByOrganizers();
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => const RestaurantHomeScreen(),
              ),
              (route) => false,
            );
          }

          if (CacheService.getData(key: 'confirmation') == "2" &&
              CacheService.getData(key: 'role_id') == 3) {
            await mainCubit.getProfile();
            await mainCubit.getFinancialLog();
            await hotelCubit.getHotelReservations();
            await hotelCubit.getHotelReservationsByOrganizers();
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => const HotelHomeScreen(),
              ),
              (route) => false,
            );
          }

          if (CacheService.getData(key: 'confirmation') == "2" &&
              CacheService.getData(key: 'role_id') == 5) {
            await mainCubit.getProfile();
            await mainCubit.getFinancialLog();
            await transporterCubit.getTransporterRoutes();
            await transporterCubit.getTransporterReservationsByOrganizers();
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => const TransporterHomeScreen(),
              ),
              (route) => false,
            );
          }

          if (CacheService.getData(key: ('confirmation')) == "3") {
            Dialogs.showFailedDialog(context,
                'Your Account Rejected and Banned From Using Travelia App , Contact Support For More Info');
          }

          if (CacheService.getData(key: 'confirmation') != "3" &&
              CacheService.getData(key: 'confirmation') != "0") {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                backgroundColor: MyColors.scaffoldColor,
                content: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Signed in Successfully',
                      style: TextStyle(color: MyColors.blue),
                    ),
                    Text(
                      'Welcome to Travelia',
                      style: TextStyle(color: MyColors.blue),
                    )
                  ],
                ),
                duration: Duration(seconds: 4),
              ),
            );
          }

          if (CacheService.getData(key: 'confirmation') == "0" &&CacheService.getData(key: 'role_id') != 1 ) {
            Dialogs.showFailedDialog(context,
                "Your Account is Under Review By admins , It Won't Take Long Time , Thanks for Your Patience");
          }
        }
        if (state is LoginFailureState) {
          Dialogs.showFailedDialog(
              context, "Something Went Wrong, Please Try Again");
        }
      },
    );
  }
}
