import 'package:flutter/material.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

class CustomHalfScreen extends StatelessWidget {
  final String imagePATH;

  const CustomHalfScreen({super.key, required this.imagePATH});



  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: AspectRatio(
        aspectRatio: 1 / 2,
        child: WidgetAnimator(
          incomingEffect:
          WidgetTransitionEffects.incomingSlideInFromBottom(
              duration: const Duration(seconds: 2)),
          child: Image.asset(
            imagePATH,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}