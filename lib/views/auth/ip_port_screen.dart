import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/constants/api.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/auth_cubit/auth_cubit.dart';
import 'package:travelia_dashboard/views/auth/login_screen.dart';

class IpPortScreen extends StatelessWidget {
  const IpPortScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final authCubit = BlocProvider.of<AuthCubit>(context);
    final screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    final screenHeight = MediaQuery
        .of(context)
        .size
        .height;
    return Form(key: authCubit.ipPortKey,
      child: Scaffold(
        body: Stack(
          fit: StackFit.expand,
          children: [
            Container(
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('images/background5.jpg'),
                      fit: BoxFit.fill)),
            ),
            Center(
              child: Column(
                children: [
                  SizedBox(height: screenHeight / 35,),

                  Image.asset('images/logo4.png', width: screenWidth / 7,),
                  SizedBox(height: screenHeight / 35,),

                  Text(
                    'Please Set The Desired Ip and Port Or Continue With Default One',
                    style: TextStyle(
                        color: MyColors.blue, fontSize: screenWidth * 0.013),
                  ),
                  SizedBox(height: screenHeight / 15,),
                  CustomTextFormFiled(validator: (value) {
                    if (value == null || value.isEmpty)
                      {
                        return 'Required Field ';

                      } else {
                      return null;
                    }
                  },
                      controller: authCubit.ipController,
                      obscureText: false,
                      labelText: 'Ip',
                      maxLines: 1,
                      divider: 5,
                      preIcon: const Icon(
                        Icons.numbers, color: MyColors.blue,)),
                  SizedBox(height: screenHeight / 20,),
                  CustomTextFormFiled(controller: authCubit.portController,
                    obscureText: false,
                    validator: (value) {
                      if (value == null || value.isEmpty)
                      {
                        return 'Required Field ';

                      } else {
                        return null;
                      }
                    },
                    labelText: 'Port',
                    maxLines: 1,
                    divider: 5,
                    preIcon: const Icon(Icons.numbers, color: MyColors.blue,),),
                  SizedBox(height: screenHeight / 10,),
                  CustomButton(text: 'Set', onTap: () {
                    if(authCubit.ipPortKey.currentState!.validate())
                      {
                        Api.ip=authCubit.ipController.text;
                        Api.port=authCubit.portController.text;
                        Navigator.pushAndRemoveUntil(
                          context, createRoute(), (route) => false,);


                      }
                  }, fontSize: 120,),
                  SizedBox(height: screenHeight / 20,),
                  CustomButton(
                    text: 'Continue', color: MyColors.blueCyan, onTap: () {
                    Navigator.pushAndRemoveUntil(
                      context, createRoute(), (route) => false,);
                  }, fontSize: 120,),


                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

Route createRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation,
        secondaryAnimation) => const LoginScreen(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(0, 2);
      const end = Offset.zero;
      final tween = Tween(begin: begin, end: end).chain(
          CurveTween(curve: Curves.slowMiddle));

      final offsetAnimation = animation.drive(tween);
      return SlideTransition(position: offsetAnimation, child: child);
    },
  );
}

