import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/components/dialogs.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/auth_cubit/auth_cubit.dart';
import 'package:travelia_dashboard/views/auth/login_screen.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';
import 'package:lottie/lottie.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    final authCubit = BlocProvider.of<AuthCubit>(context);
    return BlocConsumer<AuthCubit, AuthState>(
      builder: (context, state) => Scaffold(
        backgroundColor: MyColors.scaffoldColor,
        body: Form(
            key: authCubit.regFormKey,
            child: Stack(
              children: [
                Container(
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('images/background3.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 5,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              IconButton(
                                onPressed: () {
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            const LoginScreen(),
                                      ));
                                  authCubit.imageFile = null;
                                },
                                icon: const Icon(Icons.arrow_back_ios_new),
                                color: MyColors.blue,
                              ),
                              const SizedBox(),
                              Image.asset(
                                'images/logo4.png',
                                width: screenWidth / 10,
                              ),
                              SizedBox(
                                width: screenWidth / 8.5,
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width / 5.9,
                                  child: CustomTextFormFiled(
                                    controller: authCubit.firstNameController,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "Last Name is Required";
                                      }
                                      return null;
                                    },
                                    labelText: 'First Name',
                                    obscureText: false,
                                    preIcon: const Icon(
                                      Icons.person,
                                      color: MyColors.blue,
                                    ),
                                  )),
                              const SizedBox(
                                width: 30,
                              ),
                              SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width / 5.9,
                                  child: CustomTextFormFiled(
                                    controller: authCubit.lastNameController,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "Last Name is Required";
                                      }
                                      return null;
                                    },
                                    labelText: 'Last Name',
                                    obscureText: false,
                                    preIcon: const Icon(
                                      Icons.person,
                                      color: MyColors.blue,
                                    ),
                                  ))
                            ],
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          SizedBox(
                              width: MediaQuery.of(context).size.width / 2.8,
                              child: CustomTextFormFiled(
                                controller: authCubit.regEmailController,
                                validator: (value) {
                                  if (authCubit.emailValidator(value!)) {
                                    return null;
                                  } else {
                                    return 'Please enter a valid email';
                                  }
                                },
                                labelText: 'Email',
                                obscureText: false,
                                preIcon: const Icon(
                                  Icons.email,
                                  color: MyColors.blue,
                                ),
                              )),
                          const SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                              width: MediaQuery.of(context).size.width / 2.8,
                              child: CustomTextFormFiled(
                                maxLines: 1,
                                controller: authCubit.regPasswordController,
                                validator: (value) {
                                  if (authCubit.passwordValidator(value!)) {
                                    return null;
                                  } else {
                                    return 'Password must be at least 8 characters long';
                                  }
                                },
                                labelText: 'Password',
                                suffixIcon: IconButton(
                                  onPressed: () {
                                    authCubit.showPassword();
                                  },
                                  icon: Icon(
                                    authCubit.passwordIcon,
                                    color: MyColors.blue,
                                  ),
                                ),
                                obscureText: authCubit.isVisible,
                                preIcon: const Icon(
                                  Icons.lock,
                                  color: MyColors.blue,
                                ),
                              )),
                          const SizedBox(
                            height: 15,
                          ),
                          Column(
                            children: UserRole.values.map((role) {
                              return SizedBox(
                                width: MediaQuery.of(context).size.width / 2.7,
                                height: 32,
                                child: Theme(
                                  data: ThemeData(
                                      splashColor: MyColors.scaffoldColor,
                                      hoverColor: MyColors.scaffoldColor),
                                  child: RadioListTile(
                                    controlAffinity:
                                        ListTileControlAffinity.trailing,
                                    activeColor: MyColors.blue,
                                    title: Text(
                                      authCubit.enumToString(role),
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.01),
                                    ),
                                    value: role,
                                    secondary: Icon(
                                      color: MyColors.blue,
                                      role == UserRole.restaurant
                                          ? Icons.restaurant
                                          : role == UserRole.hotel
                                              ? Icons.hotel
                                              : role == UserRole.tripOrganizer
                                                  ? Icons.flight_takeoff
                                                  : Icons.directions_car,
                                    ),
                                    groupValue: authCubit.roleSelected,
                                    onChanged: (UserRole? value) {
                                      authCubit.selectRole(value!);
                                    },
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          if (state is ImageLoading)
                            Lottie.asset('lotties/loading.json', width: 50)
                          else if (authCubit.imageFile != null)
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Lottie.asset(
                                    'lotties/success1.json',
                                    width: 50,
                                    repeat: true,
                                  ),
                                  CustomButton(
                                      color: Colors.red,
                                      text: 'Remove',
                                      fontSize: 120,
                                      dividerH: 20,
                                      dividerW: 18,
                                      onTap: () {
                                        authCubit.imageRemoved();
                                      }),
                                ])
                          else
                            GestureDetector(
                              onTap: () {
                                authCubit.pickImage();
                              },
                              child: Container(
                                height: MediaQuery.of(context).size.height / 16,
                                width: MediaQuery.of(context).size.width / 9,
                                decoration: BoxDecoration(
                                  color: MyColors.blue,
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      WidgetAnimator(
                                        atRestEffect:
                                            WidgetRestingEffects.swing(
                                                numberOfPlays: 3),
                                        child: Icon(
                                          size: screenWidth * 0.016,
                                          Icons.add_a_photo,
                                          color: Colors.white,
                                        ),
                                      ),
                                      SizedBox(
                                        width: screenWidth * 0.01,
                                      ),
                                      Text(
                                        ' License ',
                                        style: TextStyle(
                                            fontSize: screenWidth * 0.01,
                                            color: Colors.white),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          if (state is ImageLoading)
                            const Text(
                              'Lower Than 512 Kb',
                              style: TextStyle(color: Colors.red, fontSize: 10),
                            ),
                          const SizedBox(
                            height: 20,
                          ),
                          if (state is RegisterLoadingState)
                            const CircularProgressIndicator(
                              color: MyColors.blue,
                            )
                          else
                            CustomButton(
                                text: 'Register',
                                onTap: () {
                                  if (authCubit.imageFile == null) {
                                    showDialog(
                                      context: context,
                                      builder: (context) => AlertDialog(
                                        content: SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              10,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Text(
                                                'Uploading Image is Required',
                                                style: TextStyle(
                                                    fontSize:
                                                        screenWidth * 0.01),
                                              ),
                                              TextButton(
                                                  onPressed: () {
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text(
                                                    'Close',
                                                    style: TextStyle(
                                                        fontSize:
                                                            screenWidth * 0.01,
                                                        color: Colors.black),
                                                  ))
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  }
                                  authCubit.regFormKey.currentState!.validate();
                                  authCubit.register(
                                      firstName:
                                          authCubit.firstNameController.text,
                                      lastName:
                                          authCubit.lastNameController.text,
                                      roleId: authCubit.roleValue,
                                      email: authCubit.regEmailController.text,
                                      password:
                                          authCubit.regPasswordController.text,
                                      imageFile: authCubit.imageFile!);
                                })
                        ],
                      ),
                    ),
                    Expanded(
                        child: WidgetAnimator(
                            incomingEffect:
                                WidgetTransitionEffects.incomingSlideInFromTop(
                                    duration: const Duration(seconds: 2)),
                            child: Image.asset(
                              'images/register.png',
                              width: MediaQuery.of(context).size.width / 2,
                            )))
                  ],
                ),
              ],
            )),
      ),
      listener: (context, state) {
        if (state is ImageWrongExt) {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              content: SizedBox(
                height: MediaQuery.of(context).size.height / 10,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      state.imgErrWrongExt,
                      style: const TextStyle(fontSize: 20),
                    ),
                    TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text(
                          'Close',
                          style: TextStyle(fontSize: 15, color: Colors.black),
                        ))
                  ],
                ),
              ),
            ),
          );
        }
        if (state is RegisterSuccessState) {
          showDialog(
              context: context,
              builder: (context) => AlertDialog(
                    content: SizedBox(
                      height: MediaQuery.of(context).size.height / 6,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            const Text(
                              "Registration Successful! Your account is currently under review by the Travelia Admins. \n Please try to sign in after 2 minutes. Thank you for your patience.",
                              style: TextStyle(fontSize: 20),
                            ),
                            TextButton(
                                onPressed: () {
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            const LoginScreen(),
                                      ));
                                  authCubit.imageFile = null;
                                },
                                child: const Text(
                                  'Close',
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.black),
                                ))
                          ],
                        ),
                      ),
                    ),
                  ));
        }
        if (state is RegisterFailureState) {
          Dialogs.showFailedDialog(
              context, "Something Went Wrong, Please Try Again");
          authCubit.imageFile = null;
        }
      },
    );
  }
}
