import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/cubits/hotel_cubit/hotel_cubit.dart';

import '../../components/custom_text_form_field.dart';
import '../../constants/my_colors.dart';
import '../../cubits/main_cubit/main_cubit.dart';

class AvailableRooms extends StatelessWidget {
  const AvailableRooms({super.key});

  @override
  Widget build(BuildContext context) {
    final MainCubit mainCubit = BlocProvider.of<MainCubit>(context);
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final HotelCubit hotelCubit = BlocProvider.of<HotelCubit>(context);


    return BlocConsumer<HotelCubit, HotelState>(
      builder: (context, state) {
        final rooms =  hotelCubit.availableRoomsModel;
        return
         Scaffold(

          drawer: mainCubit.getDrawer(),
          appBar: CustomAppBar(
            title: 'Available Rooms',
          ),
          body: Stack(
            fit: StackFit.expand,
            children: [
              Container(
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('images/background3.jpg'),
                        fit: BoxFit.fill)),
              )
              , Center(
                child: Container(
                  height: screenHeight / 1.1,
                  width: screenWidth / 1.1,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(screenWidth * 0.07),
                      border: Border.all(color: MyColors.blue, width: .5)),
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight / 5.5,
                      ),
                      Row(mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CustomTextFormFiled(
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "Date Field is Required";
                              } else {
                                return null;
                              }
                            },
                            divider: 6,
                            controller: hotelCubit.dateController,
                            obscureText: false,
                            readOnly: true,
                            preIcon: const Icon(
                              Icons.date_range_outlined,
                              color: MyColors.blue,
                            ),
                            labelText: 'Start Date',
                            onTap: () {
                              hotelCubit.pickStartDate(context);
                            },
                          ),
                          const SizedBox(width: 20,),
                          CustomTextFormFiled(
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "Date Field is Required";
                              } else {
                                return null;
                              }
                            },
                            divider: 6,
                            controller: hotelCubit.endController,
                            obscureText: false,
                            readOnly: true,
                            preIcon: const Icon(
                              Icons.date_range_outlined,
                              color: MyColors.blue,
                            ),
                            labelText: 'End Date',
                            onTap: () {
                              hotelCubit.pickEndDate(context);
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight / 12,
                      ),
                      Padding(
                        padding:
                        EdgeInsets.symmetric(horizontal: screenWidth / 6),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Container(
                              width: screenWidth / 8,
                              height: screenHeight / 5,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.circular(screenWidth * 0.01),
                                border: Border.all(
                                    color: MyColors.blue, width: 1),
                                gradient: LinearGradient(
                                  colors: [
                                    Colors.white,
                                    Colors.grey.shade200
                                  ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                ),
                              ),
                              child: Column(
                                children: [
                                  SizedBox(height: screenHeight / 45),
                                  Text(
                                    'Single Room',
                                    style: TextStyle(
                                      color: MyColors.blue,
                                      fontSize: screenWidth * 0.010,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Padding(
                                    padding:  EdgeInsets.symmetric(vertical: screenWidth/100),
                                    child: Image.asset(
                                      'images/singlebed.png',
                                      color: MyColors.blue,
                                      width: screenWidth / 40,
                                    ),
                                  ),
                                  if(state is GetRoomsLoading)
                                    const LinearProgressIndicator(color: MyColors.blue,)else
                                    Text(hotelCubit.endDate == null ? 'Select Date' :
                                    '${rooms?.availableRoomForOnePerson}/${rooms?.roomForOnePerson}',
                                      style: TextStyle(
                                        color: MyColors.blue,
                                        fontSize: screenWidth * 0.011,
                                      ),
                                    ),
                                ],
                              ),
                            ),
                            Container(
                              width: screenWidth / 8,
                              height: screenHeight / 5,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.circular(screenWidth * 0.01),
                                border: Border.all(
                                    color: MyColors.blue, width: 1),
                                gradient: LinearGradient(
                                  colors: [
                                    Colors.white,
                                    Colors.grey.shade200
                                  ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                ),
                              ),
                              child: Column(
                                children: [
                                  SizedBox(height: screenHeight / 45),
                                  Text(
                                    'Double Room',
                                    style: TextStyle(
                                      color: MyColors.blue,
                                      fontSize: screenWidth * 0.010,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Image.asset(
                                    'images/doublebed.png',
                                    color: MyColors.blue,
                                    width: screenWidth / 25,
                                  ),
                                  SizedBox(height: screenHeight/70,),
                                  if(state is GetRoomsLoading)
                                    const LinearProgressIndicator(color: MyColors.blue,)else
                                    Text(hotelCubit.endDate == null ? 'Select Date' :
                                    '${rooms?.availableRoomForTwoPerson}/${rooms?.roomForTwoPerson}',
                                      style: TextStyle(
                                        color: MyColors.blue,
                                        fontSize: screenWidth * 0.011,
                                      ),
                                    ),
                                ],
                              ),
                            ),
                            Container(
                              width: screenWidth / 8,
                              height: screenHeight / 5,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.circular(screenWidth * 0.01),
                                border: Border.all(
                                    color: MyColors.blue, width: 1),
                                gradient: LinearGradient(
                                  colors: [
                                    Colors.white,
                                    Colors.grey.shade200
                                  ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                ),
                              ),
                              child: Column(
                                children: [
                                  SizedBox(height: screenHeight / 45),
                                  Text(
                                    'Suite',
                                    style: TextStyle(
                                      color: MyColors.blue,
                                      fontSize: screenWidth * 0.010,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Image.asset(
                                    'images/private.png',
                                    color: MyColors.blue,
                                    width: screenWidth / 30,
                                  ),
                                  SizedBox(height: screenHeight/35,),
                                  if(state is GetRoomsLoading)
                                    const LinearProgressIndicator(color: MyColors.blue,)else
                                    Text(hotelCubit.endDate == null ? 'Select Date' :
                                    '${rooms?.availableSuite}/${rooms?.suite}',
                                      style: TextStyle(
                                        color: MyColors.blue,
                                        fontSize: screenWidth * 0.011,
                                      ),
                                    ),
                                ],
                              ),
                            ),

                          ],
                        ),
                      ),
                      SizedBox(
                        height: screenHeight / 10,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );

      },
      listener: (BuildContext context, HotelState state) {},
    );
  }
}
