import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_organizer.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';

import '../../constants/my_colors.dart';
import '../../cubits/main_cubit/main_cubit.dart';
import '../../cubits/trip_organizer_cubit/trip_organizer_cubit.dart';

class FinancialLogScreen extends StatelessWidget {
  const FinancialLogScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);
    final mainCubit = BlocProvider.of<MainCubit>(context);

    final financial = mainCubit.financialModel?.report;
    final totalBalance = mainCubit.financialModel?.totalBalance ?? 0;

    return BlocBuilder<MainCubit,MainState>(
      builder: (BuildContext context, MainState state) { return Scaffold(
        appBar: CustomAppBar(

          title: 'Financial Log',
        ),
        drawer: mainCubit.getDrawer(),
        body: Stack(
          fit: StackFit.expand,
          children: [
            Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/background3.jpg'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Center(
              child: Container(
                width: screenWidth / 1.2,
                height: screenHeight / 1.3,
                decoration: BoxDecoration(border: Border.all(width: 1,color: Colors.grey),
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.white.withOpacity(0.3),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      financial == null || financial.isEmpty
                          ? Expanded(
                        child: Center(
                          child: Text(
                            'No Reports available.',
                            style: TextStyle(
                              color: MyColors.blue,
                              fontSize: screenWidth * 0.015,
                            ),
                          ),
                        ),
                      )
                          : Expanded(
                        child: ListView.builder(
                          itemCount: financial.length,
                          itemBuilder: (context, index) {
                            return Container(
                              decoration: BoxDecoration(
                                border: Border.all(color: MyColors.blue),
                                borderRadius: BorderRadius.circular(screenWidth/15),
                                color: Colors.white,
                              ),
                              padding:  EdgeInsets.all(screenWidth/90),
                              margin:  EdgeInsets.only(bottom: screenWidth/90),
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        '${financial[index].description}',
                                        style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.012,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(width: screenWidth/4,),
                                      Text(
                                        '${financial[index].date}',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.010,fontWeight: FontWeight.bold),
                                      )
                                    ],
                                  ),
                                  const SizedBox(height: 8),
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                    children: [
                                      Text(
                                        'Before: ${financial[index].before}',
                                        style: TextStyle(
                                          color: Colors.red,
                                          fontSize: screenWidth * 0.011,
                                        ),
                                      ),
                                      Text(
                                        'InTake: ${financial[index].intake}',
                                        style: TextStyle(
                                            color: Colors.green,
                                            fontSize: screenWidth * 0.012,
                                            fontWeight: FontWeight.bold
                                        ),
                                      ),
                                      Text(
                                        'After: ${financial[index].after}',
                                        style: TextStyle(
                                          color: Colors.blue,
                                          fontSize: screenWidth * 0.011,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: MyColors.blue),
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,
                        ),
                        padding: const EdgeInsets.all(16),
                        margin: const EdgeInsets.only(top: 16),
                        child: Text(
                          'Total Balance: $totalBalance',
                          style: TextStyle(
                            color: MyColors.blue,
                            fontSize: screenWidth * 0.015,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ); },

    );
  }
}
