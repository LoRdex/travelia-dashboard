import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location_picker_flutter_map/location_picker_flutter_map.dart';
import 'package:travelia_dashboard/cubits/admin_cubit/admin_cubit.dart';
import 'package:travelia_dashboard/cubits/facility_create_cubits/facility_cubit.dart';
import 'package:travelia_dashboard/cubits/transporter_cubit/transporter_cubit.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

import '../../constants/my_colors.dart';

class MyMap extends StatelessWidget {
  const MyMap({super.key, required this.access});

  final int access;

  @override
  Widget build(BuildContext context) {
    final facilityCubit = BlocProvider.of<FacilityCubit>(context);
    final tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);
    final transporterCubit = BlocProvider.of<TransporterCubit>(context);
    final adminCubit = BlocProvider.of<AdminCubit>(context);
    return Scaffold(
      body: FlutterLocationPicker(
          markerIcon: const Icon(
            Icons.location_on_sharp,
            color: MyColors.blue,
            size: 45,
          ),
          searchBarBackgroundColor: Colors.white,
          locationButtonBackgroundColor: MyColors.blue,
          searchBarHintColor: MyColors.blue,
          mapLanguage: 'en',
          locationButtonsColor: MyColors.blue,
          searchBarTextColor: MyColors.blue,
          zoomButtonsBackgroundColor: MyColors.blue,
          initPosition: const LatLong(33, 36),
          selectLocationButtonStyle: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(MyColors.blue),
          ),
          selectedLocationButtonTextstyle:
              const TextStyle(fontSize: 18, color: Colors.white),
          selectLocationButtonText: 'Set Location',
          selectLocationButtonLeadingIcon: const Icon(
            Icons.check,
            color: Colors.white,
          ),
          initZoom: 11,
          minZoomLevel: 5,
          maxZoomLevel: 16,
          trackMyPosition: true,
          onError: (e) => print(e),
          onPicked: (pickedData) {
            print("  خط عرض   ${pickedData.latLong.latitude}");
            print("  هي طول ${pickedData.latLong.longitude}");
            print(" العنوان ${pickedData.address}");
            print("  البلد ${pickedData.addressData['country']}");
            print(" المحافظة  ${pickedData.addressData['state']}");
            print(" المدينة ${pickedData.addressData['city']}");
            print(" التاون ${pickedData.addressData['town']}");
            print("الناحية ${pickedData.addressData['county']}");
            showDialog(
                context: context,
                builder: (context) => AlertDialog(
                      content: SizedBox(
                        height: MediaQuery.of(context).size.height / 6,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              const Text(
                                "are you sure you picked location correctly ?",
                                style: TextStyle(fontSize: 20),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  TextButton(
                                      onPressed: () {
                                        if (access == 1) {
                                          print(
                                              'only for facility cubit values');
                                          facilityCubit.addressController.text =
                                              pickedData.address;
                                          facilityCubit.countryController.text =
                                              pickedData.addressData['country'];
                                          facilityCubit
                                                  .latitudeController.text =
                                              pickedData.latLong.latitude
                                                  .toString();
                                          facilityCubit
                                                  .longitudeController.text =
                                              pickedData.latLong.longitude
                                                  .toString();
                                          facilityCubit
                                              .stateController.text = pickedData
                                                  .addressData['state'] ??=
                                              pickedData.addressData['region'];
                                          facilityCubit
                                              .cityController.text = pickedData
                                                  .addressData['city'] ??=
                                              pickedData.addressData['town'] ??=
                                                  pickedData.addressData[
                                                          'county'] ??=
                                                      pickedData
                                                          .addressData['state'];
                                          facilityCubit.locationPicked();
                                        }
                                        if (access == 2) {
                                          print(
                                              'only for organizer cubit values');

                                          tripOrganizerCubit.longitude =
                                              pickedData.latLong.longitude;
                                          tripOrganizerCubit.latitude =
                                              pickedData.latLong.latitude;
                                          tripOrganizerCubit.address =
                                              pickedData.address;
                                          tripOrganizerCubit.country =
                                              pickedData.addressData['country'];
                                          tripOrganizerCubit.states = pickedData
                                                  .addressData['state'] ??=
                                              pickedData.addressData['region'];
                                          tripOrganizerCubit.city = pickedData
                                                  .addressData['city'] ??=
                                              pickedData.addressData['town'] ??=
                                                  pickedData.addressData[
                                                          'county'] ??=
                                                      pickedData
                                                          .addressData['state'];
                                          tripOrganizerCubit.locationPick();
                                        }
                                        if (access == 3) {
                                          print(
                                              'only for transporter start cubit values');

                                          transporterCubit.strLatitude =
                                              pickedData.latLong.latitude;
                                          transporterCubit.strLongitude =
                                              pickedData.latLong.longitude;
                                          transporterCubit.strCity = pickedData
                                                  .addressData['city'] ??=
                                              pickedData.addressData['town'] ??=
                                                  pickedData.addressData[
                                                          'county'] ??=
                                                      pickedData
                                                          .addressData['state'];
                                          transporterCubit.strCountry =
                                              pickedData.addressData['country'];
                                          transporterCubit.strState = pickedData
                                                  .addressData['state'] ??=
                                              pickedData.addressData['region'];
                                          transporterCubit.strAddress =
                                              pickedData.address;
                                          transporterCubit
                                                  .transporterStartController
                                                  .text =
                                              '${transporterCubit.strCountry},${transporterCubit.strState},${transporterCubit.strCity}';
                                          transporterCubit
                                              .startLocationPicked();
                                        }
                                        if (access == 4) {
                                          print(
                                              'only for transporter end cubit values');

                                          transporterCubit.endAddress =
                                              pickedData.address;
                                          transporterCubit.endLatitude =
                                              pickedData.latLong.latitude;
                                          transporterCubit.endLongitude =
                                              pickedData.latLong.longitude;
                                          transporterCubit.endCity = pickedData
                                                  .addressData['city'] ??=
                                              pickedData.addressData['town'] ??=
                                                  pickedData.addressData[
                                                          'county'] ??=
                                                      pickedData
                                                          .addressData['state'];
                                          transporterCubit.endCountry =
                                              pickedData.addressData['country'];
                                          transporterCubit.endState = pickedData
                                                  .addressData['state'] ??=
                                              pickedData.addressData['region'];
                                          transporterCubit
                                                  .transporterEndController
                                                  .text =
                                              '${transporterCubit.endCountry},${transporterCubit.endState},${transporterCubit.endCity}';

                                          transporterCubit.endLocationPicked();
                                        }
                                        if(access == 5){
                                          print(
                                              'only for admin start cubit values');

                                         adminCubit.latitude =
                                              pickedData.latLong.latitude;
                                         adminCubit.longitude =
                                              pickedData.latLong.longitude;
                                         adminCubit.city = pickedData
                                              .addressData['city'] ??=
                                          pickedData.addressData['town'] ??=
                                          pickedData.addressData[
                                          'county'] ??=
                                          pickedData
                                              .addressData['state'];
                                         adminCubit.country =
                                          pickedData.addressData['country'];
                                          adminCubit.states = pickedData
                                              .addressData['state'] ??=
                                          pickedData.addressData['region'];
                                         adminCubit.address =
                                              pickedData.address;
                                          adminCubit
                                              .locationController
                                              .text =
                                          '${adminCubit.country},${adminCubit.states},${adminCubit.city}';
                                         adminCubit.locationPicked();


                                        }
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                      },
                                      child: const Text(
                                        'Yes Close The Map',
                                        style: TextStyle(
                                            fontSize: 15, color: MyColors.blue),
                                      )),
                                  TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: const Text(
                                        'Continue Searching',
                                        style: TextStyle(
                                            fontSize: 15, color: MyColors.blue),
                                      )),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ));
          },
          onChanged: (pickedData) {
            print(pickedData.latLong.latitude);
            print(pickedData.latLong.longitude);
            print(pickedData.address);
            print(pickedData.addressData);
          }),
    );
  }
}
