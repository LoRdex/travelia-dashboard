import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_organizer.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/components/dialogs.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/main_cubit/main_cubit.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';

class SupportScreen extends StatelessWidget {
  const SupportScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final TripOrganizerCubit tripOrganizerCubit =
        BlocProvider.of<TripOrganizerCubit>(context);
    final MainCubit mainCubit =
    BlocProvider.of<MainCubit>(context);
    return BlocConsumer<MainCubit, MainState>(
      builder: (context, state) => Form(
        key: mainCubit.supportKey,
        child: Scaffold(
          appBar: CustomAppBar(


            title: 'Contact Support',
          ),
          drawer: mainCubit.getDrawer(),
          body: Stack(
            children: [
              Container(
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('images/background3.jpg'),
                        fit: BoxFit.fill)),
              ),
              Row(
                children: [
                  Expanded(
                    child: Image.asset(
                      'images/support.png',
                      scale: screenWidth / 400,
                    ),
                  ),
                  Expanded(
                      child: Padding(
                    padding: EdgeInsets.all(screenWidth / 20),
                    child: Container(
                      width: screenWidth / 2.5,
                      height: screenHeight / 1.3,
                      decoration: BoxDecoration(border: Border.all(width: 1,color: Colors.grey,),
                          borderRadius: BorderRadius.circular(30),
                          color: Colors.white24.withOpacity(.5)),
                      child: Column(
                        children: [
                          SizedBox(
                            height: screenHeight / 15,
                          ),
                          SizedBox(
                            height: screenHeight / 40,
                          ),
                          Text(
                            'Welcome to Our Support Page ',
                            style: TextStyle(
                                color: MyColors.blue,
                                fontSize: screenWidth * 0.017,
                                fontWeight: FontWeight.w600),
                          ),
                          Text(
                            'Feel Free to Ask About Everything or Report Any Bad Action',
                            style: TextStyle(
                                color: MyColors.blue,
                                fontSize: screenWidth * 0.012,
                                fontWeight: FontWeight.w400),
                          ),
                          SizedBox(
                            height: screenHeight / 20,
                          ),
                          CustomTextFormFiled(
                            controller: mainCubit.titleController,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Field is Required';
                              } else {
                                return null;
                              }
                            },
                            obscureText: false,
                            labelText: 'Title',
                            divider: 4,
                            preIcon: const Icon(
                              Icons.subtitles_outlined,
                              color: MyColors.blue,
                            ),
                          ),
                          SizedBox(
                            height: screenHeight / 18,
                          ),
                          CustomTextFormFiled(
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Field is Required';
                              } else {
                                return null;
                              }
                            },
                            controller: mainCubit.messageController,
                            maxLines: 3,
                            obscureText: false,
                            labelText: 'Content',
                            divider: 4,
                            preIcon: const Icon(
                              Icons.content_copy_outlined,
                              color: MyColors.blue,
                            ),
                          ),
                          SizedBox(
                            height: screenHeight / 20,
                          ),
                          if(state is SupportMessageLoading)
                            const CircularProgressIndicator(color: MyColors.blue,)
                          else
                          CustomButton(
                              text: 'Send',
                              onTap: () {
                                if (mainCubit.supportKey.currentState!
                                    .validate()) {
                                  mainCubit.sendSupportMessage(
                                      title: mainCubit
                                          .titleController.text,
                                      message: mainCubit
                                          .messageController.text);
                                }
                              })
                        ],
                      ),
                    ),
                  )),
                ],
              )
            ],
          ),
        ),
      ),
      listener: (BuildContext context, MainState state) {
        if(state is SupportMessageFailure)
          {
            Dialogs.showFailedDialog(context,"Something Went Wrong, Please Try Again");
          }
        else if (state is SupportMessageSuccess)
          {
            Dialogs.showsSuccessDialog(context, 'Thanks For Sharing Your Thoughts With Us , We Will Reply Soon');
            mainCubit.titleController.clear();
            mainCubit.messageController.clear();
          }


      },
    );
  }
}
