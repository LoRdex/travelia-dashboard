import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_organizer.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/main_cubit/main_cubit.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/models/profile_model.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/widgets/license_image.dart';

import '../../constants/api.dart';
import '../home/organizer_home_screen.dart';

class ProfileView extends StatelessWidget {
  const ProfileView({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);
    final mainCubit = BlocProvider.of<MainCubit>(context);
    ProfileModel profileModel = mainCubit.profileModel!;
    return BlocBuilder<MainCubit, MainState>(
      builder: (context, state) => Scaffold(
        appBar: CustomAppBar(title: 'Profile',),
        drawer: mainCubit.getDrawer(),
        body: Stack(
          fit: StackFit.expand,
          children: [
            Container(
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage('images/background3.jpg')))),
            Row(
              children: [
                Expanded(
                    child: mainCubit.isLicense ? const ImageLicense():Image.asset(
                  'images/profile.png',
                  width: screenWidth / 2,
                )),
                Expanded(
                    child: Column(
                  children: [
                    const SizedBox(
                      height: 35,
                    ),
                    Container(
                      width: screenWidth / 2.5,
                      height: screenHeight / 1.3,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey,width: 1),
                        borderRadius: BorderRadius.circular(30),
                        color: Colors.white.withOpacity(.5),
                      ),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 15,
                          ),
                          Text(
                            '${profileModel.name}',
                            style: TextStyle(
                                color: MyColors.blue,
                                fontSize: screenWidth * 0.017),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            '${profileModel.role}',
                            style: TextStyle(
                                color: MyColors.blue,
                                fontSize: screenWidth * 0.012),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CircleAvatar(
                            radius: screenWidth * 0.038,
                            backgroundColor: MyColors.blue,
                            child: CircleAvatar(
                              backgroundImage: NetworkImage(
                                  '${Api.imgUrl}/${profileModel.facilityPhoto}'),
                              radius: screenWidth * 0.037,
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          Divider(
                            color: MyColors.blue.withOpacity(.3),
                            height: 10,
                            endIndent: 80,
                            indent: 80,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          CustomTextFormFiled(
                            controller:
                                mainCubit.profileNameController,
                            readOnly: true,
                            preIcon: Icon(
                              Icons.person,
                              color: MyColors.blue.withOpacity(.8),
                            ),
                            labelColor: MyColors.blue.withOpacity(.8),
                            labelText: 'Name',
                            obscureText: false,
                            divider: 5,
                            color: MyColors.blue.withOpacity(.8),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          CustomTextFormFiled(
                            controller:
                                mainCubit.profileEmailController,
                            readOnly: true,
                            preIcon: Icon(
                              Icons.email,
                              color: MyColors.blue.withOpacity(.8),
                            ),
                            labelColor: MyColors.blue.withOpacity(.8),
                            labelText: 'Email',
                            obscureText: false,
                            divider: 5,
                            color: MyColors.blue.withOpacity(.8),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          CustomTextFormFiled(
                            controller:
                                mainCubit.profileCountryController,
                            readOnly: true,
                            preIcon: Icon(
                              Icons.flag_sharp,
                              color: MyColors.blue.withOpacity(.8),
                            ),
                            labelColor: MyColors.blue.withOpacity(.8),
                            labelText: 'Country',
                            obscureText: false,
                            divider: 5,
                            color: MyColors.blue.withOpacity(.8),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Divider(
                            color: MyColors.blue.withOpacity(.3),
                            height: 10,
                            endIndent: 80,
                            indent: 80,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Column(
                            children: [
                              const SizedBox(
                                height: 7,
                              ),
                              TextButton(
                                  onPressed: () {
                                   mainCubit.updateIsLicense();
                                  },
                                  child: Text(
                                    'License',
                                    style: TextStyle(
                                        color: MyColors.blue,
                                        fontSize: screenWidth * 0.010),
                                  )),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                )),
              ],
            )
          ],
        ),
      ),
    );
  }
}


