import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/components/dialogs.dart';
import 'package:travelia_dashboard/components/drawers/custom_admin_drawer.dart';
import 'package:travelia_dashboard/cubits/admin_cubit/admin_cubit.dart';

import '../../constants/my_colors.dart';
import '../../cubits/main_cubit/main_cubit.dart';

class SendNotification extends StatelessWidget {
  const SendNotification({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final adminCubit = BlocProvider.of<AdminCubit>(context);
    final mainCubit= BlocProvider.of<MainCubit>(context);

    return BlocListener<AdminCubit, AdminState>(
      child: Form(key: adminCubit.sendMessageKey,
        child: Scaffold(
          appBar: CustomAppBar(
            title: 'Send Notification',
          ),
          drawer: mainCubit.getDrawer(),
          body: Stack(
            fit: StackFit.expand,
            children: [
              Container(
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('images/background5.jpg'),
                        fit: BoxFit.fill)),
              ),
              Row(
                children: [
                  Expanded(
                      child: Image.asset(
                    'images/noti.png',
                    width: screenWidth / 2,
                  )),
                  Expanded(
                      child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight / 18,
                      ),
                      Image.asset(
                        'images/logo4.png',
                        width: screenWidth / 10,
                      ),
                      SizedBox(height: screenHeight/50,),
                      Text('Send a Notification for All Travelia Mobile App Users to Warn Them About any Updates',style: TextStyle(color: MyColors.blue,fontSize: screenWidth*0.011),),
                      SizedBox(
                        height: screenHeight / 18,
                      ),
                      CustomTextFormFiled(
                        controller: adminCubit.titleController,
                        validator: (value){
                          if(value == null || value.isEmpty)
                            {
                              return 'Field is Required';
                            }
                          else {
                            return null ;
                          }
                        },
                        divider: 5,
                        obscureText: false,
                        preIcon: const Icon(
                          Icons.title,
                          color: MyColors.blue,
                        ),
                        labelText: 'Title',
                      ),
                      SizedBox(height: screenHeight/15,),
                      CustomTextFormFiled(maxLines: 2,

                        divider: 4,
                        obscureText: false,
                        preIcon: const Icon(
                          Icons.notification_add,
                          color: MyColors.blue,
                        ),
                        labelText: 'Message',
                        controller: adminCubit.messageController,
                        validator: (value){
                          if(value == null || value.isEmpty)
                          {
                            return 'Field is Required';
                          }
                          else {
                            return null ;
                          }
                        },
                      ),
                      SizedBox(height: screenHeight/15,),
                      CustomButton(text: 'Send', onTap: ()async{
                        if(adminCubit.sendMessageKey.currentState!.validate())
                          {
                            await adminCubit.sendNotification();
                          }

                      },fontSize: 120,dividerH: 17,dividerW:15 ,)


                    ],
                  ))
                ],
              )
            ],
          ),
        ),
      ),
      listener: (BuildContext context, AdminState state) {

        if(state is NotificationSuccess)
          {
            Dialogs.showsSuccessDialog(context, "Notification Sent Successfully ");
            adminCubit.titleController.clear();
            adminCubit.messageController.clear();
          }
        if(state is NotificationFailure)
          {
            Dialogs.showFailedDialog(context, state.errMessage);
            adminCubit.titleController.clear();
            adminCubit.messageController.clear();
          }
      },
    );
  }
}
