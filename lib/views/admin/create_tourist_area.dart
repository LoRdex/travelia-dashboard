import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/components/dialogs.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/admin_cubit/admin_cubit.dart';
import 'package:travelia_dashboard/views/shared/my_map.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

import '../../cubits/main_cubit/main_cubit.dart';

class CreateTouristArea extends StatelessWidget {
  const CreateTouristArea({super.key});

  @override
  Widget build(BuildContext context) {
    final adminCubit = BlocProvider.of<AdminCubit>(context);

    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final MainCubit mainCubit = BlocProvider.of<MainCubit>(context);


    return BlocConsumer<AdminCubit, AdminState>(
      builder: (context, state) => Form(
        key: adminCubit.createAreaKey,
        child: Scaffold(drawer: mainCubit.getDrawer(),
          appBar: CustomAppBar(
            title: 'Create Tourist Are',
          ),
          body: Stack(
            fit: StackFit.expand,
            children: [
              Container(
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('images/background5.jpg'),
                        fit: BoxFit.fill)),
              ),
              Row(
                children: [
                  Expanded(
                      child: Image.asset(
                    'images/global.png',
                    width: screenWidth / 2,
                  )),
                  Expanded(
                      child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight / 50,
                      ),
                          Image.asset('images/logo4.png',width: screenWidth/12,),
                      SizedBox(
                        height: screenHeight / 40,
                      ),
                      CustomTextFormFiled(
                        maxLines: 1,
                        validator: (value){
                          if(value==null||value.isEmpty)
                            {
                              return "Field is Required";
                            }
                          else {return null;}
                        },
                        controller: adminCubit.nameController,
                        obscureText: false,
                        labelText: 'Name',
                        divider: 4,
                        preIcon: const Icon(
                          Icons.drive_file_rename_outline,
                          color: MyColors.blue,
                        ),
                      ),
                      SizedBox(
                        height: screenHeight / 35,
                      ),
                      CustomTextFormFiled(
                        maxLines: 1,
                        obscureText: false,
                        controller: adminCubit.typeController,
                        labelText: 'Type',
                        validator: (value){
                          if(value==null||value.isEmpty)
                          {
                            return "Field is Required";
                          }
                          else {return null;}
                        },
                        divider: 4,
                        preIcon: const Icon(
                          Icons.abc,
                          color: MyColors.blue,
                        ),
                      ),
                      SizedBox(
                        height: screenHeight / 35,
                      ),
                      CustomTextFormFiled(
                        obscureText: false,
                        controller: adminCubit.descriptionController,
                        labelText: 'Description',
                        validator: (value){
                          if(value==null||value.isEmpty)
                          {
                            return "Field is Required";
                          }
                          else {return null;}
                        },
                        maxLines: 3,
                        divider: 4,
                        preIcon: const Icon(
                          Icons.description,
                          color: MyColors.blue,
                        ),
                      ),
                      SizedBox(
                        height: screenHeight / 35,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: screenWidth / 8),
                        child: Row(
                          children: [
                            CustomTextFormFiled(
                              readOnly: true,
                              controller: adminCubit.locationController,
                              maxLines: 1,
                              obscureText: false,
                              validator: (value){
                                if(value==null||value.isEmpty)
                                {
                                  return "Field is Required";
                                }
                                else {return null;}
                              },
                              labelText: 'Location',
                              divider: 4,
                              preIcon: const Icon(
                                Icons.location_on,
                                color: MyColors.blue,
                              ),
                            ),
                            SizedBox(
                              width: screenWidth / 65,
                            ),
                            CustomButton(
                                dividerH: 19,
                                dividerW: 16,
                                fontSize: 120,
                                text: 'Set Location',
                                onTap: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                                    return const MyMap(access: 5);
                                  },));
                                })
                          ],
                        ),
                      ),
                      SizedBox(
                        height: screenHeight / 30,
                      ),
                      if (state is ImageLoadingAdmin)
                        Lottie.asset('lotties/loading.json', width: 50)
                      else if (adminCubit.imageFile != null)
                        Padding(
                          padding: EdgeInsets.only(left: screenWidth / 5.5),
                          child: Row(
                            children: [
                              Lottie.asset(
                                'lotties/success1.json',
                                width: 50,
                                repeat: true,
                              ),
                              SizedBox(
                                width: screenWidth / 35,
                              ),
                              CustomButton(
                                  color: Colors.red,
                                  text: 'Remove',
                                  dividerH: 18,
                                  dividerW: 15,
                                  fontSize: 120,
                                  onTap: () {
                                    adminCubit.imageRemoved();
                                  })
                            ],
                          ),
                        )
                      else
                        GestureDetector(
                          onTap: () {
                            adminCubit.pickImage();
                          },
                          child: Container(
                            height: MediaQuery.of(context).size.height / 16,
                            width: MediaQuery.of(context).size.width / 9,
                            decoration: BoxDecoration(
                              color: MyColors.blue,
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  WidgetAnimator(
                                    atRestEffect: WidgetRestingEffects.swing(
                                        numberOfPlays: 3),
                                    child: Icon(
                                      size: screenWidth * 0.016,
                                      Icons.add_a_photo,
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    width: screenWidth * 0.01,
                                  ),
                                  Text(
                                    ' Image ',
                                    style: TextStyle(
                                        fontSize: screenWidth * 0.01,
                                        color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      SizedBox(
                        height: screenHeight / 30,
                      ),
                      if( state is AreaPushLoadingAdmin)
                        const CircularProgressIndicator(color: MyColors.blue,)
                          else
                      CustomButton(
                          dividerH: 15,
                          dividerW: 13,
                          fontSize: 120,
                          text: 'Create Area',
                          onTap: () {
                            if (adminCubit.imageFile == null) {
                              showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  content: SizedBox(
                                    height:
                                        MediaQuery.of(context).size.height / 10,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Text(
                                          'Uploading Image is Required',
                                          style: TextStyle(
                                              fontSize: screenWidth * 0.01),
                                        ),
                                        TextButton(
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            child: Text(
                                              'Close',
                                              style: TextStyle(
                                                  fontSize: screenWidth * 0.01,
                                                  color: Colors.black),
                                            ))
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            }
                            if(adminCubit.createAreaKey.currentState!.validate()) {
                              adminCubit.pushArea();
                            }
                          })
                    ],
                  )),
                ],
              )
            ],
          ),
        ),
      ),
      listener: (BuildContext context, AdminState state) {
        if (state is ImageWrongExtAdmin) {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              content: SizedBox(
                height: MediaQuery.of(context).size.height / 10,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      state.imgErrWrongExt,
                      style: const TextStyle(fontSize: 20),
                    ),
                    TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text(
                          'Close',
                          style: TextStyle(fontSize: 15, color: Colors.black),
                        ))
                  ],
                ),
              ),
            ),
          );
        }
        if(state is AreaPushSuccessAdmin)
          {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                content: SizedBox(
                  height: MediaQuery.of(context).size.height / 10,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const Text(
                        'Area Created Successfully',
                        style: TextStyle(fontSize: 20,color: MyColors.blue),
                      ),
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text(
                            'Close',
                            style: TextStyle(fontSize: 15, color: Colors.black),
                          ))
                    ],
                  ),
                ),
              ),
            );
          }
        if(state is AreaPushFailureAdmin)
          {
            Dialogs.showsSuccessDialog(context, "Failed , Please Try Again");
          }
      },
    );
  }
}
