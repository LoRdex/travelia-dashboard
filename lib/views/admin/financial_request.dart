import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/components/dialogs.dart';
import 'package:travelia_dashboard/cubits/admin_cubit/admin_cubit.dart';
import 'package:travelia_dashboard/cubits/main_cubit/main_cubit.dart';

import '../../components/custom_button.dart';
import '../../constants/api.dart';
import '../../constants/my_colors.dart';

class FinancialRequests extends StatelessWidget {
  const FinancialRequests({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final AdminCubit adminCubit = BlocProvider.of<AdminCubit>(context);
    final MainCubit mainCubit = BlocProvider.of<MainCubit>(context);
    return BlocConsumer<AdminCubit, AdminState>(
      builder: (context, state) {
        final requests = adminCubit.requestModel?.requierments;

        return Scaffold(drawer: mainCubit.getDrawer(),
          appBar: CustomAppBar(
            title: 'Requests Confirmation',
          ),
          body: Stack(
            fit: StackFit.expand,
            children: [
              Container(
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('images/background3.jpg'),
                        fit: BoxFit.fill)),
              ),
              Center(
                child: Container(
                  width: screenWidth / 1.3,
                  height: screenHeight / 1.2,
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Colors.grey),
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.white.withOpacity(0.8),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: requests == null || requests.isEmpty
                        ? Center(
                      child: state is FinancialRequestLoading ?   const CircularProgressIndicator(color: MyColors.blue,) :   Text(
                        'No Accounts Available For Confirmation',
                        style: TextStyle(
                          color: MyColors.blue,
                          fontSize: screenWidth * 0.015,
                        ),
                      ),
                    )
                        : ListView.builder(
                      itemCount: requests.length,
                      itemBuilder: (context, index) {
                        return Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                            side: BorderSide(
                              color: MyColors.blue.withOpacity(0.8),
                              width: 2,
                            ),
                          ),
                          elevation: 1,
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                  width: 1, color: MyColors.blue),
                              gradient: LinearGradient(
                                colors: [
                                  Colors.white,
                                  Colors.grey.shade200
                                ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                              ),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: screenHeight / 35,
                                            left: screenWidth / 25),
                                        child: InkWell(
                                          child: Container(
                                            width: screenWidth / 15,
                                            height: screenWidth / 15,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(
                                                  screenWidth / 200),
                                              border: Border.all(
                                                  color: MyColors.blue
                                                      .withOpacity(.8),
                                                  width: 2.6),
                                              image: DecorationImage(
                                                image: NetworkImage(
                                                    '${Api.imgUrl}/${requests[index].user?.photo}'),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          onTap: () {
                                            showDialog(
                                              context: context,
                                              builder: (context) =>
                                                  AlertDialog(
                                                    title: Text(
                                                      'License Image',
                                                      style: TextStyle(
                                                          color:
                                                          MyColors.blue,
                                                          fontSize:
                                                          screenWidth *
                                                              0.010),
                                                    ),
                                                    content: Container(
                                                      height: screenHeight,
                                                      width: screenWidth / 2,
                                                      decoration: BoxDecoration(
                                                          image: DecorationImage(
                                                              fit:
                                                              BoxFit.fill,
                                                              image: NetworkImage(
                                                                  '${Api.imgUrl}/${requests[index].user?.photo}'))),
                                                    ),
                                                  ),
                                            );
                                          },
                                        ),
                                      ),
                                      SizedBox(width: screenWidth * 0.05),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${requests[index].user?.firstName} ${requests[index].user?.lastName}',
                                              style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize:
                                                screenWidth * 0.012,
                                                fontWeight:
                                                FontWeight.bold,
                                              ),
                                            ),
                                            const SizedBox(height: 5),
                                            const SizedBox(width: 5),
                                            Text(
                                              'Email: ${requests[index].user?.email}',
                                              style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize:
                                                screenWidth * 0.011,
                                              ),
                                            ),
                                            const SizedBox(height: 5),
                                            const SizedBox(width: 5),
                                            Text(
                                              'Phone : ${requests[index].user?.phone}',
                                              style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize:
                                                screenWidth * 0.011,
                                              ),
                                            ),
                                            const SizedBox(height: 5),
                                            Text(
                                              'Amount: ${requests[index].amount}',
                                              style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize:
                                                screenWidth * 0.011,
                                              ),
                                            ),
                                            const SizedBox(height: 5),
                                            Text(
                                              'Operation Code: ${requests[index].note}',
                                              style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize:
                                                screenWidth * 0.011,
                                              ),
                                            ),
                                            const SizedBox(height: 5),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            right: screenWidth / 100,
                                            top: screenHeight / 15),
                                        child: CustomButton(
                                            color: Colors.green,
                                            dividerW: 10,
                                            dividerH: 20,
                                            fontSize: 100,
                                            text: 'Accept',
                                            onTap: () async {
                                              showDialog(
                                                  context: context,
                                                  builder:
                                                      (context) =>
                                                      BlocBuilder<AdminCubit,AdminState>(
                                                        builder: (context, state) =>  AlertDialog(
                                                          title: Text(
                                                            'Confirmation',
                                                            style: TextStyle(
                                                                color: MyColors
                                                                    .blue,
                                                                fontSize:
                                                                screenWidth *
                                                                    0.011),
                                                          ),
                                                          content:
                                                          SizedBox(
                                                            height:
                                                            screenHeight /
                                                                7,
                                                            width:
                                                            screenWidth /
                                                                3.5,
                                                            child:
                                                            Column(
                                                              children: [
                                                                Text(
                                                                  'are You Sure You Want to Accept This Transaction ?',
                                                                  style: TextStyle(
                                                                      color: Colors.black,
                                                                      fontSize: screenWidth * 0.011),
                                                                ),
                                                                const SizedBox(height: 15,),
                                                                Row(
                                                                  mainAxisAlignment:
                                                                  MainAxisAlignment.spaceEvenly,
                                                                  children: [
                                                                    if(state is FinancialHandlingLoading)
                                                                      const CircularProgressIndicator(color: Colors.green,)
                                                                    else
                                                                    CustomButton(
                                                                        dividerH: 23,
                                                                        dividerW: 25,
                                                                        fontSize: 115,
                                                                        text: 'Yes',
                                                                        onTap: () async {
                                                                          await adminCubit.handleTransactions(requests[index].id!, 'accept');
                                                                          await adminCubit.getFinancialRequests();
                                                                          Navigator.pop(context);
                                                                        }),






                                                                    CustomButton(
                                                                        dividerH: 23,
                                                                        dividerW: 25,
                                                                        fontSize: 115,
                                                                        text: 'No',
                                                                        onTap: () {
                                                                          Navigator.pop(context);
                                                                        })
                                                                  ],
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ));
                                            }),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            right: screenWidth / 100,
                                            top: screenHeight / 15),
                                        child: CustomButton(
                                            color: Colors.red,
                                            dividerW: 10,
                                            dividerH: 20,
                                            fontSize: 100,
                                            text: 'Reject',
                                            onTap: () async {
                                              showDialog(
                                                  context: context,
                                                  builder:
                                                      (context) =>
                                                      BlocBuilder<AdminCubit,AdminState>(
                                                        builder: (context, state) =>  AlertDialog(
                                                          title: Text(
                                                            'Confirmation',
                                                            style: TextStyle(
                                                                color: MyColors
                                                                    .blue,
                                                                fontSize:
                                                                screenWidth *
                                                                    0.011),
                                                          ),
                                                          content:
                                                          SizedBox(
                                                            height:
                                                            screenHeight /
                                                                7,
                                                            width:
                                                            screenWidth /
                                                                3.5,
                                                            child:
                                                            Column(
                                                              children: [
                                                                Text(
                                                                  'are You Sure You Want to Reject This Transaction ?',
                                                                  style: TextStyle(
                                                                      color: Colors.black,
                                                                      fontSize: screenWidth * 0.011),
                                                                ),
                                                                const SizedBox(height: 15,),
                                                                Row(
                                                                  mainAxisAlignment:
                                                                  MainAxisAlignment.spaceEvenly,
                                                                  children: [
                                                                    if(state is FinancialHandlingLoading)
                                                                      const CircularProgressIndicator(color: Colors.red,)
                                                                    else
                                                                    CustomButton(
                                                                        dividerH: 23,
                                                                        dividerW: 25,
                                                                        fontSize: 115,
                                                                        text: 'Yes',
                                                                        onTap: () async {
                                                                          await adminCubit.handleTransactions(requests[index].id!, 'reject');
                                                                          await adminCubit.getFinancialRequests();
                                                                          Navigator.pop(context);
                                                                        }),
                                                                    CustomButton(
                                                                        dividerH: 23,
                                                                        dividerW: 25,
                                                                        fontSize: 115,
                                                                        text: 'No',
                                                                        onTap: () {
                                                                          Navigator.pop(context);
                                                                        })
                                                                  ],
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ));
                                            }),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
      listener: (BuildContext context, AdminState state) {
        if(state is FinancialHandlingSuccess)
        {
          Dialogs.showCustomSnackbar(context, 'Confirmation', 'Action on Account is Done Successfully',5);
        }
        if(state is FinancialHandlingFailure)
        {
          Dialogs.showFailedDialog(context, 'Something Went Wrong :( , Try Again');
        }
      },
    );
  }
}
