import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';

import '../../components/custom_button.dart';
import '../../constants/api.dart';
import '../../constants/my_colors.dart';
import '../../cubits/admin_cubit/admin_cubit.dart';
import '../../cubits/main_cubit/main_cubit.dart';

class SupportMessages extends StatelessWidget {
  const SupportMessages({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final adminCubit = BlocProvider.of<AdminCubit>(context);
    final mainCubit = BlocProvider.of<MainCubit>(context);
    final message = adminCubit.messagesModel?.messages;
    return BlocConsumer<AdminCubit, AdminState>(
      builder: (context, state) => Scaffold(
        appBar: CustomAppBar(
          title: 'Support Messages',
        ),
        drawer: mainCubit.getDrawer(),
        body: Stack(
          fit: StackFit.expand,
          children: [
            Container(
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('images/background3.jpg'),
                      fit: BoxFit.fill)),
            ),
            Center(
              child: Container(
                width: screenWidth / 1.6,
                height: screenHeight / 1.2,
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Colors.grey),
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.white.withOpacity(0.8),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: message == null || message.isEmpty
                      ? Center(
                          child: Text(
                            'No Available Messages',
                            style: TextStyle(
                              color: MyColors.blue,
                              fontSize: screenWidth * 0.015,
                            ),
                          ),
                        )
                      : ListView.builder(
                          itemCount: message.length,
                          itemBuilder: (context, index) {
                            return Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                                side: BorderSide(
                                  color: MyColors.blue.withOpacity(0.8),
                                  width: 2,
                                ),
                              ),
                              elevation: 1,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1, color: MyColors.blue),
                                  gradient: LinearGradient(
                                    colors: [
                                      Colors.white,
                                      Colors.grey.shade200
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: screenHeight / 35,
                                                left: screenWidth / 25),
                                            child: InkWell(
                                              child: Container(
                                                width: screenWidth / 15,
                                                height: screenWidth / 15,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          screenWidth / 200),
                                                  border: Border.all(
                                                      color: MyColors.blue
                                                          .withOpacity(.8),
                                                      width: 2.6),
                                                  image: DecorationImage(
                                                    image: NetworkImage(
                                                        '${Api.imgUrl}/${message[index].photo}'),
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                              onTap: () {
                                                showDialog(
                                                  context: context,
                                                  builder: (context) =>
                                                      AlertDialog(
                                                    title: Text(
                                                      'License Image',
                                                      style: TextStyle(
                                                          color: MyColors.blue,
                                                          fontSize:
                                                              screenWidth *
                                                                  0.010),
                                                    ),
                                                    content: Container(
                                                      height: screenHeight,
                                                      width: screenWidth / 2,
                                                      decoration: BoxDecoration(
                                                          image: DecorationImage(
                                                              fit: BoxFit.fill,
                                                              image: NetworkImage(
                                                                  '${Api.imgUrl}/${message[index].photo}'))),
                                                    ),
                                                  ),
                                                );
                                              },
                                            ),
                                          ),
                                          SizedBox(width: screenWidth * 0.05),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  '${message[index].name} ',
                                                  style: TextStyle(
                                                    color: MyColors.blue,
                                                    fontSize:
                                                        screenWidth * 0.012,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                                const SizedBox(height: 5),
                                                const SizedBox(width: 5),
                                                Text(
                                                  'Email: ${message[index].from}',
                                                  style: TextStyle(
                                                    color: MyColors.blue,
                                                    fontSize:
                                                        screenWidth * 0.011,
                                                  ),
                                                ),
                                                const SizedBox(height: 5),
                                                const SizedBox(width: 5),
                                                Text(
                                                  'Date: ${message[index].date}',
                                                  style: TextStyle(
                                                    color: MyColors.blue,
                                                    fontSize:
                                                        screenWidth * 0.011,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(
                                                right: screenWidth / 100,
                                                top: screenHeight / 15),
                                            child: CustomButton(
                                                color: MyColors.blue,
                                                dividerW: 10,
                                                dividerH: 20,
                                                fontSize: 100,
                                                text: 'Show Message',
                                                onTap: () async {
                                                  showDialog(
                                                      context: context,
                                                      builder:
                                                          (context) =>
                                                              AlertDialog(
                                                                titleTextStyle: TextStyle(
                                                                    fontSize:
                                                                        screenWidth *
                                                                            0.012,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                                title: Text(
                                                                  '${message[index].title}',
                                                                  style: TextStyle(
                                                                      color: MyColors
                                                                          .blue,
                                                                      fontSize:
                                                                          screenWidth *
                                                                              0.011),
                                                                ),
                                                                content:
                                                                    SizedBox(
                                                                  height:
                                                                      screenHeight /
                                                                          2.5,
                                                                  width:
                                                                      screenWidth /
                                                                          3.5,
                                                                  child: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                    children: [
                                                                      Container(
                                                                        decoration: const BoxDecoration(

                                                                            ),
                                                                        child: Text(
                                                                                                                                                  ' Message : ${message[index].msg}',
                                                                                                                                                  style: TextStyle(
                                                                          fontSize: screenWidth * 0.011,
                                                                          color: MyColors.blue),
                                                                                                                                                ),
                                                                      ),
                                                                      Center(
                                                                        child: CustomButton(
                                                                            dividerH: 23,
                                                                            dividerW: 25,
                                                                            fontSize: 115,
                                                                            text: 'Close',
                                                                            onTap: () {
                                                                              Navigator.pop(context);
                                                                            }),
                                                                      )
                                                                    ],
                                                                  ),
                                                                ),
                                                              ));
                                                }),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                ),
              ),
            )
          ],
        ),
      ),
      listener: (BuildContext context, AdminState state) {},
    );
  }
}
