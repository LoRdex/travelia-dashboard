import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/dialogs.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/facility_create_cubits/facility_cubit.dart';
import 'package:travelia_dashboard/cubits/facility_create_cubits/step_cubit.dart';
import 'package:travelia_dashboard/views/facility_create/facility_setup_screen.dart';

class ImagesScreen extends StatelessWidget {
  const ImagesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final stepCubit = StepCubit();
    final facilityCubit = BlocProvider.of<FacilityCubit>(context);
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return BlocConsumer<FacilityCubit, FacilityState>(
        builder: (context, state) => Center(
              child: Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Image.asset(
                    'images/logo4.png',
                    width: screenWidth / 8,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Now Let's Set an Image For The Establishment",
                    style: TextStyle(
                        fontSize: screenWidth * 0.02, color: MyColors.blue),
                  ),
                  Text(
                    "Pick 1 Image to Continue",
                    style: TextStyle(
                        fontSize: screenWidth * 0.01, color: MyColors.blue),
                  ),
                  // Lottie.asset('lotties/camera.json',
                  //     width: MediaQuery.of(context).size.width / 4),
                  Image.asset(
                    'images/camera.png',
                    width: screenHeight / 3,
                  ),
                  const SizedBox(
                    height: 7,
                  ),

                  if (state is ImagesPicked)
                    Row(mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.download_done,
                          color: MyColors.blue,
                          size: screenWidth * 0.03,
                        ),
                        CustomButton(
                            color: Colors.red,
                            text: 'Remove',
                            fontSize: 120,
                            dividerH: 20,
                            dividerW: 18,
                            onTap: () {
                              facilityCubit.removeImage();
                            }),
                      ],
                    )
                  else
                    CustomButton(
                        text: 'Pick Image',
                        onTap: () {
                          facilityCubit.pickImages();
                        }),
                  if (state is ImagesLoading)
                    const Text(
                      'Lower Than 512 KB',
                      style: TextStyle(color: Colors.red, fontSize: 14),
                    )
                ],
              ),
            ),
        listener: (context, state) {
          if (state is ImagesWrongExt) {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                content: SizedBox(
                  height: MediaQuery.of(context).size.height / 10,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        state.errMessage,
                        style: const TextStyle(fontSize: 20),
                      ),
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text(
                            'Close',
                            style: TextStyle(fontSize: 15, color: Colors.black),
                          ))
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is ImagesWrongNumber) {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                content: SizedBox(
                  height: MediaQuery.of(context).size.height / 10,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        state.errMessage,
                        style: const TextStyle(fontSize: 20),
                      ),
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text(
                            'Close',
                            style: TextStyle(fontSize: 15, color: Colors.black),
                          ))
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is ImageTooLarge) {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                content: SizedBox(
                  height: MediaQuery.of(context).size.height / 10,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const Text(
                        "Pick Smaller Image",
                        style: const TextStyle(fontSize: 20),
                      ),
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text(
                            'Close',
                            style: TextStyle(fontSize: 15, color: Colors.black),
                          ))
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is FacilityCreateSuccess) {
            Dialogs.showCustomSnackbar(context, 'Facility Created',
                "Let's Dive into Travelia World :) ", 4);
          }
          if (state is FacilityCreateFailure) {
            Dialogs.showFailedDialog(
                context, "Something Went Wrong, Please Try Again");
          }
        });
  }
}
