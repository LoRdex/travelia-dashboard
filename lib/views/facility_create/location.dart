import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/facility_create_cubits/facility_cubit.dart';
import 'package:travelia_dashboard/views/shared/my_map.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

class Location extends StatelessWidget {
  const Location({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final facilityCubit = BlocProvider.of<FacilityCubit>(context);
    return BlocBuilder<FacilityCubit, FacilityState>(
      builder: (context, state) => Form(
        key: facilityCubit.mapKey,
        child: Row(
          children: [
            Expanded(
                child: Image.asset('images/location.png',width: screenWidth/2,)),

            Expanded(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'images/logo4.png',
                  scale: 3,
                ),

                 Text(" Enter Location Information",style: TextStyle(fontSize: screenWidth*0.02,color: MyColors.blue),),
                const SizedBox(height: 5,),
                 Text("Just Press Set Location Button and Fields Will be Filled Automatically",style: TextStyle(color: MyColors.blue,fontSize: screenWidth*0.01),),
                const SizedBox(
                  height: 15,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2.8,
                  child: IgnorePointer(
                     child: CustomTextFormFiled(color: facilityCubit.addressController.text.isEmpty ? Colors.grey : MyColors.blue,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Address Field is Required";
                        }
                        return null;
                      },
                      controller: facilityCubit.addressController,
                      labelText: 'Address',
                      preIcon:  Icon(
                        Icons.edit_location_alt,
                        color: facilityCubit.addressController.text.isEmpty ? Colors.grey : MyColors.blue,
                      ),
                      obscureText: false,
                      readOnly: true,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                        width: MediaQuery.of(context).size.width / 5.9,
                        child: IgnorePointer(
                          child: CustomTextFormFiled(color: facilityCubit.longitudeController.text.isEmpty ? Colors.grey : MyColors.blue,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Longitude Field is Required";
                              }
                              return null;
                            },
                            controller: facilityCubit.longitudeController,
                            readOnly: true,
                            labelText: 'Longitude',
                            obscureText: false,
                            preIcon:  Icon(
                              Icons.location_pin,
                              color: facilityCubit.longitudeController.text.isEmpty ? Colors.grey : MyColors.blue,
                            ),
                          ),
                        )),
                    const SizedBox(
                      width: 30,
                    ),
                    SizedBox(
                        width: MediaQuery.of(context).size.width / 5.9,
                        child: IgnorePointer(
                          child: CustomTextFormFiled(color: facilityCubit.latitudeController.text.isEmpty ? Colors.grey : MyColors.blue,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Latitude Field is Required";
                              }
                              return null;
                            },
                            controller: facilityCubit.latitudeController,
                            readOnly: true,
                            labelText: 'Latitude',
                            obscureText: false,
                            preIcon:  Icon(
                              Icons.location_pin,
                              color: facilityCubit.latitudeController.text.isEmpty ? Colors.grey : MyColors.blue,
                            ),
                          ),
                        )),

                  ],
                ),
                const SizedBox(
                  height: 7,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                        width: MediaQuery.of(context).size.width / 5.9,
                        child: IgnorePointer(
                          child: CustomTextFormFiled(color: facilityCubit.countryController.text.isEmpty ? Colors.grey : MyColors.blue,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Country Field is Required";
                              }
                              return null;
                            },
                            controller: facilityCubit.countryController,
                            readOnly: true,
                            labelText: 'Country',
                            obscureText: false,
                            preIcon:  Icon(
                              Icons.flag_rounded,
                              color: facilityCubit.countryController.text.isEmpty ? Colors.grey : MyColors.blue,
                            ),
                          ),
                        )),
                    const SizedBox(
                      width: 30,
                    ),
                    SizedBox(
                        width: MediaQuery.of(context).size.width / 5.9,
                        child: IgnorePointer(
                          child: CustomTextFormFiled(color: facilityCubit.stateController.text.isEmpty ? Colors.grey : MyColors.blue,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "State Field is Required";
                              }
                              return null;
                            },
                            controller: facilityCubit.stateController,
                            readOnly: true,
                            labelText: 'State',
                            obscureText: false,
                            preIcon:  Icon(
                              Icons.flag_rounded,
                              color: facilityCubit.stateController.text.isEmpty ? Colors.grey : MyColors.blue,
                            ),
                          ),
                        )),

                  ],
                ),
                const SizedBox(height: 7,),
                SizedBox(
                    width: MediaQuery.of(context).size.width / 5.9,
                    child: IgnorePointer(
                      child: CustomTextFormFiled(color: facilityCubit.cityController.text.isEmpty ? Colors.grey : MyColors.blue,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "City Field is Required";
                          }
                          return null;
                        },
                        controller: facilityCubit.cityController,
                        readOnly: true,
                        labelText: 'City',
                        obscureText: false,
                        preIcon:  Icon(
                          Icons.location_city,
                          color: facilityCubit.cityController.text.isEmpty ? Colors.grey : MyColors.blue,
                        ),
                      ),
                    )),
                const SizedBox(
                  height: 7,
                ),
                if (state is MapSuccess)
                  Lottie.asset('lotties/success1.json', width: 50)
                else
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [const SizedBox(width: 60,),
                      CustomButton(
                          text: 'Set Location',
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => MyMap(access: 1,),
                                ));
                            MyMap(access: 1,);
                          }),
                      // Lottie.asset('lotties/arrow.json',
                      //     width: 70, frameRate: const FrameRate(60)),
                    ],
                  ),

              ],
            )),
          ],
        ),
      ),
    );
  }
}
