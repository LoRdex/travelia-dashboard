import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:travelia_dashboard/cubits/facility_create_cubits/facility_cubit.dart';
import 'package:travelia_dashboard/cubits/facility_create_cubits/step_cubit.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

import '../../components/custom_text_form_field.dart';
import '../../constants/my_colors.dart';

class NameAndDescriptionScreen extends StatelessWidget {
  const NameAndDescriptionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    final facilityCubit = BlocProvider.of<FacilityCubit>(context);
    return BlocBuilder<StepCubit, int>(
      builder: (context, currentStep) => Form(
        key: facilityCubit.formKey,
        child: Row(
          children: [
            Expanded(
              child: WidgetAnimator(
                  incomingEffect:
                      WidgetTransitionEffects.incomingSlideInFromTop(
                          duration: const Duration(seconds: 2)),
                  child: Image.asset(
                    'images/welcome.png',
                    width: screenWidth/2,
                  )),
            ),
            Expanded(
              child: Column(
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  WidgetAnimator(
                    incomingEffect:
                        WidgetTransitionEffects.incomingSlideInFromTop(
                            duration: const Duration(seconds: 2)),
                    child: Image.asset(
                      'images/logo4.png',
                      width:screenWidth/8 ,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  WidgetAnimator(
                    incomingEffect:
                        WidgetTransitionEffects.incomingSlideInFromLeft(
                            duration: const Duration(seconds: 2)),
                    child:  Text(
                      "Setup Your Establishment Information in a Quick Steps",
                      style: TextStyle(color: MyColors.blue, fontSize: screenWidth*0.015),
                    ),
                  ),
                  WidgetAnimator(
                      incomingEffect:
                          WidgetTransitionEffects.incomingSlideInFromRight(
                              duration: const Duration(seconds: 2)),
                      child:  Text(
                        "Let's Start With Establishment Name and a Short Description About it ",
                        style: TextStyle(fontSize: screenWidth*0.01, color: MyColors.blue),
                      )),
                  const SizedBox(
                    height: 20,
                  ),
                  CustomTextFormFiled(
                    validator: (value) {
                      if (value!.isEmpty) {
                        return " Field is Required";
                      } else {
                        return null;
                      }
                    },
                    controller: facilityCubit.nameController,
                    obscureText: false,
                    labelText: 'Establishment Name',
                    preIcon: const Icon(
                      Icons.factory,
                      color: MyColors.blue,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  CustomTextFormFiled(
                    validator: (value) {
                      if (value!.isEmpty) {
                        return " Field is Required";
                      } else {
                        return null;
                      }
                    },
                    controller: facilityCubit.descriptionController,
                    obscureText: false,
                    labelText: 'Description',
                    preIcon: const Icon(
                      Icons.description,
                      color: MyColors.blue,
                    ),
                    maxLines: 2,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
