import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/dialogs.dart';
import 'package:travelia_dashboard/cubits/facility_create_cubits/facility_cubit.dart';
import 'package:travelia_dashboard/views/facility_create/widgets/air_transport.dart';
import 'package:travelia_dashboard/views/facility_create/widgets/ground_transport.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';
import '../../constants/my_colors.dart';

class TransportCompany extends StatelessWidget {
  const TransportCompany({super.key});

  @override
  Widget build(BuildContext context) {
    final facilityCubit = BlocProvider.of<FacilityCubit>(context);
    return BlocConsumer<FacilityCubit, FacilityState>(
      builder: (context, state) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          const SizedBox(
            width: 50,
          ),
          Expanded(
            child:
                facilityCubit.transportType == TransportType.air_Transporation
                    ? Image.asset(
                        'images/plane1.png',
                        scale: 1,
                      )
                    : Image.asset(
                        'images/busdriver.png',
                        scale: 1,
                      ),
          ),
          Expanded(
            child: Column(
              children: [
                const SizedBox(
                  height: 30,
                ),
                Image.asset(
                  'images/logo4.png',
                  scale: 3,
                ),
                const SizedBox(
                  height: 10,
                ),
                const Text(
                  "as a Transport Manger Let's Insert Your Establishment Properties",
                  style: TextStyle(color: MyColors.blue, fontSize: 20),
                ),
                const Text(
                  "We are Almost Done",
                  style: TextStyle(color: MyColors.blue, fontSize: 15),
                ),
                const SizedBox(
                  height: 30,
                ),
                if (facilityCubit.transportType ==
                    TransportType.ground_Transporation) ...[
                  GroundTransport(
                    facilityCubit: facilityCubit,
                  )
                ] else if (facilityCubit.transportType ==
                    TransportType.air_Transporation) ...[
                  AirTransport(facilityCubit: facilityCubit)
                ],
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                  child: Column(
                    children: TransportType.values.map((type) {
                      return SizedBox(
                        width: MediaQuery.of(context).size.width / 2.7,
                        height: 32,
                        child: Theme(
                          data: ThemeData(
                              splashColor: MyColors.scaffoldColor,
                              hoverColor: MyColors.scaffoldColor),
                          child: RadioListTile(
                            controlAffinity: ListTileControlAffinity.trailing,
                            activeColor: MyColors.blue,
                            title: Text(
                              facilityCubit.enumToString(type),
                              style: TextStyle(color: MyColors.blue),
                            ),
                            value: type,
                            secondary: Icon(
                                color: MyColors.blue,
                                type == TransportType.air_Transporation
                                    ? Icons.airplanemode_active_outlined
                                    : type == TransportType.ground_Transporation
                                        ? Icons.directions_bus_filled_rounded
                                        : Icons.abc),
                            groupValue: facilityCubit.transportType,
                            onChanged: (TransportType? value) {
                              facilityCubit.selectType(value!);
                            },
                          ),
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      listener: (BuildContext context, FacilityState state) {
        if(state is FacilityCreateFailure)
          {
            Dialogs.showFailedDialog(context,"Something Went Wrong, Please Try Again");
          }
        if(state is FacilityCreateSuccess){
          Dialogs.showCustomSnackbar(context, 'Facility Created', "Let's Dive into Travelia World :) ",4);
        }
      },
    );
  }
}
