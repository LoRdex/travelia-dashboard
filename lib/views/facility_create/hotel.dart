import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/components/dialogs.dart';
import 'package:travelia_dashboard/cubits/facility_create_cubits/facility_cubit.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import '../../constants/my_colors.dart';

class Hotel extends StatelessWidget {
  const Hotel({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final facilityCubit = BlocProvider.of<FacilityCubit>(context);
    return BlocConsumer<FacilityCubit, FacilityState>(
      builder: (context, state) => Form(
        key: facilityCubit.hotelKey,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const SizedBox(
              width: 50,
            ),
            Expanded(
              child: Image.asset(
                'images/hotel.png',
                width: screenWidth/2,
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  Image.asset(
                    'images/logo4.png',
                    width: screenWidth/9,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                   Text(
                    "as a Hotel Manger Let's Insert Your Establishment Properties",
                    style: TextStyle(color: MyColors.blue, fontSize: screenWidth*0.013),
                  ),
                   Text(
                    "We are Almost Done",
                    style: TextStyle(color: MyColors.blue, fontSize: screenWidth*0.01),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                       Text(
                        'Room With ',
                        style: TextStyle(fontSize: screenWidth*0.013, color: MyColors.blue),
                      ),
                      Image.asset(
                        'images/singlebed.png',
                        scale: 18,
                        color: MyColors.blue,
                      ),
                      const Text(
                        '  : ',
                        style: TextStyle(fontSize: 20, color: MyColors.blue),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      CustomTextFormFiled(
                        controller: facilityCubit.quantityHotel1,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return " Field is Required";
                          }
                          if (!RegExp(r'^\d+$').hasMatch(value)) {
                            return "Enter a Number";
                          }

                          return null;
                        },
                        obscureText: false,
                        divider: 10,
                        labelText: 'Quantity',
                        preIcon: const Icon(
                          Icons.bedroom_child,
                          color: MyColors.blue,
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      CustomTextFormFiled(
                        controller: facilityCubit.priceHotel1,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return " Field is Required";
                          }
                          if (!RegExp(r'^\d+$').hasMatch(value)) {
                            return "Enter a Number";
                          }


                          return null;
                        },
                        obscureText: false,
                        divider: 10,
                        labelText: 'Price',
                        preIcon: const Icon(Icons.monetization_on_rounded,
                            color: MyColors.blue),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                       Text(
                        'Room With ',
                        style: TextStyle(fontSize: screenWidth*0.013, color: MyColors.blue),
                      ),
                      Image.asset(
                        'images/doublebed.png',
                        scale: 18,
                        color: MyColors.blue,
                      ),
                      const Text(
                        '  : ',
                        style: TextStyle(fontSize: 20, color: MyColors.blue),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      CustomTextFormFiled(
                        controller: facilityCubit.quantityHotel2,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return " Field is Required";
                          }
                          if (!RegExp(r'^\d+$').hasMatch(value)) {
                            return "Enter a Number";
                          }

                          return null;
                        },
                        obscureText: false,
                        divider: 10,
                        labelText: 'Quantity',
                        preIcon: const Icon(
                          Icons.bedroom_parent,
                          color: MyColors.blue,
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      CustomTextFormFiled(
                        controller: facilityCubit.priceHotel2,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return " Field is Required";
                          }
                          if (!RegExp(r'^\d+$').hasMatch(value)) {
                            return "Enter a Number";
                          }


                          return null;
                        },
                        obscureText: false,
                        divider: 10,
                        labelText: 'Price',
                        preIcon: const Icon(Icons.monetization_on_rounded,
                            color: MyColors.blue),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                       Text(
                        'Private Suite',
                        style: TextStyle(fontSize: screenWidth*0.012, color: MyColors.blue),
                      ),
                      Image.asset(
                        'images/private.png',
                        scale: 18,
                        color: MyColors.blue,
                      ),
                       Text(
                        '  : ',
                        style: TextStyle(fontSize: screenWidth*0.013, color: MyColors.blue),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      CustomTextFormFiled(
                        controller: facilityCubit.quantityHotel3,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return " Field is Required";
                          }
                          if (!RegExp(r'^\d+$').hasMatch(value)) {
                            return "Enter a Number";
                          }


                          return null;
                        },
                        obscureText: false,
                        divider: 10,
                        labelText: 'Quantity',
                        preIcon: const Icon(
                          Icons.meeting_room_rounded,
                          color: MyColors.blue,
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      CustomTextFormFiled(
                        controller: facilityCubit.priceHotel3,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return " Field is Required";
                          }
                          if (!RegExp(r'^\d+$').hasMatch(value)) {
                            return "Enter a Number";
                          }

                          return null;
                        },
                        obscureText: false,
                        divider: 10,
                        labelText: 'Price',
                        preIcon: const Icon(Icons.monetization_on_rounded,
                            color: MyColors.blue),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                       Text(
                        'Hotel Rank ',
                        style: TextStyle(color: MyColors.blue, fontSize: screenWidth*0.013),
                      ),
                      const Icon(
                        Icons.star_rate,
                        color: MyColors.blue,
                        size: 30,
                      ),
                       Text(
                        '  : ',
                        style: TextStyle(fontSize: screenWidth*0.013, color: MyColors.blue),
                      ),
                      RatingBar.builder(
                        initialRating: 1,
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemPadding:
                            const EdgeInsets.symmetric(horizontal: 14.0),
                        itemBuilder: (context, _) =>  Icon(size: screenWidth*0.012,
                          Icons.star,
                          color: MyColors.blue,
                        ),
                        onRatingUpdate: (rating) {
                          facilityCubit.stars = rating;
                          log('${facilityCubit.stars}');
                        },
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      listener: (BuildContext context, FacilityState state) {
        if(state is FacilityCreateFailure)
        {
          Dialogs.showFailedDialog(context,"Something Went Wrong, Please Try Again");
        } if(state is FacilityCreateSuccess){
          Dialogs.showCustomSnackbar(context, 'Facility Created', "Let's Dive into Travelia World :) ",4);
        }

      },
    );
  }
}
