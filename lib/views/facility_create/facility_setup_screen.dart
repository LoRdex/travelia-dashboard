import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/facility_create_cubits/facility_cubit.dart';
import 'package:travelia_dashboard/cubits/facility_create_cubits/step_cubit.dart';
import 'package:travelia_dashboard/views/facility_create/hotel.dart';
import 'package:travelia_dashboard/views/facility_create/images_screen.dart';

import 'package:travelia_dashboard/views/facility_create/location.dart';
import 'package:travelia_dashboard/views/facility_create/name_and_description.dart';
import 'package:travelia_dashboard/views/facility_create/restaurant.dart';
import 'package:travelia_dashboard/views/facility_create/transport_company.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/create_trip.dart';

import '../../constants/cache_service.dart';
import '../../cubits/main_cubit/main_cubit.dart';
import '../../cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import '../home/hotel_home_screen.dart';
import '../home/organizer_home_screen.dart';
import '../home/restaurant_home_screen.dart';
import '../home/transporter_home_screen.dart';

class FacilitySetupScreen extends StatelessWidget {
  final int roleId;
   int? step;

   FacilitySetupScreen(this.roleId, {super.key});

  @override
  Widget build(BuildContext context) {
    final stepCubit = BlocProvider.of<StepCubit>(context);
    final tripOrganizerCubit =  BlocProvider.of<TripOrganizerCubit>(context);
    final mainCubit = BlocProvider.of<MainCubit>(context);



    Map<int, List<StatelessWidget>> screens = {
      2: [const NameAndDescriptionScreen(), const Location(), const ImagesScreen(),],
      // Trip Organizer
      3: [
        const NameAndDescriptionScreen(),
        const Location(),
        const ImagesScreen(),
        const Hotel(),

      ],
      // Hotel
      4: [
        const NameAndDescriptionScreen(),
        const Location(),
        const ImagesScreen(),
        const Restaurant()
      ],
      // Restaurant
      5: [
        const NameAndDescriptionScreen(),
        const Location(),
        const ImagesScreen(),
        const TransportCompany()
      ],
      // Transport Company
    };
    final roleScreens = screens[roleId] ?? [const Card()];

    return Scaffold(
      backgroundColor: MyColors.scaffoldColor,
      body: BlocConsumer<StepCubit, int>(
        builder: (context, currentStep) {
          final facilityCubit = BlocProvider.of<FacilityCubit>(context);
          bool isLastStep = currentStep == roleScreens!.length - 1;
          return Stack(
            children: [
              Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/background3.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Column(
                children: [
                  Expanded(
                    child: IndexedStack(
                      index: currentStep,
                      children: [...roleScreens],
                    ),
                  ),
                  const SizedBox(height: 80),
                ],
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: currentStep > 0
                      ? CustomButton(
                          text: 'Back',
                          onTap: () {
                            stepCubit.previousStep();
                          })
                      : const SizedBox(),
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: CustomButton(
                    text: isLastStep ? 'Finish' : 'Next',
                    onTap: () async {
                      if (!isLastStep) {
                        if (currentStep == 0) {
                          if (facilityCubit.formKey.currentState!.validate()) {
                            stepCubit.nextStep();
                          }
                        } else if (currentStep == 1) {
                          if (facilityCubit.mapKey.currentState!.validate()) {
                            stepCubit.nextStep();
                          }
                        } else if (currentStep == 2) {
                          if (facilityCubit.imageFile!=null) {
                            stepCubit.nextStep();
                          } else {
                            showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                content: SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 10,
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      const Text(
                                        'Uploading Images is Required',
                                        style: TextStyle(fontSize: 20),
                                      ),
                                      TextButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: const Text(
                                            'Close',
                                            style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.black),
                                          ))
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }
                        }
                      } else {
                        // Handle the 'Finish' button logic
                        if (currentStep == 2) {
                          if (facilityCubit.imageFile!=null) {
                            stepCubit.nextStep();
                          } else {
                            showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                content: SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 10,
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      const Text(
                                        'Uploading Images is Required',
                                        style: TextStyle(fontSize: 20),
                                      ),
                                      TextButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: const Text(
                                            'Close',
                                            style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.black),
                                          ))
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }
                        }
                        if (currentStep == 3 && roleId == 3) {
                          if (facilityCubit.hotelKey.currentState!.validate()) {
                            await facilityCubit.sendFacilityData(context);
                            await facilityCubit.createRooms();
                            await mainCubit.getProfile();
                            await mainCubit.getFinancialLog();
                            stepCubit.resetStep();

                          }
                        }
                        if (currentStep == 3 && roleId == 4) {
                          if (facilityCubit.resKey.currentState!.validate()) {
                            await facilityCubit.sendFacilityData(context);

                            await facilityCubit.createTables();
                            await mainCubit.getProfile();
                            await mainCubit.getFinancialLog();
                            stepCubit.resetStep();

                          }
                        }
                        if (currentStep == 3 && roleId == 5) {
                          if (facilityCubit.transportType ==
                              TransportType.ground_Transporation) {
                            if (facilityCubit.groundKey.currentState!
                                .validate()) {
                              await facilityCubit.sendFacilityData(context);

                               await facilityCubit.createLandTransport();

                              await mainCubit.getProfile();
                              await mainCubit.getFinancialLog();
                              stepCubit.resetStep();

                            }
                          } else if (facilityCubit.transportType ==
                              TransportType.air_Transporation) {
                            if (facilityCubit.airKey.currentState!.validate()) {
                            await  facilityCubit.sendFacilityData(context);

                             await facilityCubit.createAirTransport();
                            await mainCubit.getProfile();
                            await mainCubit.getFinancialLog();
                            stepCubit.resetStep();

                            }
                          }
                        }
                        if (currentStep==2 && roleId == 2) {
                          await facilityCubit.sendFacilityDataOrganizer(context);
                          await mainCubit.getProfile();
                          await mainCubit.getFinancialLog();
                          stepCubit.resetStep();

                          }
                        }

                      }

                  ),
                ),
              ),
            ],
          );
        }, listener: (BuildContext context,  state) {



      },
      ),
    );
  }
}
