import 'package:flutter/material.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/cubits/facility_create_cubits/facility_cubit.dart';

import '../../../constants/my_colors.dart';

class AirTransport extends StatelessWidget {
  const AirTransport({
    super.key,
    required this.facilityCubit,
  });

  final FacilityCubit facilityCubit;

  @override
  Widget build(BuildContext context) {
    return Form(key: facilityCubit.airKey,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Plane  ',
                style: TextStyle(fontSize: 20, color: MyColors.blue),
              ),
              Image.asset(
                'images/plane.png',
                scale: 17,
                color: MyColors.blue,
              ),
              const Text(
                '   : ',
                style: TextStyle(fontSize: 20, color: MyColors.blue),
              ),
              const SizedBox(
                width: 15,
              ),
              CustomTextFormFiled(
                controller: facilityCubit.planeQuantity,
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Field is Required";
                  }

                  if (!RegExp(r'^\d+$').hasMatch(value)) {
                    return "Enter a Number";
                  }
                  return null;
                },
                obscureText: false,
                divider: 11,
                labelText: 'Quantity',
                preIcon: const Icon(
                  Icons.directions_bus,
                  color: MyColors.blue,
                ),
              ),


              const SizedBox(
                width: 20,
              ),
              CustomTextFormFiled(
                controller: facilityCubit.planeCapacity,
                validator: (value) {
                  if (value!.isEmpty) {
                    return " Field is Required";
                  }
                  if (!RegExp(r'^\d+$').hasMatch(value)) {
                    return "Enter a Number";
                  }
                  return null;
                },
                obscureText: false,
                divider: 10,
                labelText: 'Capacity',
                preIcon: const Icon(
                  Icons.reduce_capacity,
                  color: MyColors.blue,
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              CustomTextFormFiled(
                controller: facilityCubit.planePrice,
                validator: (value) {
                  if (value!.isEmpty) {
                    return " Field is Required";
                  }  if (!RegExp(r'^\d+$').hasMatch(value)) {
                    return "Enter a Number";
                  }
                  return null;
                },
                obscureText: false,
                divider: 12,
                labelText: 'Price',
                preIcon: const Icon(Icons.monetization_on_rounded,
                    color: MyColors.blue),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(right: 22),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Vip Plane ',
                  style: TextStyle(fontSize: 18, color: MyColors.blue),
                ),
                Image.asset(
                  'images/vipplane.png',
                  scale: 14,
                  color: MyColors.blue,
                ),
                const Text(
                  '  : ',
                  style: TextStyle(fontSize: 20, color: MyColors.blue),
                ),
                const SizedBox(
                  width: 15,
                ),
                CustomTextFormFiled(
                  controller: facilityCubit.vipPlaneQuantity,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return " Field is Required";
                    }  if (!RegExp(r'^\d+$').hasMatch(value)) {
                      return "Enter a Number";
                    }
                    return null;
                  },
                  obscureText: false,
                  divider: 11,
                  labelText: 'Quantity',
                  preIcon: const Icon(
                    Icons.directions_bus,
                    color: MyColors.blue,
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                CustomTextFormFiled(
                  controller: facilityCubit.vipPlaneCapacity,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return " Field is Required";
                    }  if (!RegExp(r'^\d+$').hasMatch(value)) {
                      return "Enter a Number";
                    }
                    return null;
                  },
                  obscureText: false,
                  divider: 10,
                  labelText: 'Capacity',
                  preIcon: const Icon(
                    Icons.reduce_capacity,
                    color: MyColors.blue,
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                CustomTextFormFiled(
                  controller: facilityCubit.vipPlanePrice,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return " Field is Required";
                    } if (!RegExp(r'^\d+$').hasMatch(value)) {
                      return "Enter a Number";
                    }
                    return null;
                  },
                  obscureText: false,
                  divider: 12,
                  labelText: 'Price',
                  preIcon: const Icon(Icons.monetization_on_rounded,
                      color: MyColors.blue),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}