import 'package:flutter/material.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';

import '../../../cubits/facility_create_cubits/facility_cubit.dart';

class GroundTransport extends StatelessWidget {
  final FacilityCubit facilityCubit;

  const GroundTransport({super.key,required this.facilityCubit});

  @override
  Widget build(BuildContext context) {
    return Form(key: facilityCubit.groundKey,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Big Bus  ',
                style: TextStyle(fontSize: 20, color: MyColors.blue),
              ),
              Image.asset(
                'images/bus.png',
                scale: 17,
                color: MyColors.blue,
              ),
              const Text(
                '   : ',
                style: TextStyle(fontSize: 20, color: MyColors.blue),
              ),
              const SizedBox(
                width: 15,
              ),
              CustomTextFormFiled(
                controller: facilityCubit.busQuantity,
                validator: (value) {
                  if (value!.isEmpty) {
                    return " Field is Required";
                  }  if (!RegExp(r'^\d+$').hasMatch(value)) {
                    return "Enter a Number";
                  }
                  return null;
                },
                obscureText: false,
                divider: 11,
                labelText: 'Quantity',
                preIcon: const Icon(
                  Icons.directions_bus,
                  color: MyColors.blue,
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              CustomTextFormFiled(
                controller: facilityCubit.busCapacity,
                validator: (value) {
                  if (value!.isEmpty) {
                    return " Field is Required";
                  }if (!RegExp(r'^\d+$').hasMatch(value)) {
                    return "Enter a Number";
                  }
                  return null;
                },
                obscureText: false,
                divider: 10,
                labelText: 'Capacity',
                preIcon: const Icon(
                  Icons.reduce_capacity,
                  color: MyColors.blue,
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              CustomTextFormFiled(
                controller: facilityCubit.busPrice,
                validator: (value) {
                  if (value!.isEmpty) {
                    return " Field is Required";
                  }if (!RegExp(r'^\d+$').hasMatch(value)) {
                    return "Enter a Number";
                  }
                  return null;
                },
                obscureText: false,
                divider: 12,
                labelText: 'Price',
                preIcon: const Icon(Icons.monetization_on_rounded,
                    color: MyColors.blue),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Small Bus ',
                style: TextStyle(fontSize: 18, color: MyColors.blue),
              ),
              Image.asset(
                'images/smallbus.png',
                scale: 19,
                color: MyColors.blue,
              ),
              const Text(
                '  : ',
                style: TextStyle(fontSize: 20, color: MyColors.blue),
              ),
              const SizedBox(
                width: 15,
              ),
              CustomTextFormFiled(
                controller: facilityCubit.smallBusQuantity,
                validator: (value) {
                  if (value!.isEmpty) {
                    return " Field is Required";
                  }if (!RegExp(r'^\d+$').hasMatch(value)) {
                    return "Enter a Number";
                  }
                  return null;
                },
                obscureText: false,
                divider: 11,
                labelText: 'Quantity',
                preIcon: const Icon(
                  Icons.directions_bus,
                  color: MyColors.blue,
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              CustomTextFormFiled(
                controller: facilityCubit.smallBusCapacity,
                validator: (value) {
                  if (value!.isEmpty) {
                    return " Field is Required";
                  }if (!RegExp(r'^\d+$').hasMatch(value)) {
                    return "Enter a Number";
                  }
                  return null;
                },
                obscureText: false,
                divider: 10,
                labelText: 'Capacity',
                preIcon: const Icon(
                  Icons.reduce_capacity,
                  color: MyColors.blue,
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              CustomTextFormFiled(
                controller: facilityCubit.smallBusPrice,
                validator: (value) {
                  if (value!.isEmpty) {
                    return " Field is Required";
                  }if (!RegExp(r'^\d+$').hasMatch(value)) {
                    return "Enter a Number";
                  }
                  return null;
                },
                obscureText: false,
                divider: 12,
                labelText: 'Price',
                preIcon: const Icon(Icons.monetization_on_rounded,
                    color: MyColors.blue),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 47),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Van ',
                  style: TextStyle(fontSize: 19, color: MyColors.blue),
                ),
                Image.asset(
                  'images/van.png',
                  scale: 18,
                  color: MyColors.blue,
                ),
                const Text(
                  '  : ',
                  style: TextStyle(fontSize: 19, color: MyColors.blue),
                ),
                const SizedBox(
                  width: 15,
                ),
                CustomTextFormFiled(
                  controller: facilityCubit.vanQuantity,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return " Field is Required";
                    }if (!RegExp(r'^\d+$').hasMatch(value)) {
                      return "Enter a Number";
                    }
                    return null;
                  },
                  obscureText: false,
                  divider: 11,
                  labelText: 'Quantity',
                  preIcon: const Icon(
                    Icons.directions_bus,
                    color: MyColors.blue,
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                CustomTextFormFiled(
                  controller: facilityCubit.vanCapacity,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return " Field is Required";
                    }if (!RegExp(r'^\d+$').hasMatch(value)) {
                      return "Enter a Number";
                    }
                    return null;
                  },
                  obscureText: false,
                  divider: 10,
                  labelText: 'Capacity',
                  preIcon: const Icon(
                    Icons.reduce_capacity,
                    color: MyColors.blue,
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                CustomTextFormFiled(
                  controller: facilityCubit.vanPrice,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return " Field is Required";
                    }if (!RegExp(r'^\d+$').hasMatch(value)) {
                      return "Enter a Number";
                    }
                    return null;
                  },
                  obscureText: false,
                  divider: 12,
                  labelText: 'Price',
                  preIcon: const Icon(Icons.monetization_on_rounded,
                      color: MyColors.blue),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
