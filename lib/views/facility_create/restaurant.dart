import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/facility_create_cubits/facility_cubit.dart';

import '../../components/dialogs.dart';

class Restaurant extends StatelessWidget {
  const Restaurant({super.key});

  @override
  Widget build(BuildContext context) {
    final facilityCubit = BlocProvider.of<FacilityCubit>(context);
    return BlocConsumer<FacilityCubit, FacilityState>(
      builder: (context, state) => Form(
        key: facilityCubit.resKey,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const SizedBox(
              width: 50,
            ),
            Image.asset(
              'images/restaurant.png',
              scale: 1,
            ),
            Expanded(
              child: Column(
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  Image.asset(
                    'images/logo4.png',
                    scale: 3,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "as a Restaurant Manger Let's Insert Your Establishment Properties",
                    style: TextStyle(color: MyColors.blue, fontSize: 20),
                  ),
                  const Text(
                    "We are Almost Done",
                    style: TextStyle(color: MyColors.blue, fontSize: 15),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'Table For ',
                        style: TextStyle(fontSize: 20, color: MyColors.blue),
                      ),
                      Image.asset(
                        'images/2.png',
                        scale: 18,
                        color: MyColors.blue,
                      ),
                      const Text(
                        ' Persons  : ',
                        style: TextStyle(fontSize: 20, color: MyColors.blue),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      CustomTextFormFiled(
                        controller: facilityCubit.quantity1,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return " Field is Required";
                          }
                          if (!RegExp(r'^\d+$').hasMatch(value)) {
                            return "Enter a Number";
                          }
                          return null;
                        },
                        obscureText: false,
                        divider: 10,
                        labelText: 'Quantity',
                        preIcon: const Icon(
                          Icons.table_bar,
                          color: MyColors.blue,
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      CustomTextFormFiled(
                        controller: facilityCubit.price1,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return " Field is Required";
                          }
                          if (!RegExp(r'^\d+$').hasMatch(value)) {
                            return "Enter a Number";
                          }
                          return null;
                        },
                        obscureText: false,
                        divider: 10,
                        labelText: 'Price',
                        preIcon: const Icon(Icons.monetization_on_rounded,
                            color: MyColors.blue),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'Table For ',
                        style: TextStyle(fontSize: 20, color: MyColors.blue),
                      ),
                      Image.asset(
                        'images/4.png',
                        scale: 18,
                        color: MyColors.blue,
                      ),
                      const Text(
                        ' Persons  : ',
                        style: TextStyle(fontSize: 20, color: MyColors.blue),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      CustomTextFormFiled(
                        controller: facilityCubit.quantity2,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return " Field is Required";
                          }
                          if (!RegExp(r'^\d+$').hasMatch(value)) {
                            return "Enter a Number";
                          }
                          return null;
                        },
                        obscureText: false,
                        divider: 10,
                        labelText: 'Quantity',
                        preIcon: const Icon(
                          Icons.table_bar,
                          color: MyColors.blue,
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      CustomTextFormFiled(
                        controller: facilityCubit.price2,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return " Field is Required";
                          }
                          if (!RegExp(r'^\d+$').hasMatch(value)) {
                            return "Enter a Number";
                          }
                          return null;
                        },
                        obscureText: false,
                        divider: 10,
                        labelText: 'Price',
                        preIcon: const Icon(Icons.monetization_on_rounded,
                            color: MyColors.blue),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'More Than ',
                        style: TextStyle(fontSize: 18, color: MyColors.blue),
                      ),
                      Image.asset(
                        'images/4.png',
                        scale: 18,
                        color: MyColors.blue,
                      ),
                      const Text(
                        ' Persons  : ',
                        style: TextStyle(fontSize: 19, color: MyColors.blue),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      CustomTextFormFiled(
                        controller: facilityCubit.quantity3,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return " Field is Required";
                          }
                          if (!RegExp(r'^\d+$').hasMatch(value)) {
                            return "Enter a Number";
                          }
                          return null;
                        },
                        obscureText: false,
                        divider: 10,
                        labelText: 'Quantity',
                        preIcon: const Icon(
                          Icons.table_bar,
                          color: MyColors.blue,
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      CustomTextFormFiled(
                        controller: facilityCubit.price3,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return " Field is Required";
                          }
                          if (!RegExp(r'^\d+$').hasMatch(value)) {
                            return "Enter a Number";
                          }
                          return null;
                        },
                        obscureText: false,
                        divider: 10,
                        labelText: 'Price',
                        preIcon: const Icon(Icons.monetization_on_rounded,
                            color: MyColors.blue),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 1.5,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 2.49,
                          child: Row(
                            children: [
                              Expanded(
                                  child: CheckboxListTile(
                                      activeColor: MyColors.blue,
                                      checkColor: Colors.white,
                                      title: Row(
                                        children: [
                                          Image.asset(
                                            'images/seafood.png',
                                            scale: 19,
                                            color: MyColors.blue,
                                          ),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          const Text(
                                            'Sea Food',
                                            style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize: 12),
                                          )
                                        ],
                                      ),
                                      controlAffinity:
                                          ListTileControlAffinity.leading,
                                      value: facilityCubit.isSeaFoodChecked,
                                      onChanged: (value) {
                                        facilityCubit.updateCheckBox(1, value!);
                                        facilityCubit.restaurantRoleSelected();
                                      })),
                              Expanded(
                                  child: CheckboxListTile(
                                      activeColor: MyColors.blue,
                                      checkColor: Colors.white,
                                      title: Row(
                                        children: [
                                          Image.asset(
                                            'images/cafe.png',
                                            scale: 19,
                                            color: MyColors.blue,
                                          ),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          const Text(
                                            'Cafe',
                                            style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize: 12),
                                          )
                                        ],
                                      ),
                                      controlAffinity:
                                          ListTileControlAffinity.leading,
                                      value: facilityCubit.isCafeChecked,
                                      onChanged: (value) {
                                        facilityCubit.updateCheckBox(2, value!);
                                      })),
                              Expanded(
                                  child: CheckboxListTile(
                                      activeColor: MyColors.blue,
                                      checkColor: Colors.white,
                                      title: Row(
                                        children: [
                                          Image.asset(
                                            'images/buffet.png',
                                            scale: 19,
                                            color: MyColors.blue,
                                          ),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          const Text(
                                            'Buffet',
                                            style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize: 12),
                                          )
                                        ],
                                      ),
                                      controlAffinity:
                                          ListTileControlAffinity.leading,
                                      value: facilityCubit.isBuffetChecked,
                                      onChanged: (value) {
                                        facilityCubit.updateCheckBox(3, value!);
                                      })),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 2.49,
                          child: Row(
                            children: [
                              Expanded(
                                  child: CheckboxListTile(
                                      activeColor: MyColors.blue,
                                      checkColor: Colors.white,
                                      title: Row(
                                        children: [
                                          Image.asset(
                                            'images/fastfood.png',
                                            scale: 19,
                                            color: MyColors.blue,
                                          ),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          const Text(
                                            'Fast Food',
                                            style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize: 12),
                                          )
                                        ],
                                      ),
                                      controlAffinity:
                                          ListTileControlAffinity.leading,
                                      value: facilityCubit.isFastFoodChecked,
                                      onChanged: (value) {
                                        facilityCubit.updateCheckBox(4, value!);
                                      })),
                              Expanded(
                                  child: CheckboxListTile(
                                      activeColor: MyColors.blue,
                                      checkColor: Colors.white,
                                      title: Row(
                                        children: [
                                          Image.asset(
                                            'images/east.png',
                                            scale: 19,
                                            color: MyColors.blue,
                                          ),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          const Text(
                                            'Eastern',
                                            style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize: 12),
                                          )
                                        ],
                                      ),
                                      controlAffinity:
                                          ListTileControlAffinity.leading,
                                      value: facilityCubit.isEasternChecked,
                                      onChanged: (value) {
                                        facilityCubit.updateCheckBox(5, value!);
                                      })),
                              Expanded(
                                  child: CheckboxListTile(
                                      activeColor: MyColors.blue,
                                      checkColor: Colors.white,
                                      title: Row(
                                        children: [
                                          Image.asset(
                                            'images/west.png',
                                            scale: 19,
                                            color: MyColors.blue,
                                          ),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          const Text(
                                            'Western',
                                            style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize: 12),
                                          )
                                        ],
                                      ),
                                      controlAffinity:
                                          ListTileControlAffinity.leading,
                                      value: facilityCubit.isWesternChecked,
                                      onChanged: (value) {
                                        facilityCubit.updateCheckBox(6, value!);
                                      })),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      listener: (BuildContext context, FacilityState state) {
        if(state is FacilityCreateFailure)
        {
          Dialogs.showFailedDialog(context,"Something Went Wrong, Please Try Again");
        } if(state is FacilityCreateSuccess){
          Dialogs.showCustomSnackbar(context, 'Facility Created', "Let's Dive into Travelia World :) ",4);
        }

      },
    );
  }
}
