import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/components/dialogs.dart';
import 'package:travelia_dashboard/views/shared/my_map.dart';
import 'package:travelia_dashboard/views/home/organizer_home_screen.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/choose_hotel.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/choose_restaurants.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/choose_transport.dart';

import '../../constants/my_colors.dart';
import '../../cubits/trip_organizer_cubit/trip_organizer_cubit.dart';

class ChooseFacilitesView extends StatelessWidget {
  const ChooseFacilitesView({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final TripOrganizerCubit tripOrganizerCubit =
        BlocProvider.of<TripOrganizerCubit>(context);
    return BlocConsumer<TripOrganizerCubit, TripOrganizerState>(
      builder: (context, state) {
        return Form(
          key: tripOrganizerCubit.secPageKey,
          child: Scaffold(
            body: Stack(
              children: [
                Container(
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('images/background3.jpg'),
                          fit: BoxFit.cover)),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Image.asset(
                        'images/create2.png',
                        width: screenWidth / 3,
                      ),
                    ),
                    Expanded(
                        child: Column(
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        Image.asset(
                          'images/logo4.png',
                          width: screenWidth / 10,
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Text(
                          'Ensure the Comfortable Experience for the Trip, Please Select The Trip Facilities',
                          style: TextStyle(
                              fontSize: screenWidth * 0.012,
                              color: MyColors.blue),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            IgnorePointer(
                              child: CustomTextFormFiled(
                                controller: tripOrganizerCubit.hotelController,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Please Select The Hotel';
                                  }
                                  return null;
                                },
                                preIcon: Icon(
                                  Icons.hotel,
                                  color: tripOrganizerCubit
                                          .hotelController.text.isEmpty
                                      ? Colors.grey
                                      : MyColors.blue,
                                ),
                                obscureText: false,
                                labelText: 'Hotel',
                                divider: 3.5,
                                labelColor: tripOrganizerCubit
                                        .restaurantController.text.isEmpty
                                    ? Colors.grey
                                    : MyColors.blue,
                                color: tripOrganizerCubit
                                        .hotelController.text.isEmpty
                                    ? Colors.grey
                                    : MyColors.blue,
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            CustomButton(
                              fontSize: 110,
                              text: 'Hotels',
                              onTap: () async {
                                await tripOrganizerCubit.getHotels();
                              },
                              dividerH: 16,
                              dividerW: 14,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            IgnorePointer(
                              child: CustomTextFormFiled(
                                controller:
                                    tripOrganizerCubit.restaurantController,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Please Select The Restaurant';
                                  }
                                  return null;
                                },
                                preIcon: Icon(
                                  Icons.fastfood_rounded,
                                  color: tripOrganizerCubit
                                          .restaurantController.text.isEmpty
                                      ? Colors.grey
                                      : MyColors.blue,
                                ),
                                obscureText: false,
                                labelText: 'Restaurant',
                                divider: 3.5,
                                labelColor: tripOrganizerCubit
                                        .restaurantController.text.isEmpty
                                    ? Colors.grey
                                    : MyColors.blue,
                                color: tripOrganizerCubit
                                        .restaurantController.text.isEmpty
                                    ? Colors.grey
                                    : MyColors.blue,
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            CustomButton(
                              fontSize: 110,
                              text: 'Restaurants',
                              onTap: () async {
                                await tripOrganizerCubit.getRestaurants();
                              },
                              dividerH: 16,
                              dividerW: 14,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            IgnorePointer(
                              child: CustomTextFormFiled(
                                controller:
                                    tripOrganizerCubit.transportController,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Please Select The Transport Company';
                                  }
                                  return null;
                                },
                                preIcon: Icon(Icons.directions_bus_filled,
                                    color: tripOrganizerCubit
                                            .transportController.text.isEmpty
                                        ? Colors.grey
                                        : MyColors.blue),
                                obscureText: false,
                                labelText: 'Transport Company',
                                divider: 3.5,
                                labelColor: tripOrganizerCubit
                                        .transportController.text.isEmpty
                                    ? Colors.grey
                                    : MyColors.blue,
                                color: tripOrganizerCubit
                                        .transportController.text.isEmpty
                                    ? Colors.grey
                                    : MyColors.blue,
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            CustomButton(
                              fontSize: 110,
                              text: 'Transporters',
                              onTap: () async {
                                await tripOrganizerCubit.getTransports();
                              },
                              dividerH: 16,
                              dividerW: 14,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: screenWidth / 14),
                          child: CustomButton(
                              text: 'Create Trip',
                              onTap: () async {
                                if (tripOrganizerCubit.secPageKey.currentState!
                                    .validate()) {
                                  await tripOrganizerCubit.createTrip(context);
                                  await tripOrganizerCubit.getOrganizedTrips();
                                }
                              }),
                        )
                      ],
                    )),
                  ],
                ),
              ],
            ),
          ),
        );
      },
      listener: (BuildContext context, TripOrganizerState state) {
        if (state is RestaurantLoadSuccess) {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const RestaurantsView(),
              ));
        }
        if (state is TransportLoadSuccess) {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const TransportView(),
              ));
        }
        if (state is HotelsLoadSuccess) {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const HotelsView(),
              ));
        }
        if (state is HotelsLoadFailure ||
            state is RestaurantLoadFailure ||
            state is TransportLoadFailure ||
            state is CreateTripFailure) {
          Dialogs.showFailedDialog(context,"Something Went Wrong, Please Try Again");
        }

      },
    );
  }
}
