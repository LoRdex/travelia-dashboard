import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_organizer.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/models/discover_areas_model.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/widgets/discover_area_details.dart';

import '../../constants/api.dart';
import '../../cubits/main_cubit/main_cubit.dart';

class DiscoverAreaScreen extends StatelessWidget {
  const DiscoverAreaScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    final TripOrganizerCubit tripOrganizerCubit =
        BlocProvider.of<TripOrganizerCubit>(context);
    final MainCubit mainCubit =
    BlocProvider.of<MainCubit>(context);
    final discoverAreas = tripOrganizerCubit
        .discoverAreasModel!.areas; // Access the touristAreas list

    return Scaffold(
      drawer: mainCubit.getDrawer(),
      appBar: CustomAppBar(


        title: 'Discover Tourist Areas',

      ),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/background2.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          GridView.builder(
            padding: EdgeInsets.only(
                right: screenWidth / 16,
                left: screenWidth / 16,
                top: screenWidth / 22),
            itemCount: discoverAreas?.length ?? 0,
            // Use the length of the areas list
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisSpacing: screenWidth / 25,
              mainAxisSpacing: screenHeight / 13,
              childAspectRatio: 3 / 2,
              crossAxisCount: 3,
            ),
            itemBuilder: (context, index) {
              return GridItem(
                area: discoverAreas![index],
                index: index,
              );
            },
          ),
        ],
      ),
    );
  }
}

class GridItem extends StatelessWidget {
  final Areas area;
  final int index; // Accept a TouristAreas object

  const GridItem({super.key, required this.area, required this.index});

  @override
  Widget build(BuildContext context) {
    final TripOrganizerCubit tripOrganizerCubit =
        BlocProvider.of<TripOrganizerCubit>(context);

    double screenWidth = MediaQuery.of(context).size.width;
    return BlocBuilder<TripOrganizerCubit, TripOrganizerState>(
      builder: (context, state) {
        final isHovering = state is GridHover && state.index == area.id;
        final scaleFactor = isHovering ? 1.05 : 1.0;
        final glowColor =
            isHovering ? MyColors.blue.withOpacity(0.8) : Colors.transparent;
        return MouseRegion(
          onEnter: (_) =>
              context.read<TripOrganizerCubit>().updateGrid(area.id!, true),
          onExit: (_) =>
              context.read<TripOrganizerCubit>().updateGrid(area.id!, false),
          child: AnimatedContainer(
            decoration:
                BoxDecoration(border: Border.all(style: BorderStyle.none)),
            duration: const Duration(milliseconds: 800),
            curve: Curves.easeInOut,
            transform: Matrix4.identity()..scale(scaleFactor),
            child: Transform.scale(
              scale: scaleFactor,
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DiscoverAreaDetails(index: index),
                      ));
                },
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                    gradient: const LinearGradient(
                      colors: [Colors.blueAccent, Colors.cyanAccent],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: glowColor,
                        spreadRadius: 4,
                        blurRadius: 15,
                        offset: const Offset(0, 0),
                      ),
                      BoxShadow(
                        color: Colors.black.withOpacity(0.3),
                        spreadRadius: 2,
                        blurRadius: 8,
                        offset: const Offset(0, 4),
                      ),
                    ],
                    border: Border.all(color: Colors.white, width: 2),
                    image: DecorationImage(
                      image: NetworkImage('${Api.imgUrl}/${area.img}'),
                      fit: BoxFit.cover,
                      colorFilter: isHovering
                          ? const ColorFilter.mode(
                              Colors.transparent,
                              BlendMode.lighten,
                            )
                          : ColorFilter.mode(
                              Colors.black.withOpacity(0.3),
                              BlendMode.darken,
                            ),
                    ),
                  ),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Positioned(
                        bottom: 20,
                        child: Text(
                          area.name!,
                          style: TextStyle(
                            shadows: const [
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(-1.5, -1.5)),
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(1.5, -1.5)),
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(-1.5, 1.5)),
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(1.5, 1.5)),
                            ],
                            fontSize: screenWidth * 0.012,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      if (!isHovering)
                        Positioned(
                          bottom: 10,
                          left: 20,
                          child: Text(
                            area.location!.country!,
                            style: TextStyle(
                              shadows: const [
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(-1.5, -1.5)),
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(1.5, -1.5)),
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(-1.5, 1.5)),
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(1.5, 1.5)),
                              ],
                              fontSize: screenWidth * 0.007,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      if (!isHovering)
                        Positioned(
                          bottom: 10,
                          right: 20,
                          child: Text(
                            area.type!,
                            style: TextStyle(
                              shadows: const [
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(-1.5, -1.5)),
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(1.5, -1.5)),
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(-1.5, 1.5)),
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(1.5, 1.5)),
                              ],
                              fontSize: screenWidth * 0.007,
                              color: Colors.white,
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
