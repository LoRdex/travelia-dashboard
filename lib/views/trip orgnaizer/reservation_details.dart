import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_organizer.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';

import '../../constants/api.dart';

class ReservationDetailsScreen extends StatelessWidget {
  const ReservationDetailsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final TripOrganizerCubit tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);
    final reservations = tripOrganizerCubit.reservationModel?.reservations;

    return Scaffold(
      appBar: CustomAppBar(


        title: 'Reservations',
      ),
      drawer: const CustomDrawerOrganizer(),
      body: Stack(
        fit: StackFit.expand,
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/background3.jpg'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Center(
            child: Container(
              width: screenWidth / 1.6,
              height: screenHeight / 1.3,
              decoration: BoxDecoration(border: Border.all(width: 1,color: Colors.grey),
                borderRadius: BorderRadius.circular(30),
                color: Colors.white.withOpacity(0.3),
              ),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: reservations == null || reservations.isEmpty
                    ? Center(
                  child: Text(
                    'No reservations available.',
                    style: TextStyle(
                      color: MyColors.blue,
                      fontSize: screenWidth * 0.015,
                    ),
                  ),
                )
                    : ListView.builder(
                  itemCount: reservations.length,
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              left: screenWidth * 0.10, top: 8, bottom: 8),
                          child: Row(
                            children: [
                              CircleAvatar(
                                radius: screenWidth * 0.017,
                                backgroundColor: MyColors.blue,
                                child: CircleAvatar(
                                  radius: screenWidth * 0.016,
                                  backgroundImage: NetworkImage(
                                      '${Api.imgUrl}/${reservations[index].photo}'),
                                ),
                              ),
                              SizedBox(width: screenWidth * 0.020),
                              SizedBox(width: screenWidth/18,
                                child: Text(
                                  '${reservations[index].name}',
                                  style: TextStyle(
                                    color: MyColors.blue,
                                    fontSize: screenWidth * 0.011,
                                  ),
                                ),
                              ),
                              SizedBox(width: screenWidth * 0.050),
                              SizedBox(width: screenWidth/25,
                                child: Text(
                                  '${reservations[index].address}',
                                  style: TextStyle(
                                    color: MyColors.blue,
                                    fontSize: screenWidth * 0.011,
                                  ),
                                ),
                              ),
                              SizedBox(width: screenWidth * 0.050),
                              TextButton(
                                  onPressed: () {
                                    showDialog(
                                        context: context,
                                        builder: (context) => AlertDialog(
                                          content: SizedBox(
                                            height: MediaQuery.of(context)
                                                .size
                                                .height /
                                                1.5,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment:
                                                MainAxisAlignment
                                                    .spaceEvenly,
                                                children: [
                                                  Text(
                                                    'User Details : ',
                                                    style: TextStyle(
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        color: MyColors
                                                            .blue,
                                                        fontSize:
                                                        screenWidth *
                                                            0.012),
                                                  ),
                                                  const SizedBox(
                                                    height: 3,
                                                  ),
                                                  CircleAvatar(
                                                    backgroundColor:
                                                    MyColors.blue,
                                                    radius: screenWidth *
                                                        0.025,
                                                    child: CircleAvatar(
                                                      radius: screenWidth *
                                                          0.024,
                                                      backgroundImage:
                                                      NetworkImage(
                                                          '${Api.imgUrl}/${reservations[index].photo}'),
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    height: 3,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                    children: [
                                                      Text(
                                                        'Name : ',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      ),
                                                      const SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        '${reservations[index].name}',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      )
                                                    ],
                                                  ),
                                                  const SizedBox(
                                                    height: 8,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                    children: [
                                                      Text(
                                                        'Email : ',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      ),
                                                      const SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        '${reservations[index].email}',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      )
                                                    ],
                                                  ),
                                                  const SizedBox(
                                                    height: 8,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                    children: [
                                                      Text(
                                                        'Phone : ',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      ),
                                                      const SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        '${reservations[index].phone}',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      )
                                                    ],
                                                  ),
                                                  const SizedBox(
                                                    height: 8,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                    children: [
                                                      Text(
                                                        'Age :',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      ),
                                                      const SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        '${reservations[index].age}',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      )
                                                    ],
                                                  ),
                                                  const SizedBox(
                                                    height: 8,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                    children: [
                                                      Text(
                                                        'Address : ',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      ),
                                                      const SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        '${reservations[index].address}',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      )
                                                    ],
                                                  ),
                                                  const SizedBox(
                                                    height: 8,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                    children: [
                                                      Text(
                                                        'Paid Balance : ',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      ),
                                                      const SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        '${reservations[index].cost}',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      )
                                                    ],
                                                  ),
                                                  const SizedBox(
                                                    height: 8,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                    children: [
                                                      Text(
                                                        'Reserved Places : ',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      ),
                                                      const SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        '${reservations[index].placeNum}',
                                                        style: TextStyle(
                                                            color: MyColors
                                                                .blue,
                                                            fontSize:
                                                            screenWidth *
                                                                0.011),
                                                      )
                                                    ],
                                                  ),
                                                  const SizedBox(
                                                    height: 8,
                                                  ),
                                                  TextButton(
                                                      onPressed: () {
                                                        Navigator.pop(
                                                            context);
                                                      },
                                                      child: const Text(
                                                        'Close',
                                                        style:  TextStyle(
                                                            color: Colors.black),
                                                      ))
                                                ],
                                              ),
                                            ),
                                          ),
                                        ));
                                  },
                                  child: Text(
                                    'User Details',
                                    style: TextStyle(
                                      color: Colors.blue,
                                      fontSize: screenWidth * 0.011,
                                    ),
                                  ))
                            ],
                          ),
                        ),
                        Divider(
                          color: MyColors.blue.withOpacity(.3),
                          indent: screenWidth * 0.10,
                          endIndent: screenWidth * 0.14,
                          height: .7,
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
