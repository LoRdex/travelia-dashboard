import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_organizer.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/views/home/organizer_home_screen.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/widgets/trips_items.dart';

class TripsHistoryScreen extends StatelessWidget {
  const TripsHistoryScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    final TripOrganizerCubit tripOrganizerCubit =
        BlocProvider.of<TripOrganizerCubit>(context);
    return BlocBuilder<TripOrganizerCubit, TripOrganizerState>(
      builder: (context, state) => DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.grey.withOpacity(.2),
            leading: Builder(builder: (context) {
              return IconButton(
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
                icon: Icon(
                  Icons.menu,
                  color: MyColors.blue,
                  size: screenWidth * 0.015,
                ),
              );
            }),
            title: Text(
              'Trips Log',
              style: TextStyle(
                  color: MyColors.blue, fontSize: screenWidth * 0.014),
            ),
            actions: [
              IconButton(
                onPressed: () async {
                  tripOrganizerCubit.selectedIndex = 0;
                  await tripOrganizerCubit.getOrganizedTrips();
                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      settings: const RouteSettings(name: '/'),
                      builder: (context) => const OrganizerHomeScreen(),
                    ),
                    (route) => false,
                  );
                },
                icon: Icon(
                  Icons.home_sharp,
                  size: screenWidth * 0.015,
                  color: MyColors.blue,
                ),
              ),
            ],
            bottom: const TabBar(indicatorColor: MyColors.blue,labelColor: MyColors.blue,tabs: [
              Tab(text: 'Ended Trips'),
              Tab(text: 'Current Trips'),
              Tab(
                text: 'Coming Trips',
              )
            ]),
          ),
          drawer: const CustomDrawerOrganizer(),
          body: Stack(
            fit: StackFit.expand,
            children: [
              Container(
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('images/background3.jpg'),
                        fit: BoxFit.fill)),
              ),
              const TabBarView(
                  children: [PastTrips(), CurrentTrips(), UpComingTrips()])
            ],
          ),
        ),
      ),
    );
  }
}
