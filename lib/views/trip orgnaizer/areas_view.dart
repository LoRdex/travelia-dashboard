import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/models/areas_model.dart';

import '../../constants/api.dart';



class AreasView extends StatelessWidget {
  const AreasView({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    final TripOrganizerCubit tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);
    final areas = tripOrganizerCubit.areas!.touristAreas; // Access the touristAreas list

    return BlocBuilder<TripOrganizerCubit, TripOrganizerState>(
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: MyColors.scaffoldColor,
            title: const Text(
              'Pick Destination Area',
              style: TextStyle(color: MyColors.blue),
            ),
            centerTitle: true,
          ),
          body: Stack(
            children: [
              Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/background2.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              GridView.builder(
                padding: EdgeInsets.only(
                    right: screenWidth / 16,
                    left: screenWidth / 16,
                    top: screenWidth / 22),
                itemCount: areas?.length ?? 0, // Use the length of the areas list
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisSpacing: screenWidth / 25,
                  mainAxisSpacing: screenHeight / 13,
                  childAspectRatio: 3 / 2,
                  crossAxisCount: 3,
                ),
                itemBuilder: (context, index) {
                  return GridItem(area: areas![index]);
                },
              ),
            ],
          ),
        );
      },
    );
  }
}


class GridItem extends StatelessWidget {
  final TouristAreas area; // Accept a TouristAreas object

  const GridItem({super.key, required this.area});

  @override
  Widget build(BuildContext context) {
    final TripOrganizerCubit tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);

    double screenWidth = MediaQuery.of(context).size.width;

    return MouseRegion(
      onEnter: (_) => tripOrganizerCubit.updateGrid(area.id!, true),
      onExit: (_) => tripOrganizerCubit.updateGrid(area.id!, false),
      child: BlocBuilder<TripOrganizerCubit, TripOrganizerState>(
        builder: (context, state) {
          final isHovering = state is GridHover && state.index == area.id;
          final scaleFactor = isHovering ? 1.05 : 1.0;
          final glowColor = isHovering ? MyColors.blue.withOpacity(0.8) : Colors.transparent;

          return AnimatedContainer(
            duration: const Duration(milliseconds: 800),
            curve: Curves.easeInOut,
            transform: Matrix4.identity()..scale(scaleFactor),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              gradient: const LinearGradient(
                colors: [Colors.blueAccent, Colors.cyanAccent],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              ),
              boxShadow: [
                BoxShadow(
                  color: glowColor,
                  spreadRadius: 4,
                  blurRadius: 15,
                  offset: const Offset(0, 0),
                ),
                BoxShadow(
                  color: Colors.black.withOpacity(0.3),
                  spreadRadius: 2,
                  blurRadius: 8,
                  offset: const Offset(0, 4),
                ),
              ],
              border: Border.all(color: Colors.white, width: 2),
            ),
            child: GestureDetector(
              onTap: () {
                tripOrganizerCubit.destinationController.text =
                '${area.name!},${area.country!}';
                tripOrganizerCubit.touristAreaID = area.id;
                tripOrganizerCubit.touristAreaPicked();
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    content: SizedBox(
                      height: MediaQuery.of(context).size.height / 6,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              "Are You Sure You Want to Pick ${area.name}",
                              style: TextStyle(fontSize: screenWidth * 0.012),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                  },
                                  child: const Text(
                                    'Yes',
                                    style: TextStyle(fontSize: 15, color: MyColors.blue),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text(
                                    'Close',
                                    style: TextStyle(fontSize: 15, color: Colors.black),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  image: DecorationImage(
                    image: NetworkImage('${Api.imgUrl}/${area.img}'),
                    fit: BoxFit.cover,
                    colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(isHovering ? 0.2 : 0.5),
                      BlendMode.darken,
                    ),
                  ),
                ),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      bottom: 20,
                      child: Text(
                        area.name!,
                        style: TextStyle(
                          shadows: const [
                            Shadow(color: Colors.black, offset: Offset(-1.5, -1.5)),
                            Shadow(color: Colors.black, offset: Offset(1.5, -1.5)),
                            Shadow(color: Colors.black, offset: Offset(-1.5, 1.5)),
                            Shadow(color: Colors.black, offset: Offset(1.5, 1.5)),
                          ],
                          fontSize: screenWidth * 0.012,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    if (!isHovering)
                      Positioned(
                        bottom: 10,
                        left: 20,
                        child: Text(
                          area.country!,
                          style: TextStyle(
                            shadows: const [
                              Shadow(color: Colors.black, offset: Offset(-1.5, -1.5)),
                              Shadow(color: Colors.black, offset: Offset(1.5, -1.5)),
                              Shadow(color: Colors.black, offset: Offset(-1.5, 1.5)),
                              Shadow(color: Colors.black, offset: Offset(1.5, 1.5)),
                            ],
                            fontSize: screenWidth * 0.007,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    if (!isHovering)
                      Positioned(
                        bottom: 10,
                        right: 20,
                        child: Text(
                          area.type!,
                          style: TextStyle(
                            shadows: const [
                              Shadow(color: Colors.black, offset: Offset(-1.5, -1.5)),
                              Shadow(color: Colors.black, offset: Offset(1.5, -1.5)),
                              Shadow(color: Colors.black, offset: Offset(-1.5, 1.5)),
                              Shadow(color: Colors.black, offset: Offset(1.5, 1.5)),
                            ],
                            fontSize: screenWidth * 0.007,
                            color: Colors.white,
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}






