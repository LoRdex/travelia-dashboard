import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_organizer.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/components/custom_text_form_field.dart';
import 'package:travelia_dashboard/components/dialogs.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/views/shared/my_map.dart';
import 'package:travelia_dashboard/views/home/organizer_home_screen.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/areas_view.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/choose_facilites.dart';

class CreateTripScreen extends StatelessWidget {
  const CreateTripScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final TripOrganizerCubit tripOrganizerCubit =
        BlocProvider.of<TripOrganizerCubit>(context);
    double screenWidth = MediaQuery.of(context).size.width;
    return BlocConsumer<TripOrganizerCubit, TripOrganizerState>(
      builder: (context, state) {
        return Form(key: tripOrganizerCubit.firstPageKey,
          child: Scaffold(drawer: const CustomDrawerOrganizer(),
            appBar: CustomAppBar(title: 'Create Trip'),

            body: Stack(
              fit: StackFit.expand,
              children: [
                Container(
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage('images/background5.jpg'))),
                ),
                Row(
                  children: [
                    Expanded(
                        child: Image.asset(
                      'images/trip.png',
                      width: screenWidth / 2.5,
                    )),
                    Expanded(
                        child: Center(
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: screenWidth / 10),
                            child: Image.asset(
                              'images/logo4.png',
                              width: screenWidth / 11,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: screenWidth / 12),
                            child: Text(
                              'Craft the perfect journey. Your next unforgettable experience starts here.',
                              style: TextStyle(
                                  fontSize: screenWidth * 0.011,
                                  color: MyColors.blue),
                            ),
                          ),
                          const SizedBox(
                            height: 25,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              IgnorePointer(
                                child: CustomTextFormFiled(controller: tripOrganizerCubit.startLocationController,validator: (value){
                                  if(value!.isEmpty )
                                    {
                                      return 'Please Select The Start Location';
                                    }
                                  return null;
                                },
                                  preIcon: const Icon(
                                    Icons.location_on_sharp,
                                    color: Colors.grey,
                                  ),
                                  obscureText: false,
                                  labelText: 'Start Location',
                                  divider: 3.5,
                                  labelColor: Colors.grey,
                                  color: Colors.grey,
                                ),
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              CustomButton(
                                fontSize: 110,
                                text: 'Set Location',
                                onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context) => MyMap( access: 2,),));},
                                dividerH: 16,
                                dividerW: 14,
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 25,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              IgnorePointer(
                                child: CustomTextFormFiled(controller: tripOrganizerCubit.destinationController,
                                  validator: (value){
                                  if(value!.isEmpty)
                                    {return 'Please Select a Destination';}
                                  return null;
                                  },
                                  preIcon: const Icon(
                                    Icons.travel_explore,
                                    color: Colors.grey,
                                  ),
                                  obscureText: false,
                                  labelText: 'Destination',
                                  divider: 3.5,
                                  labelColor: Colors.grey,
                                  color: Colors.grey,
                                ),
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              CustomButton(
                                fontSize: 110,
                                text: 'Destination',
                                onTap: () async{
                                  await tripOrganizerCubit.getTouristAreas();
                                },
                                dividerH: 16,
                                dividerW: 14,
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 25,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: screenWidth / 11.5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CustomTextFormFiled(controller: tripOrganizerCubit.priceController,
                                  validator: (value){
                                  if(value!.isEmpty)
                                    {
                                      return 'Set the Trip Price !';
                                    }
                                  if (!RegExp(r'^\d+$').hasMatch(value)) {
                                    return "Enter a Number";
                                  } else {
                                    return null;
                                  }
                                  },
                                  preIcon: const Icon(
                                    Icons.attach_money,
                                    color: MyColors.blue,
                                  ),
                                  obscureText: false,
                                  labelText: "Price",
                                  divider: 7.2,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                CustomTextFormFiled(controller: tripOrganizerCubit.capacityController,
                                  validator: (value){
                                    if(value!.isEmpty)
                                    {
                                      return 'Set the Trip Capacity !';
                                    }
                                    if (!RegExp(r'^\d+$').hasMatch(value)) {
                                      return "Enter a Number";
                                    } else {
                                      return null;
                                    }
                                  },
                                  preIcon: const Icon(
                                    Icons.reduce_capacity,
                                    color: MyColors.blue,
                                  ),
                                  obscureText: false,
                                  labelText: "Capacity",
                                  divider: 7.2,
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 25,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: screenWidth / 60),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                InkWell(
                                  onTap: () async {
                                    await tripOrganizerCubit
                                        .pickStartDate(context);
                                  },
                                  child: AbsorbPointer(
                                    child: CustomTextFormFiled(controller: tripOrganizerCubit.startController,
                                      readOnly: true,
                                      validator: (value){
                                      if(value!.isEmpty)
                                        {
                                          return 'Please Set The Start Date';
                                        }
                                      return null;
                                      },
                                      preIcon: const Icon(
                                        Icons.date_range,
                                        color: MyColors.blue,
                                      ),
                                      obscureText: false,
                                      labelText: "Start Date",
                                      divider: 7.2,
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                InkWell(onTap: ()async{

                                  await tripOrganizerCubit.pickEndDate(context);
                                },
                                  child: AbsorbPointer(
                                    child: CustomTextFormFiled(controller: tripOrganizerCubit.endController,
                                      readOnly: false,
                                      preIcon: const Icon(
                                        Icons.calendar_month,
                                        color: MyColors.blue,
                                      ),
                                      obscureText: false,
                                      labelText: "End Date",
                                      divider: 7.2,
                                      validator: (value){
                                        if(value!.isEmpty)
                                        {
                                          return 'Please Set The End Date';
                                        }
                                        return null;
                                      },
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                tripOrganizerCubit.daysBet != null ? SizedBox(width: screenWidth/18,
                                  child: Text(
                                    '${tripOrganizerCubit.daysBet} Days',
                                    style: TextStyle(
                                        shadows: const [
                                          Shadow(
                                              color: Colors.white,
                                              offset: Offset(-1.5, -1.5)),
                                          Shadow(
                                              color: Colors.white,
                                              offset: Offset(1.5, -1.5)),
                                          Shadow(
                                              color: Colors.white,
                                              offset: Offset(-1.5, 1.5)),
                                          Shadow(
                                              color: Colors.white,
                                              offset: Offset(1.5, 1.5)),
                                        ],
                                        fontWeight: FontWeight.bold,
                                        color: MyColors.blue,
                                        fontSize: screenWidth * 0.013),
                                  ),
                                ) :  Text('0 Days',style: TextStyle(
                                    shadows: const [
                                      Shadow(
                                          color: Colors.white,
                                          offset: Offset(-1.5, -1.5)),
                                      Shadow(
                                          color: Colors.white,
                                          offset: Offset(1.5, -1.5)),
                                      Shadow(
                                          color: Colors.white,
                                          offset: Offset(-1.5, 1.5)),
                                      Shadow(
                                          color: Colors.white,
                                          offset: Offset(1.5, 1.5)),
                                    ],
                                    fontWeight: FontWeight.bold,
                                    color: MyColors.blue,
                                    fontSize: screenWidth * 0.017),)
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 25,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: screenWidth / 11),
                            child: CustomButton(
                                text: 'Continue',
                                onTap: () async {
                                 if(tripOrganizerCubit.firstPageKey.currentState!.validate())
                                   {
                                     Navigator.push(context, MaterialPageRoute(builder: (context) => const ChooseFacilitesView(),));


                                   }
                                }),
                          )
                        ],
                      ),
                    ))
                  ],
                )
              ],
            ),
          ),
        );
      }, listener: (BuildContext context, TripOrganizerState state) {

        if(state is AreasLoadSuccess)
          {        Navigator.push(context, MaterialPageRoute(builder: (context) => const AreasView(),));
          }
        if(state is AreasLoadFailure)
          {
            Dialogs.showFailedDialog(context,"Something Went Wrong, Please Try Again");
          }
    },
    );
  }
}


