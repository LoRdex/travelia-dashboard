import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_organizer.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/components/dialogs.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/reservation_details.dart';
import '../../../constants/api.dart';
import '../../../models/organized_trips_filtered_model.dart';

class ReservationScreen extends StatelessWidget {
  const ReservationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    final TripOrganizerCubit tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);

    // Null safety: Ensure both currentTrips and upcomingTrips are not null
    final validTrips = (tripOrganizerCubit.organizedTripsFilteredModel?.currentTrips ?? []) +
        (tripOrganizerCubit.organizedTripsFilteredModel?.upcomingTrips ?? []);

    return Scaffold(
      appBar: CustomAppBar(),
      drawer: const CustomDrawerOrganizer(),
      body: Stack(
        fit: StackFit.expand,
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/background3.jpg'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          // Show message if no trips are available
          if (validTrips.isEmpty)
            Center(
              child: Text(
                'There are No Trips',
                style: TextStyle(
                  fontSize: screenWidth * 0.018,
                  color: MyColors.blue,
                ),
              ),
            )
          else
            GridView.builder(
              padding: EdgeInsets.only(
                right: screenWidth / 16,
                left: screenWidth / 16,
                top: screenWidth / 22,
              ),
              itemCount: validTrips.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: screenWidth / 25,
                mainAxisSpacing: screenHeight / 13,
                childAspectRatio: 3 / 2,
                crossAxisCount: 3,
              ),
              itemBuilder: (context, index) {
                return GridItem(trips: validTrips[index]);
              },
            ),
        ],
      ),
    );
  }
}

class GridItem extends StatelessWidget {
  final Trip trips;

  const GridItem({super.key, required this.trips});

  @override
  Widget build(BuildContext context) {
    final TripOrganizerCubit tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);
    double screenWidth = MediaQuery.of(context).size.width;

    return BlocBuilder<TripOrganizerCubit, TripOrganizerState>(
      builder: (context, state) {
        final isHovering = state is GridHover && state.index == trips.id;
        final scaleFactor = isHovering ? 1.05 : 1.0;
        final glowColor = isHovering ? MyColors.blue.withOpacity(0.8) : Colors.transparent;

        return MouseRegion(
            onEnter: (_) => context.read<TripOrganizerCubit>().updateGrid(trips.id, true),
            onExit: (_) => context.read<TripOrganizerCubit>().updateGrid(trips.id, false),
            child: AnimatedContainer(
            decoration: BoxDecoration(border: Border.all(style: BorderStyle.none)),
        transform: Matrix4.identity()..scale(scaleFactor),
        duration: const Duration(milliseconds: 800),
        curve: Curves.easeInOut,
        child: Transform.scale(
        scale: scaleFactor,
        child: GestureDetector(
        onTap: () {
        showDialog(
        context: context,
        builder: (context) => AlertDialog(
        content: SizedBox(
        height: MediaQuery.of(context).size.height / 2,
        child: Center(
        child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
        Text(
        'Trip Details:',
        style: TextStyle(
        color: MyColors.blue,
        fontSize: screenWidth * 0.012,
        fontWeight: FontWeight.bold,
        ),
        ),
        const SizedBox(height: 5),
        Text(
        'Tourist Area: ${trips.touristArea ?? 'N/A'}',
        style: TextStyle(
        color: MyColors.blue,
        fontSize: screenWidth * 0.010,
        ),
        ),
        const SizedBox(height: 8),
        Text(
        'Start Location: ${trips.strLocation ?? 'N/A'}',
        style: TextStyle(
        color: MyColors.blue,
        fontSize: screenWidth * 0.010,
        ),
        ),
        const SizedBox(height: 8),
        Text(
        'Restaurant: ${trips.restaurant ?? 'N/A'}',
        style: TextStyle(
        color: MyColors.blue,
        fontSize: screenWidth * 0.010,
        ),
        ),
        const SizedBox(height: 8),
        Text(
        'Hotel: ${trips.hotel ?? 'N/A'}',
        style: TextStyle(
        color: MyColors.blue,
        fontSize: screenWidth * 0.010,
        ),
        ),
        const SizedBox(height: 8),
        Text(
        'Transport Company: ${trips.transporter ?? 'N/A'}',
        style: TextStyle(
        color: MyColors.blue,
        fontSize: screenWidth * 0.010,
        ),
        ),
        const SizedBox(height: 8),
        Text(
        'Start Date: ${DateFormat('dd/MM/yyyy').format(trips.strDate ?? DateTime.now())}',
        style: TextStyle(
        color: MyColors.blue,
        fontSize: screenWidth * 0.010,
        ),
        ),
        const SizedBox(height: 8),
        Text(
        'End Date: ${DateFormat('dd/MM/yyyy').format(trips.endDate)}',
        style: TextStyle(
        color: MyColors.blue,
        fontSize: screenWidth * 0.010,
        ),
        ),
        const SizedBox(height: 8),
        Text(
        'Cost: ${trips.cost?.toString() ?? 'N/A'}',
        style: TextStyle(
        color: MyColors.blue,
        fontSize: screenWidth * 0.010,
        ),
        ),
        const SizedBox(height: 8),
        Text(
        'Capacity: ${trips.totalCapacity?.toString() ?? 'N/A'}',
        style: TextStyle(
        color: MyColors.blue,
        fontSize: screenWidth * 0.010,
        ),
        ),
        const SizedBox(height: 8),
        Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
        TextButton(
        onPressed: () {
        Navigator.pop(context);
        },
        child: const Text(
        'Close',
        style: TextStyle(color: Colors.black),
        ),
        ),
        TextButton(
        onPressed: () async {
        await tripOrganizerCubit.getReservation(trips.id);
        if (tripOrganizerCubit.reservationModel?.reservations == null ||
        tripOrganizerCubit.reservationModel!.reservations!.isEmpty) {
        Dialogs.showFailedDialog(context, 'There are no reservations for this trip yet');
        } else {
        Navigator.pop(context);
        Navigator.push(
        context,
        MaterialPageRoute(
        builder: (context) => const ReservationDetailsScreen(),
        ),
        );
        }
        },
        child: const Text(
        'Show Reservation',
        style: TextStyle(color: Colors.black),
        ),
        ),
        ],
        ),
        ],
        ),
        ),
        ),
        ),
        );
        },
        child: Container(
        decoration: BoxDecoration(
        gradient: const LinearGradient(
        colors: [Colors.blueAccent, Colors.cyanAccent],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        ),
        boxShadow: [
        BoxShadow(
        color: glowColor,
        spreadRadius: 4,
        blurRadius: 15,
        offset: const Offset(0, 0),
        ),
        BoxShadow(
        color: Colors.black.withOpacity(0.3),
        spreadRadius: 2,
        blurRadius: 8,
        offset: const Offset(0, 4),
        ),
        ],
        border: Border.all(color: Colors.white, width: 2),
        borderRadius: BorderRadius.circular(20),
        image: DecorationImage(
        image: NetworkImage('${Api.imgUrl}/${trips.img}'),
        fit: BoxFit.cover,
        colorFilter: isHovering
        ? const ColorFilter.mode(Colors.transparent, BlendMode.lighten)
            : ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.darken),
        ),
        ),
        child: Stack(
        alignment: Alignment.center,
        children: [
        Positioned(
        bottom: 20,
        child: Text(
        trips.touristArea,
        style: TextStyle(
        shadows: const [
        Shadow(color: Colors.black, offset: Offset(-1.5, -1.5)),
        Shadow(color: Colors.black, offset: Offset(1.5, -1.5)),
        Shadow(color: Colors.black, offset: Offset(-1.5, 1.5)),
        Shadow(color: Colors.black, offset: Offset(1.5, 1.5)),
        ],
        fontSize: screenWidth * 0.012,
        fontWeight: FontWeight.bold,
        color: Colors.white,
        ),
        ),
        ),
        if (!isHovering)
        Positioned(
        bottom: 10,
        left: 20,
        child: Text(
        DateFormat('dd/MM/yyyy').format(trips.strDate ?? DateTime.now()),
        style: TextStyle(
        shadows: const [
        Shadow(color: Colors.black, offset: Offset(-1.5, -1.5)),
        Shadow(color: Colors.black, offset: Offset(1.5, -1.5)),
        Shadow(color: Colors.black, offset: Offset(-1.5, 1.5)),
        Shadow(color: Colors.black, offset: Offset(1.5, 1.5)),
        ],
        fontSize: screenWidth * 0.007,
        color: Colors.white,
        ),
        ),
        ),
        ],
        ),
        ),
        ),
        )));
      },
    );
  }
}
