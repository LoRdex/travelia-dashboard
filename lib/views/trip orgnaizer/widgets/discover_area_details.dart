import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_organizer.dart';
import 'package:travelia_dashboard/components/custom_appbar.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/main_cubit/main_cubit.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';

import '../../../constants/api.dart';

class DiscoverAreaDetails extends StatelessWidget {
  const DiscoverAreaDetails({
    super.key,
    required this.index,
  });

  final int index;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);
    final mainCubit = BlocProvider.of<MainCubit>(context);
    final discoverAreas = tripOrganizerCubit.discoverAreasModel?.areas;
    return Scaffold(
        drawer: mainCubit.getDrawer(),
        appBar: CustomAppBar(


          title: discoverAreas?[index].name,

        ),
        body: Stack(
          fit: StackFit.expand,
          children: [
            Container(
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage('images/background3.jpg')))),
            Row(
              children: [
                Expanded(
                    child: Padding(
                  padding: EdgeInsets.only(left: screenWidth / 11),
                  child: Container(
                    width: screenWidth / 1.6,
                    height: screenHeight / 1.7,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage(
                          '${Api.imgUrl}/${discoverAreas?[index].img}',
                        ),
                      ),
                      gradient: const LinearGradient(
                        colors: [Colors.blueAccent, Colors.cyanAccent],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(
                        color: Colors.white,
                        // Set your desired border color
                        width: 3, // Set your desired border width
                      ),
                      color: Colors.black.withOpacity(.8),
                      boxShadow: [
                        const BoxShadow(
                          color: Colors.white,
                          spreadRadius: 4,
                          blurRadius: 15,
                          offset: Offset(0, 0),
                        ),
                        BoxShadow(
                          color: Colors.black.withOpacity(0.3),
                          spreadRadius: 2,
                          blurRadius: 8,
                          offset: const Offset(0, 4),
                        ),
                      ],
                    ),
                  ),
                )),
                Expanded(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(
                      height: 35,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: screenWidth / 15),
                      child: Container(
                          width: screenWidth / 2,
                          height: screenHeight / 1.2,
                          decoration: BoxDecoration(border: Border.all(color: Colors.white,width: 3),
                            borderRadius: BorderRadius.circular(30),
                            color: Colors.white.withOpacity(.3),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              children: [
                                Text(
                                  'About ${discoverAreas?[index].name} :',
                                  style: TextStyle(
                                      color: MyColors.blue,
                                      fontSize: screenWidth * 0.012,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  '${discoverAreas?[index].description}',
                                  style: TextStyle(
                                      color: MyColors.blue,
                                      fontSize: screenWidth * 0.013),
                                ),
                                const SizedBox(
                                  height: 30,
                                ),
                                Text(
                                  'Area Details :',
                                  style: TextStyle(
                                      color: MyColors.blue,
                                      fontWeight: FontWeight.bold,
                                      fontSize: screenWidth * 0.012),
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                Padding(
                                  padding:
                                      EdgeInsets.only(left: screenWidth / 25),
                                  child: Row(
                                    children: [
                                      Text(
                                        'Name :',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.012),
                                      ),
                                      const SizedBox(
                                        width: 7,
                                      ),
                                      Text(
                                        '${discoverAreas?[index].name}',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.012),
                                      ),
                                      const SizedBox(
                                        width: 40,
                                      ),
                                      Text(
                                        'Type :',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.012),
                                      ),
                                      const SizedBox(
                                        width: 7,
                                      ),
                                      Text(
                                        '${discoverAreas?[index].type}',
                                        style: TextStyle(
                                            color: MyColors.blue,
                                            fontSize: screenWidth * 0.012),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                Text(
                                  'Location Details :',
                                  style: TextStyle(
                                      color: MyColors.blue,
                                      fontWeight: FontWeight.bold,
                                      fontSize: screenWidth * 0.011),
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                SizedBox(
                                  width: screenWidth / 2,
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            'Country :',
                                            style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize: screenWidth * 0.011),
                                          ),
                                          const SizedBox(
                                            width: 7,
                                          ),
                                          Text(
                                            '${discoverAreas?[index].location?.country}',
                                            style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize: screenWidth * 0.011),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 15,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            'State :',
                                            style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize: screenWidth * 0.011),
                                          ),
                                          const SizedBox(
                                            width: 7,
                                          ),
                                          Text(
                                            '${discoverAreas?[index].location?.state}',
                                            style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize: screenWidth * 0.011),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 15,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            'City :',
                                            style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize: screenWidth * 0.011),
                                          ),
                                          const SizedBox(
                                            width: 7,
                                          ),
                                          Text(
                                            '${discoverAreas?[index].location?.city}',
                                            style: TextStyle(
                                                color: MyColors.blue,
                                                fontSize: screenWidth * 0.011),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )),
                    )
                  ],
                ))
              ],
            )
          ],
        ));
  }
}
