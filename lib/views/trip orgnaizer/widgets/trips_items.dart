import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:travelia_dashboard/components/custom_button.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/views/home/organizer_home_screen.dart';
import 'package:universal_html/html.dart';

import '../../../constants/api.dart';
import '../../../constants/my_colors.dart';
import '../../../models/organized_trips_filtered_model.dart';

class PastTrips extends StatelessWidget {
  const PastTrips({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    double screenHeight = MediaQuery
        .of(context)
        .size
        .height;
    final TripOrganizerCubit tripOrganizerCubit =
    BlocProvider.of<TripOrganizerCubit>(context);
    final pastTrips = tripOrganizerCubit.organizedTripsFilteredModel?.pastTrips;

    if (pastTrips == null || pastTrips.isEmpty) {
      return Center(
        child: Text(
          'No Trips Available',
          style: TextStyle(color: MyColors.blue, fontSize: screenWidth * 0.018),
        ),
      );
    }

    return GridView.builder(
      padding: EdgeInsets.only(
          right: screenWidth / 16,
          left: screenWidth / 16,
          top: screenWidth / 22),
      itemCount: pastTrips.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: screenWidth / 25,
        mainAxisSpacing: screenHeight / 13,
        childAspectRatio: 3 / 2,
        crossAxisCount: 3,
      ),
      itemBuilder: (context, index) {
        return GridItem(trips: pastTrips[index]);
      },
    );
  }
}

class CurrentTrips extends StatelessWidget {
  const CurrentTrips({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    double screenHeight = MediaQuery
        .of(context)
        .size
        .height;
    final TripOrganizerCubit tripOrganizerCubit =
    BlocProvider.of<TripOrganizerCubit>(context);
    final currentTrips = tripOrganizerCubit.organizedTripsFilteredModel
        ?.currentTrips;

    if (currentTrips == null || currentTrips.isEmpty) {
      return Center(
        child: Text(
          'No Trips Available',
          style: TextStyle(color: MyColors.blue, fontSize: screenWidth * 0.018),
        ),
      );
    }

    return GridView.builder(
      padding: EdgeInsets.only(
          right: screenWidth / 16,
          left: screenWidth / 16,
          top: screenWidth / 22),
      itemCount: currentTrips.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: screenWidth / 25,
        mainAxisSpacing: screenHeight / 13,
        childAspectRatio: 3 / 2,
        crossAxisCount: 3,
      ),
      itemBuilder: (context, index) {
        return GridItem(trips: currentTrips[index]);
      },
    );
  }
}

class UpComingTrips extends StatelessWidget {
  const UpComingTrips({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    double screenHeight = MediaQuery
        .of(context)
        .size
        .height;
    final TripOrganizerCubit tripOrganizerCubit =
    BlocProvider.of<TripOrganizerCubit>(context);
    final upComingTrips = tripOrganizerCubit.organizedTripsFilteredModel
        ?.upcomingTrips;

    if (upComingTrips == null || upComingTrips.isEmpty) {
      return Center(
        child: Text(
          'No Trips Available',
          style: TextStyle(color: MyColors.blue, fontSize: screenWidth * 0.018),
        ),
      );
    }

    return GridView.builder(
      padding: EdgeInsets.only(
          right: screenWidth / 16,
          left: screenWidth / 16,
          top: screenWidth / 22),
      itemCount: upComingTrips.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: screenWidth / 25,
        mainAxisSpacing: screenHeight / 13,
        childAspectRatio: 3 / 2,
        crossAxisCount: 3,
      ),
      itemBuilder: (context, index) {
        return GridItemUpComing(trips: upComingTrips[index]);
      },
    );
  }
}










class GridItem extends StatelessWidget {
  final Trip trips;

  const GridItem({super.key, required this.trips});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    final TripOrganizerCubit tripOrganizerCubit =
    BlocProvider.of<TripOrganizerCubit>(context);
    return BlocBuilder<TripOrganizerCubit, TripOrganizerState>(
      builder: (context, state) {
        final isHovering = state is GridHover && state.index == trips.id;
        final scaleFactor = isHovering ? 1.05 : 1.0;
        final glowColor =
        isHovering ? MyColors.blue.withOpacity(0.8) : Colors.transparent;
        return MouseRegion(
          onEnter: (_) =>
              context.read<TripOrganizerCubit>().updateGrid(trips.id, true),
          onExit: (_) =>
              context.read<TripOrganizerCubit>().updateGrid(trips.id, false),
          child: AnimatedContainer(
            duration: const Duration(milliseconds: 800),
            curve: Curves.easeInOut,
            transform: Matrix4.identity()..scale(scaleFactor),
            child: Transform.scale(
              scale: scaleFactor,
              child: GestureDetector(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        content: SizedBox(
                          height: MediaQuery.of(context).size.height / 2,
                          child: Center(
                            child: Column(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  'Trip Details : ',
                                  style: TextStyle(
                                      color: MyColors.blue,
                                      fontSize: screenWidth * 0.012,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Tourist Area : ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.touristArea} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Start Location : ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.strLocation} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.009),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Restaurant: ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.restaurant} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Hotel: ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.hotel} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Transport Company: ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.transporter} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Start Date: ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      DateFormat('dd/MM/yyyy')
                                          .format(trips.strDate),
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'End Date: ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      DateFormat('dd/MM/yyyy')
                                          .format(trips.endDate),
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Cost : ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.cost} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Capacity : ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.totalCapacity} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                CustomButton(dividerH: 22,dividerW: 20,fontSize: 130,text: 'Close', onTap: (){Navigator.pop(context);})
                              ],
                            ),
                          ),
                        ),
                      ));
                },
                child: Container(
                  decoration: BoxDecoration(
                    gradient: const LinearGradient(
                      colors: [Colors.blueAccent, Colors.cyanAccent],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: glowColor,
                        spreadRadius: 4,
                        blurRadius: 15,
                        offset: const Offset(0, 0),
                      ),
                      BoxShadow(
                        color: Colors.black.withOpacity(0.3),
                        spreadRadius: 2,
                        blurRadius: 8,
                        offset: const Offset(0, 4),
                      ),
                    ],
                    border: Border.all(color: Colors.white, width: 2),
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                      image: NetworkImage('${Api.imgUrl}/${trips.img}'),
                      fit: BoxFit.cover,
                      colorFilter: isHovering
                          ? const ColorFilter.mode(
                        Colors.transparent,
                        BlendMode.lighten,
                      )
                          : ColorFilter.mode(
                        Colors.black.withOpacity(0.3),
                        BlendMode.darken,
                      ),
                    ),
                  ),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Positioned(
                        bottom: 20,
                        child: Text(
                          trips.touristArea,
                          style: TextStyle(
                            shadows: const [
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(-1.5, -1.5)),
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(1.5, -1.5)),
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(-1.5, 1.5)),
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(1.5, 1.5)),
                            ],
                            fontSize: screenWidth * 0.012,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      if (!isHovering)
                        Positioned(
                          bottom: 10,
                          left: 20,
                          child: Text(
                            DateFormat('dd/MM/yyyy').format(trips.strDate),
                            style: TextStyle(
                              shadows: const [
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(-1.5, -1.5)),
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(1.5, -1.5)),
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(-1.5, 1.5)),
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(1.5, 1.5)),
                              ],
                              fontSize: screenWidth * 0.007,
                              color: Colors.white,
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}


class GridItemUpComing extends StatelessWidget {
  final Trip trips;

  const GridItemUpComing({super.key, required this.trips});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final TripOrganizerCubit tripOrganizerCubit =
    BlocProvider.of<TripOrganizerCubit>(context);
    return BlocBuilder<TripOrganizerCubit, TripOrganizerState>(
      builder: (context, state) {
        final isHovering = state is GridHover && state.index == trips.id;
        final scaleFactor = isHovering ? 1.05 : 1.0;
        final glowColor =
        isHovering ? MyColors.blue.withOpacity(0.8) : Colors.transparent;
        return MouseRegion(
          onEnter: (_) =>
              context.read<TripOrganizerCubit>().updateGrid(trips.id, true),
          onExit: (_) =>
              context.read<TripOrganizerCubit>().updateGrid(trips.id, false),
          child: AnimatedContainer(
            duration: const Duration(milliseconds: 800),
            curve: Curves.easeInOut,
            transform: Matrix4.identity()..scale(scaleFactor),
            child: Transform.scale(
              scale: scaleFactor,
              child: GestureDetector(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        content: SizedBox(
                          height: MediaQuery.of(context).size.height / 2,
                          child: Center(
                            child: Column(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  'Trip Details : ',
                                  style: TextStyle(
                                      color: MyColors.blue,
                                      fontSize: screenWidth * 0.012,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Tourist Area : ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.touristArea} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Start Location : ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.strLocation} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.009),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Restaurant: ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.restaurant} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Hotel: ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.hotel} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Transport Company: ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.transporter} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Start Date: ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      DateFormat('dd/MM/yyyy')
                                          .format(trips.strDate),
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'End Date: ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      DateFormat('dd/MM/yyyy')
                                          .format(trips.endDate),
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Cost : ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.cost} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Capacity : ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${trips.totalCapacity} ',
                                      style: TextStyle(
                                          color: MyColors.blue,
                                          fontSize: screenWidth * 0.011),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceEvenly,
                                  children: [
                                    CustomButton(
                                      color: Colors.red,
                                      fontSize: 130,
                                      dividerW: 18,
                                      dividerH: 22,
                                      text: 'Remove Trip',
                                      onTap: () async {
                                        showDialog(
                                          context: context,
                                          builder: (context) {
                                            return AlertDialog(
                                              title: Text(
                                                'Confirmation',
                                                style: TextStyle(
                                                  fontSize: screenWidth *
                                                      0.012,
                                                ),
                                              ),
                                              content: SizedBox(
                                                height:
                                                screenHeight / 3,
                                                child: Column(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceEvenly,
                                                  children: [
                                                    Text(
                                                      'Are You Sure You Want to Delete the Trip?',
                                                      style: TextStyle(
                                                        fontSize:
                                                        screenWidth *
                                                            0.010,
                                                        color: MyColors
                                                            .blue,
                                                      ),
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                      children: [
                                                        CustomButton(color: Colors.red,
                                                          fontSize: 130,
                                                          dividerW: 18,
                                                          dividerH: 22,
                                                          text:
                                                          'Yes, Delete',
                                                          onTap:
                                                              () async {
                                                           await tripOrganizerCubit
                                                                .removeTrip(
                                                                trips.id);
                                                           await tripOrganizerCubit.getTripsFiltered();
                                                           await tripOrganizerCubit.getOrganizedTrips();
                                                           Navigator.pushAndRemoveUntil(context,MaterialPageRoute(builder: (context) => OrganizerHomeScreen(),) , (route) => false,);
                                                          },
                                                        ),
                                                        CustomButton(
                                                          fontSize: 130,
                                                          dividerW: 18,
                                                          dividerH: 22,
                                                          text: 'No',
                                                          onTap: () {
                                                            Navigator.of(
                                                                context)
                                                                .pop();
                                                          },
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            );
                                          },
                                        );
                                      },
                                    ),
                                    CustomButton(fontSize: 130,
                                        dividerW: 18,
                                        dividerH: 22,
                                        text: 'Close',
                                        onTap: () {
                                          Navigator.pop(context);
                                        })
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ));
                },
                child: Container(
                  decoration: BoxDecoration(
                    gradient: const LinearGradient(
                      colors: [Colors.blueAccent, Colors.cyanAccent],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: glowColor,
                        spreadRadius: 4,
                        blurRadius: 15,
                        offset: const Offset(0, 0),
                      ),
                      BoxShadow(
                        color: Colors.black.withOpacity(0.3),
                        spreadRadius: 2,
                        blurRadius: 8,
                        offset: const Offset(0, 4),
                      ),
                    ],
                    border: Border.all(color: Colors.white, width: 2),
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                      image: NetworkImage('${Api.imgUrl}/${trips.img}'),
                      fit: BoxFit.cover,
                      colorFilter: isHovering
                          ? const ColorFilter.mode(
                        Colors.transparent,
                        BlendMode.lighten,
                      )
                          : ColorFilter.mode(
                        Colors.black.withOpacity(0.3),
                        BlendMode.darken,
                      ),
                    ),
                  ),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Positioned(
                        bottom: 20,
                        child: Text(
                          trips.touristArea,
                          style: TextStyle(
                            shadows: const [
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(-1.5, -1.5)),
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(1.5, -1.5)),
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(-1.5, 1.5)),
                              Shadow(
                                  color: Colors.black,
                                  offset: Offset(1.5, 1.5)),
                            ],
                            fontSize: screenWidth * 0.012,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      if (!isHovering)
                        Positioned(
                          bottom: 10,
                          left: 20,
                          child: Text(
                            DateFormat('dd/MM/yyyy').format(trips.strDate),
                            style: TextStyle(
                              shadows: const [
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(-1.5, -1.5)),
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(1.5, -1.5)),
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(-1.5, 1.5)),
                                Shadow(
                                    color: Colors.black,
                                    offset: Offset(1.5, 1.5)),
                              ],
                              fontSize: screenWidth * 0.007,
                              color: Colors.white,
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
