import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../constants/api.dart';
import '../../../cubits/main_cubit/main_cubit.dart';
import '../../../cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import '../../../models/profile_model.dart';

class ImageLicense extends StatelessWidget {
  const ImageLicense({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);
    final mainCubit = BlocProvider.of<MainCubit>(context);

    ProfileModel profileModel = mainCubit.profileModel!;
    return Padding(
      padding: EdgeInsets.only(left: screenWidth / 10),
      child: Container(
          width: screenWidth / 2,
          height: screenHeight / 1.2,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey, width: 1),
            image: DecorationImage(
                image: NetworkImage(
                    '${Api.imgUrl}/${profileModel.photo}')),
            borderRadius: BorderRadius.circular(30),
            color: Colors.white.withOpacity(.5),
          )),
    );
  }
}
