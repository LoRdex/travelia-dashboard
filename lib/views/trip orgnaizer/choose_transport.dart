import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';

import '../../constants/api.dart';
import '../../models/transports_model.dart';

class TransportView extends StatelessWidget {
  const TransportView({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    final TripOrganizerCubit tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);
    final transports = tripOrganizerCubit.transportModel?.transporters ?? [];

    return BlocBuilder<TripOrganizerCubit, TripOrganizerState>(

      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: MyColors.scaffoldColor,
            title: const Text(
              'Pick Transport Company',
              style: TextStyle(color: MyColors.blue),
            ),
            centerTitle: true,
          ),
          body:transports.isEmpty ? Center(
            child: Text('No Transport Companies Available',style: TextStyle(color: MyColors.blue,fontSize: screenWidth*0.018),),
          ): Stack(
            children: [
              Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/background2.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              GridView.builder(
                padding: EdgeInsets.only(
                    right: screenWidth / 16,
                    left: screenWidth / 16,
                    top: screenWidth / 22),
                itemCount: transports?.length, // Use the length of the areas list
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisSpacing: screenWidth / 25,
                  mainAxisSpacing: screenHeight / 13,
                  childAspectRatio: 3 / 2,
                  crossAxisCount: 3,
                ),
                itemBuilder: (context, index) {
                  return GridItem(transporters: transports![index]);
                },
              ),
            ],
          ),
        );
      },
    );
  }
}


class GridItem extends StatelessWidget {
  final Transporters transporters; // Accept a TouristAreas object

  const GridItem({super.key, required this.transporters});

  @override
  Widget build(BuildContext context) {
    final TripOrganizerCubit tripOrganizerCubit = BlocProvider.of<TripOrganizerCubit>(context);

    double screenWidth = MediaQuery.of(context).size.width;
    return BlocBuilder<TripOrganizerCubit, TripOrganizerState>(
      builder: (context, state) {
        final isHovering = state is GridHover && state.index == transporters.id;
        final scaleFactor = isHovering ? 1.05 : 1.0;
        final glowColor = isHovering ? MyColors.blue.withOpacity(0.8) : Colors.transparent;
        return MouseRegion(
          onEnter: (_) =>
              context.read<TripOrganizerCubit>().updateGrid(transporters.id!, true),
          onExit: (_) =>
              context.read<TripOrganizerCubit>().updateGrid(transporters.id!, false),
          child: AnimatedContainer(
            duration: const Duration(milliseconds: 800),
            transform: Matrix4.identity()..scale(scaleFactor),
            decoration:  BoxDecoration(border: Border.all(style: BorderStyle.none)),
            curve: Curves.easeInQuad,
            child: Transform.scale(
              scale: scaleFactor,
              child: GestureDetector(
                onTap: (){
                  tripOrganizerCubit.transportController.text=transporters.name!;
                  tripOrganizerCubit.transporterID=transporters.id;
                  tripOrganizerCubit.transportPicked();
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        content: SizedBox(
                          height: MediaQuery.of(context).size.height / 6,
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  "Are You Sure You Want to Pick ${transporters.name}",
                                  style:  TextStyle(fontSize:screenWidth*0.012 ),
                                ),
                                Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    TextButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                          Navigator.pop(context);
                                        },
                                        child: const Text(
                                          'Yes',
                                          style: TextStyle(
                                              fontSize: 15, color: Colors.black),
                                        )),
                                    TextButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        child: const Text(
                                          'Close',
                                          style: TextStyle(
                                              fontSize: 15, color: Colors.black),
                                        )),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ));




                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    gradient: const LinearGradient(
                      colors: [Colors.blueAccent, Colors.cyanAccent],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: glowColor,
                        spreadRadius: 4,
                        blurRadius: 15,
                        offset: const Offset(0, 0),
                      ),
                      BoxShadow(
                        color: Colors.black.withOpacity(0.3),
                        spreadRadius: 2,
                        blurRadius: 8,
                        offset: const Offset(0, 4),
                      ),
                    ],
                    border: Border.all(color: Colors.white, width: 2),
                    image: DecorationImage(
                      image: NetworkImage('${Api.imgUrl}/${transporters.img}'),
                      fit: BoxFit.cover,

                      colorFilter: isHovering
                          ? const ColorFilter.mode(
                        Colors.transparent,
                        BlendMode.lighten,
                      )
                          : ColorFilter.mode(
                        Colors.black.withOpacity(0.3),
                        BlendMode.darken,
                      ),
                    ),
                  ),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Positioned(
                        bottom: 20,
                        child: Text(
                          transporters.name!,
                          style: TextStyle(
                            shadows: const [
                              Shadow(color: Colors.black, offset: Offset(-1.5, -1.5)),
                              Shadow(color: Colors.black, offset: Offset(1.5, -1.5)),
                              Shadow(color: Colors.black, offset: Offset(-1.5, 1.5)),
                              Shadow(color: Colors.black, offset: Offset(1.5, 1.5)),
                            ],
                            fontSize: screenWidth * 0.012,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      if (!isHovering)
                        Positioned(
                          bottom: 10,
                          left: 20,
                          child: Text(

                            transporters.type!,
                            style: TextStyle(
                              shadows: const [
                                Shadow(color: Colors.black, offset: Offset(-1.5, -1.5)),
                                Shadow(color: Colors.black, offset: Offset(1.5, -1.5)),
                                Shadow(color: Colors.black, offset: Offset(-1.5, 1.5)),
                                Shadow(color: Colors.black, offset: Offset(1.5, 1.5)),
                              ],
                              fontSize: screenWidth * 0.007,
                              color: Colors.white,
                            ),
                          ),
                        ),

                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
