import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:travelia_dashboard/constants/cache_service.dart';
import 'package:travelia_dashboard/models/login_model.dart';
import 'package:universal_html/html.dart' as html;
import 'package:dio/dio.dart';

import '../../constants/api.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  UserRole roleSelected = UserRole.tripOrganizer;
  TextEditingController ipController = TextEditingController();
  TextEditingController portController = TextEditingController();
  AuthCubit() : super(AuthInitial());
   int roleValue=2;
  html.File? imageFile;
  TextEditingController emailController = TextEditingController();
  TextEditingController regEmailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController regPasswordController = TextEditingController();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  LoginModel? loginModel;
  final formKey = GlobalKey<FormState>();
  final regFormKey = GlobalKey<FormState>();
  final ipPortKey = GlobalKey<FormState>();
  bool isLoading = false;
  bool isVisible = true;
  IconData passwordIcon = Icons.visibility_outlined;

  void showPassword() {
    isVisible = !isVisible;
    isVisible
        ? passwordIcon = Icons.visibility_outlined
        : passwordIcon = Icons.visibility_off_outlined;
    emit(PasswordState());
  }
Color? colors = Colors.blueGrey.shade100;
  Future<void> pickImage() async {
    emit(ImageLoading());

    final picker = html.InputElement(type: 'file')..accept = 'image/*';
    picker.click();

    picker.onChange.listen((event) {
      final file = picker.files!.first;
      final reader = html.FileReader();

      // Check if the file is an image
      if (!file.type.startsWith('image/')) {
        emit(ImageWrongExt());
        return;
      }

      reader.readAsDataUrl(file);
      reader.onLoadEnd.listen((event) {
        imageFile = file;
        emit(ImagePicked());
      });

      reader.onError.listen((event) {
        emit(ImagePickFail());
      });
    });
  }

  emailValidator(String email) {
    final emailRegex =
        RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$");
    return emailRegex.hasMatch(email);
  }

  bool passwordValidator(String password) {
    return password.length >= 8;
  }

  void selectRole(UserRole role) {
    roleSelected = role;
    roleValue = getRoleValue(role);
    emit(RoleSelected(role));
  }

  String enumToString(UserRole role) {
    switch (role) {
      case UserRole.restaurant:
        return 'Restaurant';
      case UserRole.hotel:
        return 'Hotel';
      case UserRole.tripOrganizer:
        return 'Trip Organizer';
      case UserRole.transportCompany:
        return 'Transport Company';
      default:
        return '';
    }
  }

  int getRoleValue(UserRole role) {
    switch (role) {
      case UserRole.restaurant:
        return 4;
      case UserRole.hotel:
        return 3;
      case UserRole.tripOrganizer:
        return 2;
      case UserRole.transportCompany:
        return 5;
      default:
        return 0;
    }
  }

  login({required String email, required String password}) async {
    emit(LoginLoadingState());
    try {
      var data = json.encode({"email": email, "password": password});
      var response = await Api.dio.post(
        '${Api.baseUrl}/login',
        data: data,
      );
      if (response.statusCode == 200) {
        loginModel = LoginModel.fromJson(response.data);
        CacheService.saveData(key: 'token', value: loginModel!.token);
        CacheService.saveData(key: 'role_name', value: loginModel!.roleName);
        CacheService.saveData(key: 'role_id', value: loginModel!.roleId);
        CacheService.saveData(key: 'wallet', value: loginModel!.wallet.toString());
        CacheService.saveData(key: 'confirmation', value: loginModel!.confirmation);
        debugPrint(json.encode(response.data));
        debugPrint(loginModel!.roleName);
        emailController.clear();
        passwordController.clear();
        emit(LoginSuccessState(loginModel!));
      } else {
        emit(LoginFailureState(response.statusMessage ?? 'Unknown error'));
        debugPrint(response.statusMessage);
      }
    } catch (e) {
      emit(LoginFailureState(e.toString()));
      debugPrint(e.toString());
    }
  }


  register({
    required String firstName,
    required String lastName,
    required int roleId,
    required String email,
    required String password,

    required html.File imageFile,
  }) async {
    emit(RegisterLoadingState());
    final reader = html.FileReader();

    reader.readAsArrayBuffer(imageFile);
    await reader.onLoad.first;

    List<int> imageBytes =
        reader.result as List<int>;
    var formData = FormData.fromMap({
      'firstName': firstName,
      'lastName': lastName,
      'role_id': roleId,
      'email': email,
      'password': password,
      'photo': MultipartFile.fromBytes(
        imageBytes,
        filename: imageFile.name,
      ),
    });

    try {
      Response response = await Api.dio.post(
        '${Api.baseUrl}/register',
        data: formData,
      );

      if (response.statusCode == 200) {
        emit(RegisterSuccessState());
        emailController = TextEditingController(text: regEmailController.text);
        passwordController = TextEditingController(text: regPasswordController.text);
        regPasswordController.clear();
        regEmailController.clear();
        firstNameController.clear();
        lastNameController.clear();
      } else {
        debugPrint('Error: ${response.statusCode} ${response.statusMessage}');
        emit(RegisterFailureState(
            'Error: ${response.statusCode} ${response.statusMessage}'));
      }
    } on DioException catch (e) {
      if (e.response != null) {
        debugPrint('Dio error!');
        debugPrint('STATUS: ${e.response?.statusCode}');
        emit(RegisterFailureState(
            'Dio Error: ${e.response?.statusCode} ${e.response?.statusMessage}'));
      }
    } catch (e) {
      debugPrint('An unexpected error occurred: ${e.toString()}');
      emit(RegisterFailureState(
          'An unexpected error occurred: ${e.toString()}'));
    }
  }

  imageRemoved(){
    imageFile = null ;
    emit(ImageRemoved());
  }


}
