part of 'auth_cubit.dart';

enum UserRole {tripOrganizer , hotel, restaurant, transportCompany }

@immutable
abstract class AuthState {}

class AuthInitial extends AuthState {}

class RoleSelected extends AuthState {
 final UserRole role;

 RoleSelected(this.role);
}

class PasswordState extends AuthState {}
class ImagePicked extends AuthState {}
class ImageRemoved extends AuthState {}
class ImageLoading extends AuthState {}
class ImageWrongExt extends AuthState {
 final String imgErrWrongExt='Only Images are Valid';
}
class ImagePickFail extends AuthState {
 final String imgErrMessage='Please Upload Image Again';

}

class LoginLoadingState extends AuthState {}
class RegisterLoadingState extends AuthState {}
class RegisterSuccessState extends AuthState {}
class RegisterFailureState extends AuthState {
 String? errMessage;
 RegisterFailureState(this.errMessage);

}

class LoginSuccessState extends AuthState {

 final LoginModel loginModel;

  LoginSuccessState(this.loginModel);
}

class LoginFailureState extends AuthState {
  String? errMessage;

 LoginFailureState(this.errMessage);
}
