
part of 'facility_cubit.dart';
enum TransportType {air_Transporation , ground_Transporation}

@immutable
sealed class FacilityState {}

final class FacilityInitial extends FacilityState {}
final class ImageTooLarge extends FacilityState {}
final class FacilityCreateLoading extends FacilityState {}
final class FacilityCreateSuccess extends FacilityState {}
final class HotelCreateLoading extends FacilityState {}
final class RestaurantCreateLoading extends FacilityState {}
final class RestaurantCreateSuccess extends FacilityState {}
final class TransportAirCreateSuccess extends FacilityState {}
final class TransportLandCreateSuccess extends FacilityState {}
final class TransportLandCreateLoading extends FacilityState {}
final class TransportLandCreateFailure extends FacilityState {

  final String errMessage;

  TransportLandCreateFailure(this.errMessage);
}
final class TransportAirCreateLoading extends FacilityState {}
final class TransportAirCreateFailure extends FacilityState {
  final String errMessage;

  TransportAirCreateFailure(this.errMessage);

}
final class RestaurantCreateFailure extends FacilityState {
  final String errMessage;

  RestaurantCreateFailure(this.errMessage);
}
final class HotelCreateSuccess extends FacilityState {}
final class HotelCreateFailure extends FacilityState {
  final String errMessage;

  HotelCreateFailure(this.errMessage);
}
final class FacilityCreateFailure extends FacilityState {
  final String errMessage;

  FacilityCreateFailure(this.errMessage);
}
final class ImagesLoading extends FacilityState {}
final class ImagesRemoved extends FacilityState {}
final class ImagesWrongExt extends FacilityState {
  final String errMessage = 'Only Images are Valid';

}
final class ImagesWrongNumber extends FacilityState {
  final String errMessage = 'You Need to Pick 3 Images';
}
final class ImagesPicked extends FacilityState {
  final html.File files;
  ImagesPicked(this.files);
}
final class ImagesPickFail extends FacilityState {
  final String errMessage = 'Error Occurred Try to Pick Again Please';
}
final class CheckBoxUpdate extends FacilityState {}
final class TypeSelected extends FacilityState {
  final TransportType transportType;

  TypeSelected(this.transportType);


}

final class MapInitial extends FacilityState {}
final class MapLoading extends FacilityState {}
final class MapSuccess extends FacilityState {}
final class MapFailure extends FacilityState {
  final String errMessage;

  MapFailure(this.errMessage); }