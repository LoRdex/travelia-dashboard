import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

part 'step_state.dart';

class StepCubit extends Cubit<int> {
  StepCubit() : super(0);


  nextStep() {
    debugPrint('Function Called already');
    if (state < 3) emit(state + 1);
  }

  previousStep() {
    if (state > 0) emit(state - 1);
  }

  void resetStep() {
    emit(0); // Reset the step to the first step (0 index)
  }
}
