part of 'step_cubit.dart';

@immutable
sealed class StepState {}

final class StepInitial extends StepState {}
