import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:travelia_dashboard/constants/api.dart';
import 'package:travelia_dashboard/constants/cache_service.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/views/facility_create/facility_setup_screen.dart';
import 'package:travelia_dashboard/views/home/hotel_home_screen.dart';
import 'package:travelia_dashboard/views/home/organizer_home_screen.dart';
import 'package:travelia_dashboard/views/home/restaurant_home_screen.dart';
import 'package:travelia_dashboard/views/home/transporter_home_screen.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/create_trip.dart';
import 'package:universal_html/html.dart' as html;
import 'package:bloc/bloc.dart';
import 'package:universal_html/js.dart';

part 'facility_state.dart';

class FacilityCubit extends Cubit<FacilityState> {
  FacilityCubit() : super(FacilityInitial());
  TransportType transportType = TransportType.ground_Transporation;
  double stars = 1;

  html.File? imageFile;
  List<String> selectedTypes = [];
  String? selectedTypesVar;
  String? selectedTypesTransport;

  TextEditingController nameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController latitudeController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController stateController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController longitudeController = TextEditingController();
  TextEditingController quantity1 = TextEditingController();
  TextEditingController quantity2 = TextEditingController();
  TextEditingController quantity3 = TextEditingController();
  TextEditingController price1 = TextEditingController();
  TextEditingController price2 = TextEditingController();
  TextEditingController price3 = TextEditingController();

  TextEditingController quantityHotel1 = TextEditingController();
  TextEditingController quantityHotel2 = TextEditingController();
  TextEditingController quantityHotel3 = TextEditingController();
  TextEditingController priceHotel1 = TextEditingController();
  TextEditingController priceHotel2 = TextEditingController();
  TextEditingController priceHotel3 = TextEditingController();

  TextEditingController busQuantity = TextEditingController();
  TextEditingController smallBusQuantity = TextEditingController();
  TextEditingController vanQuantity = TextEditingController();

  TextEditingController busCapacity = TextEditingController();
  TextEditingController smallBusCapacity = TextEditingController();
  TextEditingController vanCapacity = TextEditingController();

  TextEditingController busPrice = TextEditingController();
  TextEditingController smallBusPrice = TextEditingController();
  TextEditingController vanPrice = TextEditingController();

  TextEditingController planeQuantity = TextEditingController();
  TextEditingController vipPlaneQuantity = TextEditingController();

  TextEditingController planeCapacity = TextEditingController();
  TextEditingController vipPlaneCapacity = TextEditingController();

  TextEditingController planePrice = TextEditingController();
  TextEditingController vipPlanePrice = TextEditingController();

  bool isFastFoodChecked = false;
  bool isEasternChecked = false;
  bool isCafeChecked = false;
  bool isSeaFoodChecked = false;
  bool isWesternChecked = false;
  bool isBuffetChecked = false;

  final mapKey = GlobalKey<FormState>();
  final formKey = GlobalKey<FormState>();
  final resKey = GlobalKey<FormState>();
  final hotelKey = GlobalKey<FormState>();
  final groundKey = GlobalKey<FormState>();
  final airKey = GlobalKey<FormState>();

  removeImage(){

    imageFile = null;
    emit(ImagesRemoved());
  }


  pickImages() async {
    emit(ImagesLoading());

    final picker = html.InputElement(type: 'file')
      ..accept = 'image/*'
      ..multiple = false; // Ensure only one file is picked
    picker.click();

    picker.onChange.listen((event) {
      final files = picker.files;
      if (files == null || files.length != 1) {
        emit(ImagesWrongNumber());
        return;
      }

      final file = files[0];
      if (!file.type.startsWith('image/')) {
        emit(ImagesWrongExt());
        return;
      }
      if (file.size > 512 * 1024) {
        emit(ImageTooLarge()); // Emit a state indicating the image is too large
        return;
      }

      final reader = html.FileReader();
      reader.readAsDataUrl(file);

      reader.onLoadEnd.listen((event) {
        imageFile = file;
        emit(ImagesPicked(imageFile!));
      });

      reader.onError.listen((event) {
        emit(ImagesPickFail());
      });
    });
  }

  updateCheckBox(int checkBoxId, bool value) {
    switch (checkBoxId) {
      case 1:
        isSeaFoodChecked = value;
        break;
      case 2:
        isCafeChecked = value;
        break;
      case 3:
        isBuffetChecked = value;
        break;
      case 4:
        isFastFoodChecked = value;
        break;
      case 5:
        isEasternChecked = value;
        break;
      case 6:
        isWesternChecked = value;
        break;
    }

    emit(CheckBoxUpdate());
  }

  selectType(TransportType transportType) {
    this.transportType = transportType;
    emit(TypeSelected(transportType));
  }

  String enumToString(TransportType transportType) {
    switch (transportType) {
      case TransportType.air_Transporation:
        selectedTypesTransport = 'air';
        return 'Air Transport';
      case TransportType.ground_Transporation:
        selectedTypesTransport = 'land';
        return 'Ground Transport';
    }
  }

  restaurantRoleSelected() {
    if (isSeaFoodChecked) {
      selectedTypes.add('Sea Food');
    }
    if (isCafeChecked) {
      selectedTypes.add('Cafe');
    }
    if (isEasternChecked) {
      selectedTypes.add('Eastern');
    }
    if (isWesternChecked) {
      selectedTypes.add('Western');
    }
    if (isBuffetChecked) {
      selectedTypes.add('Buffet');
    }
    if (isFastFoodChecked) {
      selectedTypes.add('Fast Food');
    }

    return selectedTypes.join(', ');
  }

  void locationPicked() {
    emit(MapLoading());
    if (longitudeController.text.isNotEmpty &&
        latitudeController.text.isNotEmpty &&
        addressController.text.isNotEmpty &&
        countryController.text.isNotEmpty &&
        stateController.text.isNotEmpty) {
      emit(MapSuccess());
    } else {
      emit(MapFailure(' Error Occurred Try to Pick Location Again '));
    }
  }

  dynamic getType(int role) {
    if (role == 3) {
      return stars.toString();
    } else if (role == 4) {
      selectedTypesVar = selectedTypes.join(',');
      debugPrint('selectedTypesVar watch it: $selectedTypesVar');
      return selectedTypesVar = restaurantRoleSelected();
    } else if (role == 5) {
      if (transportType == TransportType.air_Transporation) {
        selectedTypesTransport = 'air';
      } else {
        selectedTypesTransport = 'land';
      }
      return selectedTypesTransport;
    } else if (role == 2) {
      return 'trip_organizer';
    }
  }

  sendFacilityData(BuildContext context) async {
    debugPrint('Sending facility data...');
    emit(FacilityCreateLoading());

    final token = CacheService.getData(key: 'token');
    final roleId = CacheService.getData(key: 'role_id');

    Api.dio = Dio(BaseOptions(
      baseUrl: Api.baseUrl,
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    ));

    var formData = FormData();

    formData.fields.addAll([
      MapEntry('name', nameController.text),
      MapEntry('description', descriptionController.text),
      MapEntry('type', getType(roleId)),
      MapEntry('latitude', latitudeController.text),
      MapEntry('longitude', longitudeController.text),
      MapEntry('address', addressController.text),
      MapEntry('state', stateController.text),
      MapEntry('country', countryController.text),
      MapEntry('city', cityController.text),
    ]);

    // Assuming you have a single image file (html.File) named 'imageFile'
    if (imageFile != null) {
      debugPrint('Processing image file: ${imageFile!.name}');
      final reader = html.FileReader()..readAsArrayBuffer(imageFile!);
      await reader.onLoad.first;
      final bytes = reader.result as List<int>;

      var multipartFile = MultipartFile.fromBytes(
        bytes,
        filename: imageFile!.name,
        // Uncomment and set the content type if required by your backend
        // contentType: MediaType('image', 'jpeg'),
      );

      formData.files.add(MapEntry(
        'img', // Adjusted to match backend expectation
        multipartFile,
      ));
    } else {
      debugPrint('No image file selected.');
      // Handle the case where no image file is selected.
      // You can emit an appropriate state (e.g., FacilityCreateFailure).
    }


    // Send the data using Dio
    try {
      var response = await Api.dio.post('/createAccount', data: formData);
      if (response.statusCode == 200) {
        debugPrint('Upload successful.');
        emit(FacilityCreateSuccess());
        nameController.clear();
        descriptionController.clear();
        latitudeController.clear();
        addressController.clear();
        countryController.clear();
        stateController.clear();
        cityController.clear();
        longitudeController.clear();
        imageFile = null;
        if (CacheService.getData(key: 'role_id') == 3) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => const HotelHomeScreen(),
            ),
            (route) => false,
          );
        }
        if (CacheService.getData(key: 'role_id') == 4) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => const RestaurantHomeScreen(),
            ),
            (route) => false,
          );
        }
        if (CacheService.getData(key: 'role_id') == 5) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => const TransporterHomeScreen(),
            ),
            (route) => false,
          );
        }
      } else {
        emit(FacilityCreateFailure(response.statusMessage ?? 'Unknown error'));
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) =>
                FacilitySetupScreen(CacheService.getData(key: 'role_id')),
          ),
          (route) => false,
        );
      }
    } catch (e) {
      emit(FacilityCreateFailure(e.toString()));
    }
  }

  createRooms() async {
    emit(HotelCreateLoading());
    try {
      var data = json.encode({
        "num1": quantityHotel1.text,
        "cost1": priceHotel1.text,
        "num2": quantityHotel2.text,
        "cost2": priceHotel2.text,
        "num3": quantityHotel3.text,
        "cost3": priceHotel3.text
      });

      var response = await Api.dio.post('${Api.baseUrl}/createRooms',
          data: data,
          options: Options(headers: {
            'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
          }));
      if (response.statusCode == 200) {
        emit(HotelCreateSuccess());
        quantityHotel1.clear();
        quantityHotel2.clear();
        quantityHotel3.clear();
        priceHotel1.clear();
        priceHotel2.clear();
        priceHotel3.clear();

      } else {
        emit(HotelCreateFailure(response.statusMessage ?? 'Unknown Error'));
        debugPrint(response.statusMessage);
      }
    } on Exception catch (e) {
      emit(HotelCreateFailure(e.toString()));
      debugPrint(e.toString());
    }
  }

  createTables() async {
    emit(RestaurantCreateLoading());

    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var data = json.encode({
        "num1": quantity1.text,
        "cost1": price1.text,
        "num2": quantity2.text,
        "cost2": price2.text,
        "num3": quantity3.text,
        "cost3": price3.text
      });
      var response = await Api.dio.post('${Api.baseUrl}/createTables',
          data: data, options: Options(headers: headers));
      if (response.statusCode == 200) {
        emit(RestaurantCreateSuccess());
        quantity1.clear();
        price1.clear();
        quantity2.clear();
        price2.clear();
        quantity3.clear();
        price3.clear();
        selectedTypes = [];
      } else {
        emit(
            RestaurantCreateFailure(response.statusMessage ?? "unknown error"));
        debugPrint(response.statusMessage);
      }
    } catch (e) {
      emit(RestaurantCreateFailure(e.toString()));
      debugPrint("ERROR : ${e.toString()}");
    }
  }

  createAirTransport() async {
    emit(TransportAirCreateLoading());

    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var data = json.encode({
        "num1": planeQuantity.text,
        "totalCapacity1": planeCapacity.text,
        "cost1": planePrice.text,
        "num2": vipPlaneQuantity.text,
        "totalCapacity2": vipPlaneCapacity.text,
        "cost2": vipPlanePrice.text
      });
      var response = await Api.dio.post(
          '${Api.baseUrl}/createAirTransportations',
          data: data,
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        emit(TransportAirCreateSuccess());
        planeQuantity.clear();
        planeCapacity.clear();
        planePrice.clear();
        vipPlanePrice.clear();
        vipPlaneCapacity.clear();
        vipPlaneQuantity.clear();
      } else {
        emit(TransportAirCreateFailure(
            response.statusMessage ?? "Unknown Error"));
      }
    } catch (e) {
      emit(TransportAirCreateFailure(e.toString()));
    }
  }

  createLandTransport() async {
    emit(TransportLandCreateLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var data = json.encode({
        "num1": busQuantity.text,
        "totalCapacity1": busCapacity.text,
        "cost1": busPrice.text,
        "num2": smallBusQuantity.text,
        "totalCapacity2": smallBusCapacity.text,
        "cost2": smallBusPrice.text,
        "num3": vanQuantity.text,
        "totalCapacity3": vanCapacity.text,
        "cost3": vanPrice.text
      });
      var response = await Api.dio.post(
          '${Api.baseUrl}/createLandTransportations',
          data: data,
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        emit(TransportLandCreateSuccess());
        busQuantity.clear();
        busCapacity.clear();
        busPrice.clear();
        smallBusQuantity.clear();
        smallBusCapacity.clear();
        smallBusPrice.clear();
        vanQuantity.clear();
        vanCapacity.clear();
        vanPrice.clear();
      } else {
        emit(TransportLandCreateFailure(
            response.statusMessage ?? "Unknown Error"));
      }
    } catch (e) {
      emit(TransportLandCreateFailure(e.toString()));

    }
  }

  sendFacilityDataOrganizer(BuildContext context) async {

    emit(FacilityCreateLoading());

    final token = CacheService.getData(key: 'token');
    final roleId = CacheService.getData(key: 'role_id');

    Api.dio = Dio(BaseOptions(
      baseUrl: Api.baseUrl,
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    ));

    var formData = FormData();

    formData.fields.addAll([
      MapEntry('name', nameController.text),
      MapEntry('description', descriptionController.text),
      const MapEntry('type', 'Trip_organizer'),
      MapEntry('latitude', latitudeController.text),
      MapEntry('longitude', longitudeController.text),
      MapEntry('address', addressController.text),
      MapEntry('state', stateController.text),
      MapEntry('country', countryController.text),
      MapEntry('city', cityController.text),
    ]);
    if (imageFile != null) {
      debugPrint('Processing image file: ${imageFile!.name}');
      final reader = html.FileReader()..readAsArrayBuffer(imageFile!);
      await reader.onLoad.first;
      final bytes = reader.result as List<int>;

      var multipartFile = MultipartFile.fromBytes(
        bytes,
        filename: imageFile!.name,
      );
      formData.files.add(MapEntry(
        'img',
        multipartFile,
      ));
    } else {
      debugPrint('No image file selected.');
    }
    debugPrint('Image file added to FormData.');

    // Send the data using Dio
    try {
      var response = await Api.dio.post('/createAccount', data: formData);
      if (response.statusCode == 200) {
        emit(FacilityCreateSuccess());
        imageFile = null;
        nameController.clear();
        descriptionController.clear();
        latitudeController.clear();
        longitudeController.clear();
        addressController.clear();
        stateController.clear();
        cityController.clear();
        countryController.clear();
        if (CacheService.getData(key: 'role_id') == 2) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => const OrganizerHomeScreen(),
            ),
            (route) => false,
          );
        }

        debugPrint('Upload successful.');
      } else {
        debugPrint('Error: ${response.statusCode} ${response.statusMessage}');
        emit(FacilityCreateFailure(response.statusMessage ?? 'Unknown error'));
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => FacilitySetupScreen(2),
          ),
          (route) => false,
        );
      }
    } catch (e) {

      emit(FacilityCreateFailure(e.toString()));
    }
  }
}
