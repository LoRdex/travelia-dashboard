import 'dart:js_interop';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:travelia_dashboard/constants/api.dart';
import 'package:travelia_dashboard/models/available_vehciles_model.dart';
import 'package:travelia_dashboard/models/reservation_model.dart';
import 'package:travelia_dashboard/models/reservations_by_organizers.dart';
import 'package:travelia_dashboard/models/transporter_routes_model.dart';

import '../../constants/cache_service.dart';
import '../../constants/my_colors.dart';

part 'transporter_state.dart';

class TransporterCubit extends Cubit<TransporterState> {
  TransporterCubit() : super(TransporterInitial());
  TransporterRoutesModel? transporterRoutesModel;
  ReservationByOrganizersModel? transporterReservationByOrganizerModel;
  ReservationModel? reservationModel;
  String? transportType;
  double? strLatitude;
  double? strLongitude;
  double? endLongitude;
  double? endLatitude;
  String? strAddress;
  String? endAddress;
  String? strCountry;
  String? endCountry;
  String? strState;
  String? endState;
  String? strCity;
  String? endCity;
  DateTime? date;
  TextEditingController transporterStartController = TextEditingController();
  TextEditingController transporterEndController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController vehicleController = TextEditingController();

  AvailableVehiclesModel? availableVehiclesModel;
  final createRouteKey = GlobalKey<FormState>();

  startLocationPicked() {
    emit(LocationPickLoading());
    if (strLongitude != null &&
        strLatitude != null &&
        strCity != null &&
        strCountry != null &&
        strState != null &&
        strAddress != null) {
      emit(LocationPickSuccess());
    } else {
      emit(LocationPickFailure());
    }
  }

  endLocationPicked() {
    emit(EndLocationPickLoading());
    if (endLongitude != null &&
        endLatitude != null &&
        endCity != null &&
        endCountry != null &&
        endState != null &&
        endAddress != null) {
      emit(EndLocationPickSuccess());
    } else {
      emit(EndLocationPickFailure());
    }
  }

  pickStartDate(BuildContext context) async {
    emit(DateLoading());
    DateTime initialDate = date ?? DateTime.now();
    DateTime firstDate = DateTime.now();
    DateTime lastDate =
       DateTime(2025);

    final picker = await showDatePicker(
      helpText: 'Starting Day',
      context: context,
      initialDate: initialDate,
      firstDate: firstDate,
      lastDate: lastDate,
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(
              primary: MyColors.blue,
              onPrimary: MyColors.scaffoldColor,
              onSurface: MyColors.blue,
            ),
            dialogBackgroundColor: Colors.white,
          ),
          child: child!,
        );
      },
    );

    if (picker != null) {

      date = picker;
      dateController .text = DateFormat('yyyy-MM-dd').format(picker);
      emit(DatePicked(date!));
      getAvailableVehicles();
    }
  }



  getTransporterRoutes() async {
    emit(GetRoutesLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.get('${Api.baseUrl}/getTransporterRoutes',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        transporterRoutesModel = TransporterRoutesModel.fromJson(response.data);
        emit(GetRoutesSuccess(transporterRoutesModel!));

      } else {
        emit(GetRoutesFailure(response.statusMessage!));
      }
    } catch (e) {
      emit(GetRoutesFailure(e.toString()));
    }
  }

  getAvailableVehicles()async{
    emit(GetVehiclesLoading());
   try{
     var headers= {
       'Authorization': 'Bearer ${CacheService.getData(key: 'token')}',
     };
     var data = {
       "date" : dateController.text
     };
     var response = await Api.dio.post('${Api.baseUrl}/getTransportationsForRoutes',options: Options(headers: headers),data: data);
     if(response.statusCode==200)
       {
         availableVehiclesModel=AvailableVehiclesModel.fromJson(response.data);
         emit(GetVehiclesSuccess(availableVehiclesModel!));
       }
     else{emit(GetVehiclesFailure(response.statusMessage!));}
   }
       catch(e){emit(GetVehiclesFailure(e.toString()));}
  }
  createRoute() async {
    emit(RouteCreateLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}',
      };
      var data = {
        "transportation_type": vehicleController.text,
        "strLatitude": strLatitude.toString(),
        "strLongitude": strLongitude.toString(),
        "strAddress": strAddress,
        "strCountry": strCountry,
        "strState": strState,
        "strCity": strCity,
        "endLatitude": endLatitude.toString(),
        "endLongitude": endLongitude.toString(),
        "endAddress": endAddress,
        "endCountry": endCountry,
        "endState": endState,
        "endCity": endCity,
        "dateTime": dateController.text
      };


      var response = await Api.dio.post('${Api.baseUrl}/createRouting',
          data: data, options: Options(headers: headers));
      debugPrint("Response: ${response.data}");  // Print response data

      if (response.statusCode == 200) {
        emit(RouteCreateSuccess());
        dateController.clear();
        vehicleController.clear();
        transporterEndController.clear();
        transporterStartController.clear();
      } else {
        emit(RouteCreateFailure(response.statusMessage!));
      }
    } catch (e) {

      emit(RouteCreateFailure(e.toString()));
    }
  }
  getReservation(int routeId) async {

    emit(ReservationLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}',
      };
      var response = await Api.dio.get(
        '${Api.baseUrl}/getRouteReservation/$routeId',
        options: Options(headers: headers),
      );
      if (response.statusCode == 200) {

        reservationModel = ReservationModel.fromJson(response.data);
        emit(ReservationSuccess(reservationModel!));
      } else {

        emit(ReservationFailure(response.statusMessage ?? 'Unknown error'));
      }
    } catch (e) {

      emit(ReservationFailure(e.toString()));
    }
  }



  getTransporterReservationsByOrganizers()async{
    emit(TransporterReservationByOrganizersLoading());
    try{
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.get('${Api.baseUrl}/organizerBooking',options: Options(headers: headers));
      if (response.statusCode==200)
      {
        transporterReservationByOrganizerModel = ReservationByOrganizersModel.fromJson(response.data);
        emit(TransporterReservationByOrganizersSuccess(transporterReservationByOrganizerModel!));


      }
      else{
        emit(TransporterReservationByOrganizersFailure(response.statusMessage!));
      }
    }
    catch(e){  emit(TransporterReservationByOrganizersFailure(e.toString()));}
  }
}





