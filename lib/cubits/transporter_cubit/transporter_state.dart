part of 'transporter_cubit.dart';

@immutable
sealed class TransporterState {}

final class TransporterInitial extends TransporterState {}
final class TransporterReservationByOrganizersLoading extends TransporterState {}
final class TransporterReservationByOrganizersSuccess extends TransporterState {
  final ReservationByOrganizersModel transporterReservationByOrganizersModel;

  TransporterReservationByOrganizersSuccess(this.transporterReservationByOrganizersModel);
}
final class TransporterReservationByOrganizersFailure extends TransporterState {final String errMessage;

  TransporterReservationByOrganizersFailure(this.errMessage);}
final class GetRoutesSuccess extends TransporterState {
  final TransporterRoutesModel transporterRoutesModel;

  GetRoutesSuccess(this.transporterRoutesModel);
}
final class GetRoutesFailure extends TransporterState {
  final String errMessage;

  GetRoutesFailure(this.errMessage);
}
final class RouteCreateLoading extends TransporterState {}
final class RouteCreateSuccess extends TransporterState {}

final class RouteCreateFailure extends TransporterState {
  final String errMessage;

  RouteCreateFailure(this.errMessage);
}


final class GetRoutesLoading extends TransporterState {}
final class GetVehiclesLoading extends TransporterState {}
final class GetVehiclesSuccess extends TransporterState {
  final AvailableVehiclesModel availableVehiclesModel;

  GetVehiclesSuccess(this.availableVehiclesModel);
}
final class GetVehiclesFailure extends TransporterState {
  final String errMessage;

  GetVehiclesFailure(this.errMessage);
}

final class LocationPickSuccess extends TransporterState {}
final class LocationPickFailure extends TransporterState {}
final class LocationPickLoading extends TransporterState {}

final class EndLocationPickFailure extends TransporterState {}
final class EndLocationPickSuccess extends TransporterState {}
final class EndLocationPickLoading extends TransporterState {}
final class DateLoading extends TransporterState {}
final class DatePicked extends TransporterState {
  final DateTime date;

  DatePicked(this.date);
}
final class ReservationLoading extends TransporterState {}
final class ReservationSuccess extends TransporterState {
  final ReservationModel reservationModel;

  ReservationSuccess(this.reservationModel);
}
final class ReservationFailure extends TransporterState {
  final String errMessage;

  ReservationFailure(this.errMessage);
}