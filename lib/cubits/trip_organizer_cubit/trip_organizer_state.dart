part of 'trip_organizer_cubit.dart';

@immutable
sealed class TripOrganizerState {}

final class TripOrganizerInitial extends TripOrganizerState {}
final class RemoveTripLoading extends TripOrganizerState {}
final class RemoveTripSuccess extends TripOrganizerState {}
final class RemoveTripFailure extends TripOrganizerState {
  final String errMessage;

  RemoveTripFailure(this.errMessage);
}
final class IsLicenseChanged extends TripOrganizerState {}
final class OrganizedTripsLoadLoading extends TripOrganizerState {}
final class OrganizedTripsLoadSuccess extends TripOrganizerState {
  final OrganizedTripsModel organizedTripsModel;

  OrganizedTripsLoadSuccess(this.organizedTripsModel);
}
final class OrganizedTripsLoadFailure extends TripOrganizerState {
  final String errMessage;

  OrganizedTripsLoadFailure(this.errMessage);
}

final class GridHover extends TripOrganizerState {
  final int index;

  GridHover(this.index);
}

final class GridHoverEnd extends TripOrganizerState {}
final class DrawerItemPicked extends TripOrganizerState {}

final class CreateTripLoading extends TripOrganizerState {}
final class ReservationLoading extends TripOrganizerState {}

final class ReservationSuccess extends TripOrganizerState {
  final ReservationModel reservationModel;

  ReservationSuccess(this.reservationModel);
}
final class ReservationFailure extends TripOrganizerState {
  final String errMessage;

  ReservationFailure(this.errMessage);
}
final class CreateTripSuccess extends TripOrganizerState {}
final class CreateTripFailure extends TripOrganizerState {
  final String errMessage;

  CreateTripFailure(this.errMessage);
}
final class HotelsLoadLoading extends TripOrganizerState {}
final class HotelsLoadSuccess extends TripOrganizerState {
  final HotelsModel hotelsModel;

  HotelsLoadSuccess(this.hotelsModel);
}
final class HotelsLoadFailure extends TripOrganizerState {
  final String errMessage;

  HotelsLoadFailure(this.errMessage);
}
final class TouristAreaPicked extends TripOrganizerState {}
final class TripsFilteredLoading extends TripOrganizerState {}
final class TripsFilteredSuccess extends TripOrganizerState {
  final OrganizedTripsFilteredModel organizedTripsFilteredModel;

  TripsFilteredSuccess(this.organizedTripsFilteredModel);
}
final class TripsFilteredFailure extends TripOrganizerState {
  final String errMessage;

  TripsFilteredFailure(this.errMessage);
}
final class TouristAreaLoading extends TripOrganizerState {}
final class TouristAreaFailure extends TripOrganizerState {}
final class AreasLoadLoading extends TripOrganizerState {}
final class TransportLoadLoading extends TripOrganizerState {}
final class DiscoverAreaLoading extends TripOrganizerState {}
final class DiscoverAreaSuccess extends TripOrganizerState {
  final DiscoverAreasModel discoverAreasModel;

  DiscoverAreaSuccess(this.discoverAreasModel);
}
final class DiscoverAreaFailure extends TripOrganizerState {
  final String errMessage;

  DiscoverAreaFailure(this.errMessage);
}
final class TransportPickSuccess extends TripOrganizerState {}
final class TransportPickFailure extends TripOrganizerState {}
final class TransportLoadSuccess extends TripOrganizerState {
  final TransportModel transportModel;

  TransportLoadSuccess(this.transportModel);
}
final class TransportLoadFailure extends TripOrganizerState {
  final String errMessage;

  TransportLoadFailure(this.errMessage);
}
final class AreasLoadSuccess extends TripOrganizerState {
  final AreasModel areas;

  AreasLoadSuccess(this.areas);


}
final class AreasLoadFailure extends TripOrganizerState {
  final String? errMessage ;

  AreasLoadFailure(this.errMessage);
}
final class LocationPickLoading extends TripOrganizerState {}

final class LocationPickFailure extends TripOrganizerState {}
final class LocationPickSuccess extends TripOrganizerState {}
final class EndDateReset extends TripOrganizerState {}
final class RestaurantPickSuccess extends TripOrganizerState {}
final class RestaurantPickFailed extends TripOrganizerState {}
final class HotelPickFailed extends TripOrganizerState {}
final class HotelPickSuccess extends TripOrganizerState {}

final class DateLoading extends TripOrganizerState {}
final class RestaurantLoadLoading extends TripOrganizerState {}
final class RestaurantLoadFailure extends TripOrganizerState {
  final String errMessage;

  RestaurantLoadFailure(this.errMessage);
}
final class RestaurantLoadSuccess extends TripOrganizerState {
 final  RestaurantsModel restaurantsModel;

  RestaurantLoadSuccess(this.restaurantsModel);
}
final class StartDatePicked extends TripOrganizerState {
  final DateTime startDate;

  StartDatePicked(this.startDate);
}
final class EndDatePicked extends TripOrganizerState {

  final DateTime endDate;

  EndDatePicked(this.endDate);
}


