import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:travelia_dashboard/constants/cache_service.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/models/areas_model.dart';
import 'package:travelia_dashboard/models/discover_areas_model.dart';
import 'package:travelia_dashboard/models/financial_model.dart';
import 'package:travelia_dashboard/models/hotels_model.dart';
import 'package:travelia_dashboard/models/organized_trips_filtered_model.dart';
import 'package:travelia_dashboard/models/organized_trips_model.dart';
import 'package:travelia_dashboard/models/profile_model.dart';
import 'package:travelia_dashboard/models/reservation_model.dart';
import 'package:travelia_dashboard/models/restaurants_model.dart';
import 'package:travelia_dashboard/models/transports_model.dart';
import '../../constants/api.dart';
import '../../views/auth/login_screen.dart';
import '../../views/home/organizer_home_screen.dart';

part 'trip_organizer_state.dart';

class TripOrganizerCubit extends Cubit<TripOrganizerState> {
  TripOrganizerCubit() : super(TripOrganizerInitial());
  HotelsModel? hotelsModel;
  TransportModel? transportModel;
  OrganizedTripsModel? organizedTripsModel;
  OrganizedTripsFilteredModel? organizedTripsFilteredModel;
  ReservationModel? reservationModel;
  FinancialModel? financialModel;
  DateTime? startDate;
  AreasModel? areas;
  DiscoverAreasModel? discoverAreasModel;
  int selectedIndex = 0;
  RestaurantsModel? restaurantsModel;
  DateTime? endDate;
  TextEditingController startController = TextEditingController();
  TextEditingController endController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController capacityController = TextEditingController();
  TextEditingController startLocationController = TextEditingController();
  TextEditingController destinationController = TextEditingController();
  TextEditingController hotelController = TextEditingController();
  TextEditingController restaurantController = TextEditingController();
  TextEditingController transportController = TextEditingController();
  TextEditingController profileNameController = TextEditingController();
  TextEditingController profileEmailController = TextEditingController();
  TextEditingController profileCountryController = TextEditingController();

  int? daysBet;
  double? longitude;
  double? latitude;
  String? address;
  String? country;
  String? city;
  String? states;
  int? touristAreaID;
  int? restaurantID;
  int? hotelID;
  int? transporterID;
  final firstPageKey = GlobalKey<FormState>();
  final secPageKey = GlobalKey<FormState>();
  final supportKey = GlobalKey<FormState>();

  updateGrid(int index, bool isHovering) {
    if (isHovering == true) {
      emit(GridHover(index));
    }
    if (isHovering == false) {
      emit(GridHoverEnd());
    }
  }

  pickStartDate(BuildContext context) async {
    emit(DateLoading());
    DateTime initialDate = startDate ?? DateTime.now();
    DateTime firstDate = DateTime.now();
    DateTime lastDate =
        endDate?.subtract(const Duration(days: 1)) ?? DateTime(2030);

    final picker = await showDatePicker(
      helpText: 'Starting Day',
      context: context,
      initialDate: initialDate,
      firstDate: firstDate,
      lastDate: lastDate,
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(
              primary: MyColors.blue,
              onPrimary: MyColors.scaffoldColor,
              onSurface: MyColors.blue,
            ),
            dialogBackgroundColor: Colors.white,
          ),
          child: child!,
        );
      },
    );

    if (picker != null) {
      if (endDate != null && picker.isAfter(endDate!)) {
        // Reset endDate if new startDate is after the current endDate
        endDate = null;
        emit(EndDateReset());
      }
      startDate = picker;
      startController.text = DateFormat('dd/MM/yyyy').format(picker);

      emit(StartDatePicked(startDate!));
    }
  }

  pickEndDate(BuildContext context) async {
    emit(DateLoading());
    final picker = await showDatePicker(
      helpText: 'End Day',
      context: context,
      firstDate: (startDate ?? DateTime.now()).add(const Duration(days: 1)),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(
              primary: MyColors.blue,
              onPrimary: MyColors.scaffoldColor,
              onSurface: MyColors.blue,
            ),
            dialogBackgroundColor: Colors.white,
          ),
          child: child!,
        );
      },
    );
    if (picker != null && picker.isAfter(startDate!)) {
      endDate = picker;
      endController.text = DateFormat('dd/MM/yyyy').format(picker);
      daysBetween(startDate!, endDate!);
      emit(EndDatePicked(endDate!));
    }
  }

  int daysBetween(DateTime start, DateTime end) {
    return daysBet = end.difference(start).inDays;
  }

  updateDrawer() {
    emit(DrawerItemPicked());
  }

  locationPick() {
    emit(LocationPickLoading());
    if (city != null &&
        states != null &&
        longitude != null &&
        latitude != null &&
        country != null &&
        address != null) {
      startLocationController.text = '${country!} , ${states!} , ${city!}';
      emit(LocationPickSuccess());
    } else {
      emit(LocationPickFailure());
    }
  }

  getTouristAreas() async {
    emit(AreasLoadLoading());
    try {
      var response = await Api.dio.get(
        '${Api.baseUrl}/getTouristAreas',
        options: Options(
          headers: {
            'Authorization': 'Bearer ${CacheService.getData(key: 'token')}',
          },
        ),
      );
      if (response.statusCode == 200) {
        areas = AreasModel.fromJson(response.data);
        emit(AreasLoadSuccess(areas!));
      } else {
        emit(AreasLoadFailure(
            'Failed to load areas: ${response.statusMessage}'));
      }
    } on DioException catch (e) {
      if (e.response != null) {
        emit(AreasLoadFailure('DioException occurred: ${e.response?.data}'));
      } else {
        emit(AreasLoadFailure('DioException occurred: ${e.message}'));
      }
    } catch (e) {
      emit(AreasLoadFailure('An unexpected error occurred: ${e.toString()}'));
    }
  }

  touristAreaPicked() {
    emit(TouristAreaLoading());
    if (destinationController.text.isEmpty == false) {
      emit(TouristAreaPicked());
    } else {
      emit(TouristAreaFailure());
    }
  }

  restaurantPicked() {
    if (restaurantController.text.isEmpty == false) {
      emit(RestaurantPickSuccess());
    } else {
      emit(RestaurantPickFailed());
    }
  }

  hotelPicked() {
    if (hotelController.text.isEmpty == false) {
      emit(HotelPickSuccess());
    } else {
      emit(HotelPickFailed());
    }
  }

  transportPicked() {
    if (transportController.text.isEmpty == false) {
      emit(TransportPickSuccess());
    } else {
      emit(TransportPickFailure());
    }
  }

  getRestaurants() async {
    emit(RestaurantLoadLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.get('${Api.baseUrl}/getNearRestaurant/$touristAreaID',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        restaurantsModel = RestaurantsModel.fromJson(response.data);
        emit(RestaurantLoadSuccess(restaurantsModel!));
      } else {
        emit(RestaurantLoadFailure(response.statusMessage.toString()));
      }
    } catch (e) {
      debugPrint(e.toString());
      emit(RestaurantLoadFailure(e.toString()));
    }
  }

  getHotels() async {
    emit(HotelsLoadLoading());
    try {
      debugPrint('$touristAreaID just for testing the tourist area id');
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.get('${Api.baseUrl}/getNearHotel/$touristAreaID',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        hotelsModel = HotelsModel.fromJson(response.data);
        emit(HotelsLoadSuccess(hotelsModel!));
        debugPrint('Hotels loaded successfully');
      }
    } on DioException catch (e) {
      if (e.response != null) {
        emit(HotelsLoadFailure(('DioException occurred: ${e.response?.data}')));
        print(e.response?.statusCode);
      } else {
        emit(HotelsLoadFailure(('DioException occurred: ${e.message}')));
      }
    } catch (e) {
      emit(HotelsLoadFailure(e.toString()));
    }
  }

  getTransports() async {
    emit(TransportLoadLoading());

    try {
      var data = {
        "latitude": latitude.toString(),
        "longitude": longitude.toString(),
        "touristArea_id": touristAreaID.toString()
      };
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.post(data: data,'${Api.baseUrl}/getTransporters',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        transportModel = TransportModel.fromJson(response.data);
        emit(TransportLoadSuccess(transportModel!));
        debugPrint('Transport Get Successfully');
      }
    } on DioException catch (e) {
      if (e.response != null) {
        emit(TransportLoadFailure(
            ('DioException occurred: ${e.response?.data}')));
      } else {
        emit(TransportLoadFailure(('DioException occurred: ${e.message}')));
      }
    } catch (e) {
      emit(TransportLoadFailure(e.toString()));
    }
  }

  createTrip(BuildContext context) async {
    emit(CreateTripLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var formData = FormData();
      formData.fields.addAll([
        MapEntry(
          'cost',
          priceController.text,
        ),
        MapEntry('strDate', startDate.toString()),
        MapEntry('totalCapacity', capacityController.text),
        MapEntry('latitude', latitude.toString()),
        MapEntry('longitude', longitude.toString()),
        MapEntry('address', address!),
        MapEntry('state', states!),
        MapEntry('country', country!),
        MapEntry('city', city!),
        MapEntry('touristArea_id', touristAreaID.toString()),
        MapEntry('hotel_id', hotelID.toString()),
        MapEntry('transporter_id', transporterID.toString()),
        MapEntry('restaurant_id', restaurantID.toString()),
        MapEntry('endDate', endDate.toString()),
      ]);

      var response = await Api.dio.post('${Api.baseUrl}/createTrip',
          data: formData, options: Options(headers: headers));
      if (response.statusCode == 200) {
        debugPrint('The Trip Created Successfully');
        emit(CreateTripSuccess());
        restaurantController.clear();
        hotelController.clear();
        transportController.clear();
        priceController.clear();
        startLocationController.clear();
        capacityController.clear();
        startController.clear();
        endController.clear();
        destinationController.clear();
        showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  content: SizedBox(
                    height: MediaQuery.of(context).size.height / 6,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const Text(
                            "Trip Created Successfully , Enjoy it :) ",
                            style: TextStyle(fontSize: 20),
                          ),
                          TextButton(
                              onPressed: () {
                                Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        const OrganizerHomeScreen(),
                                  ),
                                  (route) => false,
                                );
                                daysBet = 0;
                              },
                              child: const Text(
                                'Go To Home Page',
                                style: TextStyle(
                                    fontSize: 15, color: MyColors.blue),
                              ))
                        ],
                      ),
                    ),
                  ),
                ));
      } else {
        emit(CreateTripFailure(response.statusMessage.toString()));

      }
    } catch (e) {
      emit(CreateTripFailure(e.toString()));
    }
  }





  getOrganizedTrips() async {
    emit(OrganizedTripsLoadLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.get('${Api.baseUrl}/getOrganizerTrips',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        organizedTripsModel = OrganizedTripsModel.fromJson(response.data);
        debugPrint('Organized Trips Successfully ${response.statusCode}');
        emit(OrganizedTripsLoadSuccess(organizedTripsModel!));
      }
    } on DioException catch (e) {
      if (e.response != null) {
        emit(OrganizedTripsLoadFailure(
            ('DioException occurred: ${e.response?.data}')));

      }
    } catch (e) {
      emit(OrganizedTripsLoadFailure(
          ('DioException occurred: ${e.toString()}')));
    }
  }


  getDiscoverTouristAreas() async {
    emit(DiscoverAreaLoading());
    try {
      var response = await Api.dio.get(
        '${Api.baseUrl}/getAreas',
        options: Options(
          headers: {
            'Authorization': 'Bearer ${CacheService.getData(key: 'token')}',
          },
        ),
      );
      if (response.statusCode == 200) {
        discoverAreasModel = DiscoverAreasModel.fromJson(response.data);
        emit(DiscoverAreaSuccess(discoverAreasModel!));
      } else {
        emit(DiscoverAreaFailure(
            'Failed to load areas: ${response.statusMessage}'));
      }
    } on DioException catch (e) {
      if (e.response != null) {
        emit(DiscoverAreaFailure('DioException occurred: ${e.response?.data}'));
      } else {
        emit(DiscoverAreaFailure('DioException occurred: ${e.message}'));
      }
    } catch (e) {
      emit(
          DiscoverAreaFailure('An unexpected error occurred: ${e.toString()}'));
    }
  }

  getTripsFiltered() async {
    emit(TripsFilteredLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.get('${Api.baseUrl}/organizerTrips?',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        organizedTripsFilteredModel =
            OrganizedTripsFilteredModel.fromJson(response.data);
        emit(TripsFilteredSuccess(organizedTripsFilteredModel!));
      } else {
        emit(TripsFilteredFailure(response.statusMessage!));
      }
    } catch (e) {
      emit(TripsFilteredFailure(e.toString()));
    }
  }

  getReservation(int id) async {
    emit(ReservationLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.get('${Api.baseUrl}/getReservation/$id',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        reservationModel = ReservationModel.fromJson(response.data);
        emit(ReservationSuccess(reservationModel!));
      } else {
        emit(ReservationFailure(response.statusMessage!));
      }
    } catch (e) {
      emit(ReservationFailure(e.toString()));
    }
  }
  
  removeTrip(int id)async{
    emit(RemoveTripLoading());
    try{
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.delete('${Api.baseUrl}/deleteTrip/$id',options: Options(headers: headers));
      if(response.statusCode==200)
        {
          emit(RemoveTripSuccess());
        } else {emit(RemoveTripFailure(response.statusMessage!));}
      
      
    }
        catch(e)
    {
      emit(RemoveTripFailure(e.toString()));

    }
  }



}
