part of 'admin_cubit.dart';

@immutable
sealed class AdminState {}

final class AdminInitial extends AdminState {}
final class FinancialHandlingLoading extends AdminState {}
final class FinancialHandlingSuccess extends AdminState {}
final class FinancialHandlingFailure extends AdminState {
  final String errMessage;

  FinancialHandlingFailure(this.errMessage);
}

final class FinancialRequestLoading extends AdminState {}
final class FinancialRequestSuccess extends AdminState {
  final RequestModel requestModel;

  FinancialRequestSuccess(this.requestModel);

}
final class FinancialRequestFailure extends AdminState {
  final String errMessage;

  FinancialRequestFailure(this.errMessage);
}
final class GetMessagesLoading extends AdminState {}
final class GetMessagesSuccess extends AdminState {
  final MessagesModel messagesModel;

  GetMessagesSuccess(this.messagesModel);
}
final class GetMessagesFailure extends AdminState {
  final String errMessage = "Error , Try Again";
}
final class MapLoadingAdmin extends AdminState {}
final class MapFailureAdmin extends AdminState {
  final String errMessage;

  MapFailureAdmin(this.errMessage);

}
final class MapSuccessAdmin extends AdminState {}
final class NotificationSuccess extends AdminState {}
final class NotificationLoading extends AdminState {}
final class NotificationFailure extends AdminState {
  final String errMessage = 'Error Occurred , Try Again';
}
final class ImageLoadingAdmin extends AdminState {}
final class ImageWrongExtAdmin extends AdminState {
  final String imgErrWrongExt='Only Images are Valid';
}
final class ImagePickedAdmin extends AdminState {}
final class ImageRemovedAdmin extends AdminState {}
final class ImagePickFailAdmin extends AdminState {
  final String imgErrMessage='Please Upload Image Again';

}
final class GetAccountsLoading extends AdminState {}
final class GetAccountsSuccess extends AdminState {
  final AccountsConfirmModel accountsConfirmModel;

  GetAccountsSuccess(this.accountsConfirmModel);
}
final class GetAccountsFailure extends AdminState {
  final String errMessage;

  GetAccountsFailure(this.errMessage);
}

final class AreaPushSuccessAdmin extends AdminState {}
final class AreaPushLoadingAdmin extends AdminState {}
final class AreaPushFailureAdmin extends AdminState {
  final String errMessage;

  AreaPushFailureAdmin(this.errMessage);
}
final class AccountConfirmLoading extends AdminState {}
final class AccountConfirmSuccess extends AdminState {}
final class AccountConfirmFailure extends AdminState {
  final String errMessage;

  AccountConfirmFailure(this.errMessage);
}


