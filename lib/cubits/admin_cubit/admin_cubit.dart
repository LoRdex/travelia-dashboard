import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:travelia_dashboard/constants/api.dart';
import 'package:travelia_dashboard/models/accounts_confirm_model.dart';
import 'package:travelia_dashboard/models/messages_model.dart';
import 'package:universal_html/html.dart' as html;
import 'package:flutter/material.dart';

import '../../constants/cache_service.dart';
import '../../models/money_request_model.dart';

part 'admin_state.dart';

class AdminCubit extends Cubit<AdminState> {
  AdminCubit() : super(AdminInitial());
  AccountsConfirmModel? accountsConfirmModel;
  html.File? imageFile;
  TextEditingController titleController = TextEditingController();
  TextEditingController messageController = TextEditingController();
  MessagesModel? messagesModel;
  RequestModel? requestModel;

  TextEditingController descriptionController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController typeController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  final createAreaKey = GlobalKey<FormState>();
  final sendMessageKey = GlobalKey<FormState>();
  double? latitude;
  double? longitude;
  String? address;
  String? country;
  String? city;
  String? states;

  getRoleName(int roleId) {
    if (roleId == 2) {
      return 'Trip Organizer';
    }
    if (roleId == 3) {
      return 'Hotel Manger';
    }
    if (roleId == 4) {
      return 'Restaurant Manger';
    }
    if (roleId == 5) {
      return 'Transport Company';
    } else {
      return 'Unknown';
    }
  }

  getAccountsForConfirmation() async {
    emit(GetAccountsLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}',
      };
      var response = await Api.dio.get('${Api.baseUrl}/getRequierments',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        accountsConfirmModel = AccountsConfirmModel.fromJson(response.data);
        emit(GetAccountsSuccess(accountsConfirmModel!));
      }
    } catch (e) {
      emit(GetAccountsFailure(e.toString()));
    }
  }

  handleAccounts(int id, String status) async {
    emit(AccountConfirmLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}',
      };
      var data = {"status": status};
      var response = await Api.dio.post(
          '${Api.baseUrl}/handlingRequierment/$id',
          options: Options(headers: headers),
          data: data);
      if (response.statusCode == 200) {
        emit(AccountConfirmSuccess());
      }
    } catch (e) {
      emit(AccountConfirmFailure(e.toString()));
    }
  }

  pickImage() async {
    emit(ImageLoadingAdmin());

    final picker = html.InputElement(type: 'file')..accept = 'image/*';
    picker.click();

    picker.onChange.listen((event) {
      final file = picker.files!.first;
      final reader = html.FileReader();

      // Check if the file is an image
      if (!file.type.startsWith('image/')) {
        emit(ImageWrongExtAdmin());
        return;
      }

      reader.readAsDataUrl(file);
      reader.onLoadEnd.listen((event) {
        imageFile = file;
        emit(ImagePickedAdmin());
      });

      reader.onError.listen((event) {
        emit(ImagePickFailAdmin());
      });
    });
  }

  imageRemoved() {
    imageFile = null;
    emit(ImageRemovedAdmin());
  }

  void locationPicked() {
    emit(MapLoadingAdmin());
    if (longitude != null &&
        latitude != null &&
        address != null &&
        country != null &&
        states != null &&
        city != null) {
      emit(MapSuccessAdmin());
    } else {
      emit(MapFailureAdmin(' Error Occurred Try to Pick Location Again '));
    }
  }

  pushArea() async {
    if (imageFile == null) {
      emit(ImagePickFailAdmin());
      return;
    }

    emit(AreaPushLoadingAdmin());

    final reader = html.FileReader();
    reader.readAsArrayBuffer(imageFile!);
    await reader.onLoad.first;
    final Uint8List imageBytes = reader.result as Uint8List;

    try {
      FormData formData = FormData.fromMap({
        'name': nameController.text,
        'description': descriptionController.text,
        'latitude': latitude.toString(),
        'longitude': longitude.toString(),
        'address': address,
        'country': country,
        'state': states,
        'city': city,
        'type': typeController.text,
        'img': MultipartFile.fromBytes(
          imageBytes as List<int>,
          filename: imageFile!.name,
        ),
      });

      Response response = await Api.dio.post(
        '${Api.baseUrl}/createArea',
        data: formData,
        options: Options(
          headers: {
            'Authorization': 'Bearer ${CacheService.getData(key: 'token')}',
          },
        ),
      );

      if (response.statusCode == 200) {
        emit(AreaPushSuccessAdmin());
        locationController.clear();
        nameController.clear();
        descriptionController.clear();
        typeController.clear();
        imageFile = null;
        country = null;
      } else {
        emit(AreaPushFailureAdmin("Failed to upload data"));
      }
    } catch (e) {
      emit(AreaPushFailureAdmin("An error occurred while uploading data"));
    }
  }

  sendNotification() async {
    emit(NotificationLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}',
      };
      var data = {
        "title": titleController.text,
        "body": messageController.text
      };
      var response = await Api.dio.post('${Api.baseUrl}/sendMessage',
          data: data, options: Options(headers: headers));
      if (response.statusCode == 200) {
        emit(NotificationSuccess());
      } else {
        emit(NotificationFailure());
      }
    } catch (e) {}
  }

  getMessages() async {
    emit(GetMessagesLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}',
      };
      var response = await Api.dio
          .get('${Api.baseUrl}/getContact', options: Options(headers: headers));
      if (response.statusCode == 200) {
        messagesModel = MessagesModel.fromJson(response.data);
        emit(GetMessagesSuccess(messagesModel!));
      } else {
        emit(GetMessagesFailure());
      }
    } catch (e) {
      emit(GetMessagesFailure());
    }
  }

  getFinancialRequests() async {
    emit(FinancialRequestLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}',
      };
      var response = await Api.dio.get('${Api.baseUrl}/getTransferRequests',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        requestModel = RequestModel.fromJson(response.data);
        emit(FinancialRequestSuccess(requestModel!));
        debugPrint('jbna al financial w jena ');
      } else {
        emit(FinancialRequestFailure(response.statusMessage!));
      }
    } catch (e) {
      emit(FinancialRequestFailure(e.toString()));
    }
  }

  handleTransactions(int id, String status) async {
    emit(FinancialHandlingLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}',
      };
      var data = {"status": status};
      var response = await Api.dio.post(
          '${Api.baseUrl}/handlingTransferRequests/$id',
          options: Options(headers: headers),
          data: data);
      if (response.statusCode == 200) {
        debugPrint('handled');
        emit(FinancialHandlingSuccess());
        debugPrint(response.data);
      }
    } catch (e) {
      emit(FinancialHandlingFailure(e.toString()));
    }
  }

}
