import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:travelia_dashboard/constants/api.dart';
import 'package:travelia_dashboard/models/available_rooms.dart';
import 'package:travelia_dashboard/models/hotel_reservation_model.dart';
import 'package:travelia_dashboard/models/reservations_by_organizers.dart';
import 'package:travelia_dashboard/views/hotel/available_rooms.dart';

import '../../constants/cache_service.dart';
import '../../constants/my_colors.dart';

part 'hotel_state.dart';

class HotelCubit extends Cubit<HotelState> {
  HotelCubit() : super(HotelInitial());
  HotelReservationModel? hotelReservationModel;
  ReservationByOrganizersModel? hotelReservationByOrganizersModel;
  AvailableRoomsModel? availableRoomsModel;
  TextEditingController dateController = TextEditingController();
  TextEditingController endController = TextEditingController();
  DateTime? strDate;
  DateTime? endDate;

  getHotelReservations() async {
    emit(HotelReservationLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.get('${Api.baseUrl}/getHotelReservation',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        hotelReservationModel = HotelReservationModel.fromJson(response.data);
        emit(HotelReservationSuccess(hotelReservationModel!));
        debugPrint(' good one i got all hotttel');

      }
      else {
        emit(HotelReservationFailure(response.statusMessage!));
      }
    }
    catch (e) {
      emit(HotelReservationFailure(e.toString()));
    }
  }

  getHotelReservationsByOrganizers() async {
    emit(HotelReservationByOrganizersLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.get('${Api.baseUrl}/organizerBooking',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        hotelReservationByOrganizersModel =
            ReservationByOrganizersModel.fromJson(response.data);
        emit(HotelReservationByOrganizersSuccess(
            hotelReservationByOrganizersModel!));
      }
      else {
        emit(HotelReservationByOrganizersFailure(response.statusMessage!));
      }
    }
    catch (e) {
      emit(HotelReservationByOrganizersFailure(e.toString()));
    }
  }

  pickStartDate(BuildContext context) async {
    emit(DateLoading());
    DateTime initialDate = strDate ?? DateTime.now();
    DateTime firstDate = DateTime.now();
    DateTime lastDate =
    DateTime(2025);

    final picker = await showDatePicker(
      helpText: 'Starting Day',
      context: context,
      initialDate: initialDate,
      firstDate: firstDate,
      lastDate: lastDate,
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(
              primary: MyColors.blue,
              onPrimary: MyColors.scaffoldColor,
              onSurface: MyColors.blue,
            ),
            dialogBackgroundColor: Colors.white,
          ),
          child: child!,
        );
      },
    );

    if (picker != null) {
      strDate = picker;
      dateController.text = DateFormat('yyyy-MM-dd').format(picker);
      emit(DatePicked(strDate!));

    }
  }
  pickEndDate(BuildContext context) async {
    emit(DateLoading());
    final picker = await showDatePicker(
      helpText: 'End Day',
      context: context,
      firstDate: (strDate ?? DateTime.now()).add(const Duration(days: 1)),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(
              primary: MyColors.blue,
              onPrimary: MyColors.scaffoldColor,
              onSurface: MyColors.blue,
            ),
            dialogBackgroundColor: Colors.white,
          ),
          child: child!,
        );
      },
    );
    if (picker != null && picker.isAfter(strDate!)) {
      endDate = picker;
      endController.text = DateFormat('yyyy-MM-dd').format(picker);

      emit(EndDatePicked(endDate!));
      if(strDate!=null && endDate !=null)
        {
          getAvailableRooms();
        }
    }
  }


  getAvailableRooms() async {

    emit(GetRoomsLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}',
        'Content-Type': 'application/json',  // Set content type
      };
      var data = {
        "strDate": dateController.text,
        "endDate": endController.text
      };
      var response = await Api.dio.post(
        '${Api.baseUrl}/getAvailableRooms/${CacheService.getData(key: 'fid')}',
        data: data,
        options: Options(headers: headers),
      );

      if (response.statusCode == 200) {

        availableRoomsModel = AvailableRoomsModel.fromJson(response.data);
        emit(GetRoomsSuccess(availableRoomsModel!));
        debugPrint('getting rooms done');
      } else {

        emit(GetRoomsFailure(response.statusMessage!));
      }
    } catch (e) {
      emit(GetRoomsFailure(e.toString()));
    }
  }



}
