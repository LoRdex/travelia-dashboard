part of 'hotel_cubit.dart';

@immutable
sealed class HotelState {}

final class HotelInitial extends HotelState {}
final class GetRoomsLoading extends HotelState {}
final class GetRoomsSuccess extends HotelState {
  final AvailableRoomsModel availableRoomsModel;

  GetRoomsSuccess(this.availableRoomsModel);
}
final class GetRoomsFailure extends HotelState {
  final String errMessage;

  GetRoomsFailure(this.errMessage);

}
final class DateLoading extends HotelState {}
final class EndDatePicked extends HotelState {
  final DateTime endDate;

  EndDatePicked(this.endDate);
}
final class DatePicked extends HotelState {
  final DateTime date;

  DatePicked(this.date);
}
final class HotelReservationByOrganizersSuccess extends HotelState {
  final ReservationByOrganizersModel hotelReservationByOrganizersModel;

  HotelReservationByOrganizersSuccess(this.hotelReservationByOrganizersModel);
}
final class HotelReservationByOrganizersLoading extends HotelState {}
final class HotelReservationByOrganizersFailure extends HotelState {
  final String errMessage;

  HotelReservationByOrganizersFailure(this.errMessage);
}
final class HotelReservationSuccess extends HotelState {
 final HotelReservationModel hotelReservationModel;

  HotelReservationSuccess(this.hotelReservationModel);
}
final class HotelReservationLoading extends HotelState {}
final class HotelReservationFailure extends HotelState {
  final String errMessage;

  HotelReservationFailure(this.errMessage);
}
