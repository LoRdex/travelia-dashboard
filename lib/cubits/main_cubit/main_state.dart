part of 'main_cubit.dart';

@immutable
sealed class MainState {}

final class MainInitial extends MainState {}

final class ProfileLoading extends MainState {}

final class SupportMessageLoading extends MainState {}

final class LogOutSuccess extends MainState {}

final class LogOutFailed extends MainState {
  String errMessage = 'Try again please ';
}

final class SupportMessageSuccess extends MainState {}

final class SupportMessageFailure extends MainState {
  final String errMessage;

  SupportMessageFailure(this.errMessage);
}

final class ProfileFailure extends MainState {
  final String errMessage;

  ProfileFailure(this.errMessage);
}

final class ProfileSuccess extends MainState {
  final ProfileModel profileModel;

  ProfileSuccess(this.profileModel);
}

final class IsLicenseChanged extends MainState {}

final class FinancialLoading extends MainState {}

final class FinancialSuccess extends MainState {
  final FinancialModel financialModel;

  FinancialSuccess(this.financialModel);
}

final class FinancialFailure extends MainState {
  final String errMessage;

  FinancialFailure(this.errMessage);
}
