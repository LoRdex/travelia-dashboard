import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:travelia_dashboard/components/drawers/custom_admin_drawer.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_hotel.dart';
import 'package:travelia_dashboard/components/drawers/custom_drawer_restaurant.dart';
import 'package:travelia_dashboard/components/drawers/custom_transporter_drawer.dart';
import 'package:travelia_dashboard/constants/api.dart';

import '../../components/drawers/custom_drawer_organizer.dart';
import '../../constants/cache_service.dart';
import '../../models/financial_model.dart';
import '../../models/profile_model.dart';
import '../../views/auth/login_screen.dart';

part 'main_state.dart';

class MainCubit extends Cubit<MainState> {
  MainCubit() : super(MainInitial());
  final supportKey = GlobalKey<FormState>();

  bool isLicense = false;
  FinancialModel? financialModel;
  ProfileModel? profileModel;
  TextEditingController profileNameController = TextEditingController();
  TextEditingController profileEmailController = TextEditingController();
  TextEditingController profileCountryController = TextEditingController();
  TextEditingController titleController = TextEditingController();
  TextEditingController messageController = TextEditingController();
  updateIsLicense() {
    isLicense = !isLicense;
    emit(IsLicenseChanged());
  }


  getProfile() async {
    debugPrint('coming from facility');
    emit(ProfileLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };

      var response = await Api.dio.get('${Api.baseUrl}/getProfile',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        profileModel = ProfileModel.fromJson(response.data);
        profileNameController.text = profileModel!.name!;
        profileEmailController.text = profileModel!.email!;
        profileCountryController.text = profileModel!.country!;

        CacheService.saveData(key: 'type', value: profileModel!.type);
        CacheService.saveData(key: 'fid', value: profileModel!.id);

        emit(ProfileSuccess(profileModel!));
      }
    } on DioException catch (e) {
      if (e.response != null) {
        emit(ProfileFailure(
            ('DioException occurred: ${e.response?.data}')));
      }
    } catch (e) {
      emit(ProfileFailure(
          ('DioException occurred: ${e.toString()}')));
    }
  }
  getFinancialLog() async {
    emit(FinancialLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.get('${Api.baseUrl}/getFinances?',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        financialModel = FinancialModel.fromJson(response.data);
        emit(FinancialSuccess(financialModel!));
      } else {
        emit(FinancialFailure(response.statusMessage!));
      }
    } catch (e) {
      emit(FinancialFailure(e.toString()));
    }
  }

  sendSupportMessage({required String title, required String message}) async {
    emit(SupportMessageLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };

      var data = {'title': title, 'msg': message};
      var response = await Api.dio.post('${Api.baseUrl}/makeContact',
          data: data, options: Options(headers: headers));
      if (response.statusCode == 200) {
        emit(SupportMessageSuccess());
      } else {
        emit(SupportMessageFailure(response.statusMessage!));
      }
    } catch (e) {
      emit(SupportMessageFailure(e.toString()));
    }
  }
  logout(BuildContext context) async {
    try {
      await CacheService.removeData(key: 'token');
      profileModel = null;
      emit(LogOutSuccess());
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => const LoginScreen()),
              (route) => false);
    } on Exception catch (e) {
      emit(LogOutFailed());
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Failed to log out. Please try again.')),
      );
    }
  }

  Widget getDrawer() {
    switch (CacheService.getData(key: 'role_id')) {
      case 1:
        return const CustomAdminDrawer();
      case 2:
        return const CustomDrawerOrganizer();
      case 4:
        return const CustomDrawerRestaurant();
      case 3:
        return const CustomDrawerHotel();
      case 5:
        return const CustomDrawerTransporter();
      default:
        return const CustomDrawerOrganizer(); // Default drawer
    }
  }


}
