part of 'restaurant_cubit.dart';

@immutable
sealed class RestaurantState {}

final class RestaurantInitial extends RestaurantState {}

final class RestaurantReservationByOrganizersLoading extends RestaurantState {}

final class RestaurantReservationByOrganizersSuccess extends RestaurantState {
  final ReservationByOrganizersModel restaurantReservationByOrganizersModel;

  RestaurantReservationByOrganizersSuccess(this.restaurantReservationByOrganizersModel);
}

final class RestaurantReservationByOrganizersFailure extends RestaurantState {
   final String errMessage;

  RestaurantReservationByOrganizersFailure(this.errMessage);
}

final class ReservationLoading extends RestaurantState {}

final class ReservationSuccessful extends RestaurantState {
  final RestaurantReservationModel restaurantReservationModel;

  ReservationSuccessful(this.restaurantReservationModel);
}

final class ReservationFailure extends RestaurantState {
  final String errMessage;

  ReservationFailure(this.errMessage);
}
