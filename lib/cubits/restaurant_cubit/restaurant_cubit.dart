import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:travelia_dashboard/constants/cache_service.dart';
import 'package:travelia_dashboard/models/RestaurantReservationModel.dart';
import 'package:travelia_dashboard/models/reservations_by_organizers.dart';

import '../../constants/api.dart';

part 'restaurant_state.dart';

class RestaurantCubit extends Cubit<RestaurantState> {
  RestaurantCubit() : super(RestaurantInitial());
  RestaurantReservationModel? restaurantReservationModel;
  ReservationByOrganizersModel? restaurantReservationByOrganizersModel;

  getRestaurantReservations() async {
    emit(ReservationLoading());
    try {
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.get('${Api.baseUrl}/getRestaurantReservation',
          options: Options(headers: headers));
      if (response.statusCode == 200) {
        restaurantReservationModel =
            RestaurantReservationModel.fromJson(response.data);
        emit(ReservationSuccessful(restaurantReservationModel!));
        debugPrint(' good one i got all');
      } else {
        emit(ReservationFailure(response.statusMessage!));
      }
    } catch (e) {
      emit(ReservationFailure(e.toString()));

    }
  }


  getRestaurantReservationsByOrganizers()async{
    emit(RestaurantReservationByOrganizersLoading());
    try{
      var headers = {
        'Authorization': 'Bearer ${CacheService.getData(key: 'token')}'
      };
      var response = await Api.dio.get('${Api.baseUrl}/organizerBooking',options: Options(headers: headers));
      if (response.statusCode==200)
      {
        restaurantReservationByOrganizersModel = ReservationByOrganizersModel.fromJson(response.data);
        emit(RestaurantReservationByOrganizersSuccess(restaurantReservationByOrganizersModel!));
        debugPrint(' good one i got all ressssssst by organizers');
      }
      else{
        emit(RestaurantReservationByOrganizersFailure(response.statusMessage!));
      }
    }
    catch(e){ emit(RestaurantReservationByOrganizersFailure(e.toString()));}
  }
}

