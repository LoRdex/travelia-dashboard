class AreasModel {
  List<TouristAreas>? touristAreas;

  AreasModel({this.touristAreas});

  factory AreasModel.fromJson(Map<String, dynamic> json) {
    return AreasModel(
      touristAreas: json['touristAreas'] != null
          ? List<TouristAreas>.from(
          json['touristAreas'].map((x) => TouristAreas.fromJson(x)))
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    if (touristAreas != null) {
      data['touristAreas'] = touristAreas!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TouristAreas {
  int? id;
  String? name;
  String? type;
  String? img;
  String? country;

  TouristAreas({this.id, this.name, this.type, this.img, this.country});

  factory TouristAreas.fromJson(Map<String, dynamic> json) {
    return TouristAreas(
      id: json['id'],
      name: json['name'],
      type: json['type'],
      img: json['img'],
      country: json['country'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['name'] = name;
    data['type'] = type;
    data['img'] = img;
    data['country'] = country;
    return data;
  }
}
