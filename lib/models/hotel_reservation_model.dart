class HotelReservationModel {
  List<Reservations>? reservations;

  HotelReservationModel({this.reservations});

  HotelReservationModel.fromJson(Map<String, dynamic> json) {
    if (json['reservations'] != null) {
      reservations = <Reservations>[];
      json['reservations'].forEach((v) {
        reservations!.add(new Reservations.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.reservations != null) {
      data['reservations'] = this.reservations!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Reservations {
  int? id;
  String? room;
  dynamic cost;
  String? strDate;
  String? endDate;
  int? daysNum;
  String? name;
  String? email;
  String? phone;
  int? age;
  String? address;
  String? photo;

  Reservations(
      {this.id,
        this.room,
        this.cost,
        this.strDate,
        this.endDate,
        this.daysNum,
        this.name,
        this.email,
        this.phone,
        this.age,
        this.address,
        this.photo});

  Reservations.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    room = json['room'];
    cost = json['cost'];
    strDate = json['strDate'];
    endDate = json['endDate'];
    daysNum = json['daysNum'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    age = json['age'];
    address = json['address'];
    photo = json['photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['room'] = this.room;
    data['cost'] = this.cost;
    data['strDate'] = this.strDate;
    data['endDate'] = this.endDate;
    data['daysNum'] = this.daysNum;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['age'] = this.age;
    data['address'] = this.address;
    data['photo'] = this.photo;
    return data;
  }
}
