class ReservationModel {
  List<Reservations>? reservations;

  ReservationModel({this.reservations});

  ReservationModel.fromJson(Map<String, dynamic> json) {
    if (json['Reservations'] != null) {
      reservations = <Reservations>[];
      json['Reservations'].forEach((v) {
        reservations!.add(Reservations.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (reservations != null) {
      data['Reservations'] = reservations!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Reservations {
  int? id;
  String? name;
  String? email;
  String? phone;
  int? age;
  String? address;
  String? photo;
  int? placeNum;
  dynamic cost;

  Reservations(
      {this.id,
        this.name,
        this.email,
        this.phone,
        this.age,
        this.address,
        this.photo,
        this.placeNum,
        this.cost});

  Reservations.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    age = json['age'];
    address = json['address'];
    photo = json['photo'];
    placeNum = json['placeNum'];
    cost = json['cost'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['age'] = age;
    data['address'] = address;
    data['photo'] = photo;
    data['placeNum'] = placeNum;
    data['cost'] = cost;
    return data;
  }
}
