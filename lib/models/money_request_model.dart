class RequestModel {
  List<Requierments>? requierments;

  RequestModel({this.requierments});

  RequestModel.fromJson(Map<String, dynamic> json) {
    if (json['requierments'] != null) {
      requierments = <Requierments>[];
      json['requierments'].forEach((v) {
        requierments!.add(new Requierments.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.requierments != null) {
      data['requierments'] = this.requierments!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Requierments {
  int? id;
  int? userId;
  String? note;
  int? amount;
  String? status;
  User? user;

  Requierments(
      {this.id, this.userId, this.note, this.amount, this.status, this.user});

  Requierments.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    note = json['note'];
    amount = json['amount'];
    status = json['status'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['note'] = this.note;
    data['amount'] = this.amount;
    data['status'] = this.status;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class User {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? phone;
  int? age;
  String? address;
  String? photo;
  String? passport;
  int? roleId;
  dynamic wallet;
  String? type;
  String? confirmation;
  Null? emailVerifiedAt;

  User(
      {this.id,
        this.firstName,
        this.lastName,
        this.email,
        this.phone,
        this.age,
        this.address,
        this.photo,
        this.passport,
        this.roleId,
        this.wallet,
        this.type,
        this.confirmation,
        this.emailVerifiedAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    email = json['email'];
    phone = json['phone'];
    age = json['age'];
    address = json['address'];
    photo = json['photo'];
    passport = json['passport'];
    roleId = json['role_id'];
    wallet = json['wallet'];
    type = json['type'];
    confirmation = json['confirmation'];
    emailVerifiedAt = json['email_verified_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['age'] = this.age;
    data['address'] = this.address;
    data['photo'] = this.photo;
    data['passport'] = this.passport;
    data['role_id'] = this.roleId;
    data['wallet'] = this.wallet;
    data['type'] = this.type;
    data['confirmation'] = this.confirmation;
    data['email_verified_at'] = this.emailVerifiedAt;
    return data;
  }
}
