class ReservationByOrganizersModel {
  List<Trips>? trips;

  ReservationByOrganizersModel({this.trips});

  ReservationByOrganizersModel.fromJson(Map<String, dynamic> json) {
    if (json['trips'] != null) {
      trips = <Trips>[];
      json['trips'].forEach((v) {
        trips!.add(new Trips.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.trips != null) {
      data['trips'] = this.trips!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Trips {
  String? organizer;
  String? organizerImg;
  String? strDate;
  String? endDate;
  String? capacity;
  String? touristArea;
  String? touristAreaImg;

  Trips(
      {this.organizer,
        this.organizerImg,
        this.strDate,
        this.endDate,
        this.capacity,
        this.touristArea,
        this.touristAreaImg});

  Trips.fromJson(Map<String, dynamic> json) {
    organizer = json['organizer'];
    organizerImg = json['organizerImg'];
    strDate = json['strDate'];
    endDate = json['endDate'];
    capacity = json['capacity'];
    touristArea = json['touristArea'];
    touristAreaImg = json['touristAreaImg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['organizer'] = this.organizer;
    data['organizerImg'] = this.organizerImg;
    data['strDate'] = this.strDate;
    data['endDate'] = this.endDate;
    data['capacity'] = this.capacity;
    data['touristArea'] = this.touristArea;
    data['touristAreaImg'] = this.touristAreaImg;
    return data;
  }
}
