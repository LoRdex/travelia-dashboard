class DiscoverAreasModel {
  List<Areas>? areas;

  DiscoverAreasModel({this.areas});

  DiscoverAreasModel.fromJson(Map<String, dynamic> json) {
    if (json['Areas'] != null) {
      areas = <Areas>[];
      json['Areas'].forEach((v) {
        areas!.add(Areas.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (areas != null) {
      data['Areas'] = areas!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Areas {
  int? id;
  String? name;
  String? description;
  String? img;
  String? type;
  int? locationId;
  Location? location;

  Areas(
      {this.id,
        this.name,
        this.description,
        this.img,
        this.type,
        this.locationId,
        this.location});

  Areas.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    img = json['img'];
    type = json['type'];
    locationId = json['location_id'];
    location = json['location'] != null
        ? Location.fromJson(json['location'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['name'] = name;
    data['description'] = description;
    data['img'] = img;
    data['type'] = type;
    data['location_id'] = locationId;
    if (location != null) {
      data['location'] = location!.toJson();
    }
    return data;
  }
}

class Location {
  int? id;
  String? latitude;
  String? longitude;
  String? address;
  String? country;
  String? state;
  String? city;

  Location(
      {this.id,
        this.latitude,
        this.longitude,
        this.address,
        this.country,
        this.state,
        this.city});

  Location.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    address = json['address'];
    country = json['country'];
    state = json['state'];
    city = json['city'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    data['address'] = address;
    data['country'] = country;
    data['state'] = state;
    data['city'] = city;
    return data;
  }
}
