// To parse this JSON data, do
//
//     final organizedTripsFilteredModel = organizedTripsFilteredModelFromJson(jsonString);

import 'dart:convert';

OrganizedTripsFilteredModel organizedTripsFilteredModelFromJson(String str) => OrganizedTripsFilteredModel.fromJson(json.decode(str));

String organizedTripsFilteredModelToJson(OrganizedTripsFilteredModel data) => json.encode(data.toJson());

class OrganizedTripsFilteredModel {
  List<Trip> pastTrips;
  List<Trip> currentTrips;
  List<Trip> upcomingTrips;

  OrganizedTripsFilteredModel({
    required this.pastTrips,
    required this.currentTrips,
    required this.upcomingTrips,
  });

  factory OrganizedTripsFilteredModel.fromJson(Map<String, dynamic> json) => OrganizedTripsFilteredModel(
    pastTrips: List<Trip>.from(json["pastTrips"].map((x) => Trip.fromJson(x))),
    currentTrips: List<Trip>.from(json["currentTrips"].map((x) => Trip.fromJson(x))),
    upcomingTrips: List<Trip>.from(json["upcomingTrips"].map((x) => Trip.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "pastTrips": List<dynamic>.from(pastTrips.map((x) => x.toJson())),
    "currentTrips": List<dynamic>.from(currentTrips.map((x) => x.toJson())),
    "upcomingTrips": List<dynamic>.from(upcomingTrips.map((x) => x.toJson())),
  };
}

class Trip {
  int id;
  String organizer;
  dynamic cost;
  DateTime strDate;
  DateTime endDate;
  int totalCapacity;
  String img;
  String strLocation;
  String touristArea;
  String hotel;
  String restaurant;
  String transporter;

  Trip({
    required this.id,
    required this.organizer,
    required this.cost,
    required this.strDate,
    required this.endDate,
    required this.totalCapacity,
    required this.img,
    required this.strLocation,
    required this.touristArea,
    required this.hotel,
    required this.restaurant,
    required this.transporter,
  });

  factory Trip.fromJson(Map<String, dynamic> json) => Trip(
    id: json["id"],
    organizer: json["organizer"],
    cost: json["cost"],
    strDate: DateTime.parse(json["strDate"]),
    endDate: DateTime.parse(json["endDate"]),
    totalCapacity: json["totalCapacity"],
    img: json["img"],
    strLocation: json["strLocation"],
    touristArea: json["touristArea"],
    hotel: json["hotel"],
    restaurant: json["restaurant"],
    transporter: json["transporter"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "organizer": organizer,
    "cost": cost,
    "strDate": "${strDate.year.toString().padLeft(4, '0')}-${strDate.month.toString().padLeft(2, '0')}-${strDate.day.toString().padLeft(2, '0')}",
    "endDate": "${endDate.year.toString().padLeft(4, '0')}-${endDate.month.toString().padLeft(2, '0')}-${endDate.day.toString().padLeft(2, '0')}",
    "totalCapacity": totalCapacity,
    "img": img,
    "strLocation": strLocation,
    "touristArea": touristArea,
    "hotel": hotel,
    "restaurant": restaurant,
    "transporter": transporter,
  };
}
