class TransportModel {
  List<Transporters>? transporters;

  TransportModel({this.transporters});

  TransportModel.fromJson(Map<String, dynamic> json) {
    if (json['transporters'] != null) {
      transporters = <Transporters>[];
      json['transporters'].forEach((v) {
        transporters!.add(new Transporters.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.transporters != null) {
      data['transporters'] = this.transporters!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Transporters {
  int? id;
  String? name;
  String? type;
  String? img;

  Transporters({this.id, this.name, this.type, this.img});

  Transporters.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    type = json['type'];
    img = json['img'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['type'] = this.type;
    data['img'] = this.img;
    return data;
  }
}
