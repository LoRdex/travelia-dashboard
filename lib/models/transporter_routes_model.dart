class TransporterRoutesModel {
  List<Routes>? routes;

  TransporterRoutesModel({this.routes});

  TransporterRoutesModel.fromJson(Map<String, dynamic> json) {
    if (json['routes'] != null) {
      routes = <Routes>[];
      json['routes'].forEach((v) {
        routes!.add(new Routes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.routes != null) {
      data['routes'] = this.routes!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Routes {
  int? id;
  String? strAddress;
  String? strCountry;
  String? strCity;
  String? strState;
  String? endAddress;
  String? endCountry;
  String? endCity;
  String? endState;
  String? transportation;
  String? dateTime;
  int? totalCapacity;
  int? availableCapacity;
  dynamic cost;
  double? distance;

  Routes(
      {this.id,
        this.strAddress,
        this.strCountry,
        this.strCity,
        this.strState,
        this.endAddress,
        this.endCountry,
        this.endCity,
        this.endState,
        this.transportation,
        this.dateTime,
        this.totalCapacity,
        this.availableCapacity,
        this.cost,
        this.distance});

  Routes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    strAddress = json['strAddress'];
    strCountry = json['strCountry'];
    strCity = json['strCity'];
    strState = json['strState'];
    endAddress = json['endAddress'];
    endCountry = json['endCountry'];
    endCity = json['endCity'];
    endState = json['endState'];
    transportation = json['transportation'];
    dateTime = json['dateTime'];
    totalCapacity = json['totalCapacity'];
    availableCapacity = json['availableCapacity'];
    cost = json['cost'];
    distance = json['distance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['strAddress'] = this.strAddress;
    data['strCountry'] = this.strCountry;
    data['strCity'] = this.strCity;
    data['strState'] = this.strState;
    data['endAddress'] = this.endAddress;
    data['endCountry'] = this.endCountry;
    data['endCity'] = this.endCity;
    data['endState'] = this.endState;
    data['transportation'] = this.transportation;
    data['dateTime'] = this.dateTime;
    data['totalCapacity'] = this.totalCapacity;
    data['availableCapacity'] = this.availableCapacity;
    data['cost'] = this.cost;
    data['distance'] = this.distance;
    return data;
  }
}
