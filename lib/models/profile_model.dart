class ProfileModel {
  int? id;
  String? name;
  String? role;
  String? email;
  String? facilityName;
  String? country;
  dynamic wallet;
  String? photo;
  String? facilityPhoto;
  String? type;

  ProfileModel(
      {this.id,
        this.name,
        this.role,
        this.email,
        this.facilityName,
        this.country,
        this.wallet,
        this.photo,
        this.facilityPhoto,
        this.type});

  ProfileModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    role = json['role'];
    email = json['email'];
    facilityName = json['facilityName'];
    country = json['country'];
    wallet = json['wallet'];
    photo = json['photo'];
    facilityPhoto = json['facilityPhoto'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['role'] = this.role;
    data['email'] = this.email;
    data['facilityName'] = this.facilityName;
    data['country'] = this.country;
    data['wallet'] = this.wallet;
    data['photo'] = this.photo;
    data['facilityPhoto'] = this.facilityPhoto;
    data['type'] = this.type;
    return data;
  }
}
