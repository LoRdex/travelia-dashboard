class HotelsModel {
  List<Near>? near;

  HotelsModel({this.near});

  HotelsModel.fromJson(Map<String, dynamic> json) {
    if (json['near'] != null) {
      near = <Near>[];
      json['near'].forEach((v) {
        near!.add(new Near.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.near != null) {
      data['near'] = this.near!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Near {
  int? id;
  int? facilityId;
  String? type;
  Facility? facility;

  Near({this.id, this.facilityId, this.type, this.facility});

  Near.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    facilityId = json['facility_id'];
    type = json['type'];
    facility = json['facility'] != null
        ? new Facility.fromJson(json['facility'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['facility_id'] = this.facilityId;
    data['type'] = this.type;
    if (this.facility != null) {
      data['facility'] = this.facility!.toJson();
    }
    return data;
  }
}

class Facility {
  int? id;
  String? name;
  String? description;
  String? img;
  int? locationId;
  int? userId;
  Location? location;

  Facility(
      {this.id,
        this.name,
        this.description,
        this.img,
        this.locationId,
        this.userId,
        this.location});

  Facility.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    img = json['img'];
    locationId = json['location_id'];
    userId = json['user_id'];
    location = json['location'] != null
        ? new Location.fromJson(json['location'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['img'] = this.img;
    data['location_id'] = this.locationId;
    data['user_id'] = this.userId;
    if (this.location != null) {
      data['location'] = this.location!.toJson();
    }
    return data;
  }
}

class Location {
  int? id;
  String? latitude;
  String? longitude;
  String? address;
  String? country;
  String? state;
  String? city;

  Location(
      {this.id,
        this.latitude,
        this.longitude,
        this.address,
        this.country,
        this.state,
        this.city});

  Location.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    address = json['address'];
    country = json['country'];
    state = json['state'];
    city = json['city'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['address'] = this.address;
    data['country'] = this.country;
    data['state'] = this.state;
    data['city'] = this.city;
    return data;
  }
}
