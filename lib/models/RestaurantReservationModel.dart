class RestaurantReservationModel {
  List<Reservations>? reservations;

  RestaurantReservationModel({this.reservations});

  RestaurantReservationModel.fromJson(Map<String, dynamic> json) {
    if (json['reservations'] != null) {
      reservations = <Reservations>[];
      json['reservations'].forEach((v) {
        reservations!.add(new Reservations.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.reservations != null) {
      data['reservations'] = this.reservations!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Reservations {
  int? id;
  String? table;
  dynamic cost;
  String? date;
  String? hour;
  String? name;
  String? email;
  String? phone;
  int? age;
  String? address;
  String? photo;

  Reservations(
      {this.id,
        this.table,
        this.cost,
        this.date,
        this.hour,
        this.name,
        this.email,
        this.phone,
        this.age,
        this.address,
        this.photo});

  Reservations.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    table = json['table'];
    cost = json['cost'];
    date = json['date'];
    hour = json['hour'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    age = json['age'];
    address = json['address'];
    photo = json['photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['table'] = this.table;
    data['cost'] = this.cost;
    data['date'] = this.date;
    data['hour'] = this.hour;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['age'] = this.age;
    data['address'] = this.address;
    data['photo'] = this.photo;
    return data;
  }
}
