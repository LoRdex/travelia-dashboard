class AvailableVehiclesModel {
  int? v1;
  int? availableV1;
  int? v2;
  int? availableV2;
  int? v3;
  int? availableV3;

  AvailableVehiclesModel(
      {this.v1,
        this.availableV1,
        this.v2,
        this.availableV2,
        this.v3,
        this.availableV3});

  AvailableVehiclesModel.fromJson(Map<String, dynamic> json) {
    v1 = json['v1'];
    availableV1 = json['availableV1'];
    v2 = json['v2'];
    availableV2 = json['availableV2'];
    v3 = json['v3'];
    availableV3 = json['availableV3'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['v1'] = this.v1;
    data['availableV1'] = this.availableV1;
    data['v2'] = this.v2;
    data['availableV2'] = this.availableV2;
    data['v3'] = this.v3;
    data['availableV3'] = this.availableV3;
    return data;
  }
}
