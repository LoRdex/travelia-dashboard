class AccountsConfirmModel {
  List<Requierments>? requierments;

  AccountsConfirmModel({this.requierments});

  AccountsConfirmModel.fromJson(Map<String, dynamic> json) {
    if (json['requierments'] != null) {
      requierments = <Requierments>[];
      json['requierments'].forEach((v) {
        requierments!.add(new Requierments.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.requierments != null) {
      data['requierments'] = this.requierments!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Requierments {
  int? id;
  int? userId;
  String? note;
  String? status;
  User? user;

  Requierments({this.id, this.userId, this.note, this.status, this.user});

  Requierments.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    note = json['note'];
    status = json['status'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['note'] = this.note;
    data['status'] = this.status;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class User {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? photo;
  int? roleId;
  dynamic wallet;
  String? type;
  String? confirmation;

  User(
      {this.id,
        this.firstName,
        this.lastName,
        this.email,
        this.photo,
        this.roleId,
        this.wallet,
        this.type,
        this.confirmation,
       });

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    email = json['email'];
    photo = json['photo'];
    roleId = json['role_id'];
    wallet = json['wallet'];
    type = json['type'];
    confirmation = json['confirmation'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['email'] = this.email;
    data['photo'] = this.photo;
    data['role_id'] = this.roleId;
    data['wallet'] = this.wallet;
    data['type'] = this.type;
    data['confirmation'] = this.confirmation;
    return data;
  }
}
