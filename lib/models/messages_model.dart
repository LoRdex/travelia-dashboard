class MessagesModel {
  List<Messages>? messages;

  MessagesModel({this.messages});

  MessagesModel.fromJson(Map<String, dynamic> json) {
    if (json['Messages'] != null) {
      messages = <Messages>[];
      json['Messages'].forEach((v) {
        messages!.add(new Messages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.messages != null) {
      data['Messages'] = this.messages!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Messages {
  int? id;
  String? from;
  String? name;
  String? photo;
  String? title;
  String? msg;
  String? date;

  Messages(
      {this.id,
        this.from,
        this.name,
        this.photo,
        this.title,
        this.msg,
        this.date});

  Messages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    from = json['from'];
    name = json['name'];
    photo = json['photo'];
    title = json['title'];
    msg = json['msg'];
    date = json['Date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['from'] = this.from;
    data['name'] = this.name;
    data['photo'] = this.photo;
    data['title'] = this.title;
    data['msg'] = this.msg;
    data['Date'] = this.date;
    return data;
  }
}
