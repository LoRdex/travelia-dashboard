class OrganizedTripsModel {
  List<Trips>? trips;

  OrganizedTripsModel({this.trips});

  OrganizedTripsModel.fromJson(Map<String, dynamic> json) {
    if (json['trips'] != null) {
      trips = <Trips>[];
      json['trips'].forEach((v) {
        trips!.add(new Trips.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.trips != null) {
      data['trips'] = this.trips!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Trips {
  int? id;
  String? organizer;
  dynamic cost;
  String? strDate;
  String? endDate;
  int? totalCapacity;
  String? img;
  String? strLocation;
  String? touristArea;
  String? hotel;
  String? restaurant;
  String? transporter;
  int? availablePlaces;

  Trips(
      {this.id,
        this.organizer,
        this.cost,
        this.strDate,
        this.endDate,
        this.totalCapacity,
        this.img,
        this.strLocation,
        this.touristArea,
        this.hotel,
        this.restaurant,
        this.transporter,
        this.availablePlaces});

  Trips.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    organizer = json['organizer'];
    cost = json['cost'];
    strDate = json['strDate'];
    endDate = json['endDate'];
    totalCapacity = json['totalCapacity'];
    img = json['img'];
    strLocation = json['strLocation'];
    touristArea = json['touristArea'];
    hotel = json['hotel'];
    restaurant = json['restaurant'];
    transporter = json['transporter'];
    availablePlaces = json['availablePlaces'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['organizer'] = this.organizer;
    data['cost'] = this.cost;
    data['strDate'] = this.strDate;
    data['endDate'] = this.endDate;
    data['totalCapacity'] = this.totalCapacity;
    data['img'] = this.img;
    data['strLocation'] = this.strLocation;
    data['touristArea'] = this.touristArea;
    data['hotel'] = this.hotel;
    data['restaurant'] = this.restaurant;
    data['transporter'] = this.transporter;
    data['availablePlaces'] = this.availablePlaces;
    return data;
  }
}
