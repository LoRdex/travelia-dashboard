class AvailableRoomsModel {
  int? roomForOnePerson;
  int? availableRoomForOnePerson;
  int? roomForTwoPerson;
  int? availableRoomForTwoPerson;
  int? suite;
  int? availableSuite;

  AvailableRoomsModel(
      {this.roomForOnePerson,
        this.availableRoomForOnePerson,
        this.roomForTwoPerson,
        this.availableRoomForTwoPerson,
        this.suite,
        this.availableSuite});

  AvailableRoomsModel.fromJson(Map<String, dynamic> json) {
    roomForOnePerson = json['roomForOnePerson'];
    availableRoomForOnePerson = json['availableRoomForOnePerson'];
    roomForTwoPerson = json['roomForTwoPerson'];
    availableRoomForTwoPerson = json['availableRoomForTwoPerson'];
    suite = json['suite'];
    availableSuite = json['availableSuite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['roomForOnePerson'] = this.roomForOnePerson;
    data['availableRoomForOnePerson'] = this.availableRoomForOnePerson;
    data['roomForTwoPerson'] = this.roomForTwoPerson;
    data['availableRoomForTwoPerson'] = this.availableRoomForTwoPerson;
    data['suite'] = this.suite;
    data['availableSuite'] = this.availableSuite;
    return data;
  }
}
