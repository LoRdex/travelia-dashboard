class LoginModel {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? phone;
  int? age;
  String? address;
  dynamic wallet;
  String? confirmation;
  String? photo;
  int? roleId;
  String? token;
  String? roleName;
  String? type;

  LoginModel(
      this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.phone,
      this.age,
      this.address,
      this.wallet,
      this.confirmation,
      this.photo,
      this.roleId,
      this.token,
      this.roleName,
      this.type);

  factory LoginModel.fromJson(dynamic data) {
    if (data == null ||
        !data.containsKey('data') ||
        data['data']['user'] == null) {
      throw Exception('Invalid data');
    }

    var userData = data['data']['user'];
    return LoginModel(
        userData['id'],
        userData['firstName'],
        userData['lastName'],
        userData['email'],
        userData['phone'],
        userData['age'],
        userData['address'],
        userData['wallet']?.toDouble(),
        userData['confirmation'],
        userData['photo'],
        userData['role_id'],
        data['data']['access_token'],
        data['data']['role']['name'],
        userData['type']);
  }

  @override
  String toString() {
    // TODO: implement toString
    return 'email = $email  ,  phone = $phone , name = $firstName , login token = $token';
  }
}
