class RestaurantsModel {
  List<Near>? near;

  RestaurantsModel({this.near});

  RestaurantsModel.fromJson(Map<String, dynamic> json) {
    if (json['near'] != null) {
      near = <Near>[];
      json['near'].forEach((v) {
        near!.add(Near.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (near != null) {
      data['near'] = near!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Near {
  int? id;
  int? facilityId;
  String? type;
  Facility? facility;

  Near({this.id, this.facilityId, this.type, this.facility});

  Near.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    facilityId = json['facility_id'];
    type = json['type'];
    facility = json['facility'] != null
        ? Facility.fromJson(json['facility'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['facility_id'] = facilityId;
    data['type'] = type;
    if (facility != null) {
      data['facility'] = facility!.toJson();
    }
    return data;
  }
}

class Facility {
  int? id;
  String? name;
  String? description;
  String? img;
  int? locationId;
  int? userId;
  Location? location;

  Facility(
      {this.id,
        this.name,
        this.description,
        this.img,
        this.locationId,
        this.userId,
        this.location});

  Facility.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    img = json['img'];
    locationId = json['location_id'];
    userId = json['user_id'];
    location = json['location'] != null
        ? Location.fromJson(json['location'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['description'] = description;
    data['img'] = img;
    data['location_id'] = locationId;
    data['user_id'] = userId;
    if (location != null) {
      data['location'] = location!.toJson();
    }
    return data;
  }
}

class Location {
  int? id;
  String? latitude;
  String? longitude;
  String? address;
  String? country;
  String? state;
  String? city;

  Location(
      {this.id,
        this.latitude,
        this.longitude,
        this.address,
        this.country,
        this.state,
        this.city});

  Location.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    address = json['address'];
    country = json['country'];
    state = json['state'];
    city = json['city'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    data['address'] = address;
    data['country'] = country;
    data['state'] = state;
    data['city'] = city;
    return data;
  }
}
