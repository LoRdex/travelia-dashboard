class FinancialModel {
  int? totalBalance;
  List<Report>? report;

  FinancialModel({this.totalBalance, this.report});

  FinancialModel.fromJson(Map<String, dynamic> json) {
    totalBalance = json['TotalBalance'];
    if (json['Report'] != null) {
      report = <Report>[];
      json['Report'].forEach((v) {
        report!.add(Report.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['TotalBalance'] = totalBalance;
    if (report != null) {
      data['Report'] = report!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Report {
  int? id;
  String? description;
  String? name;
  String? from;
  int? intake;
  int? before;
  int? after;
  String? date;

  Report(
      {this.id,
        this.description,
        this.name,
        this.from,
        this.intake,
        this.before,
        this.after,
        this.date});

  Report.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    description = json['Description'];
    name = json['name'];
    from = json['from'];
    intake = json['Intake'];
    before = json['before'];
    after = json['after'];
    date = json['Date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['Description'] = description;
    data['name'] = name;
    data['from'] = from;
    data['Intake'] = intake;
    data['before'] = before;
    data['after'] = after;
    data['Date'] = date;
    return data;
  }
}
