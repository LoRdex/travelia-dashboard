import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travelia_dashboard/blocObserver.dart';
import 'package:travelia_dashboard/constants/cache_service.dart';
import 'package:travelia_dashboard/constants/my_colors.dart';
import 'package:travelia_dashboard/cubits/admin_cubit/admin_cubit.dart';
import 'package:travelia_dashboard/cubits/auth_cubit/auth_cubit.dart';
import 'package:travelia_dashboard/cubits/facility_create_cubits/step_cubit.dart';
import 'package:travelia_dashboard/cubits/hotel_cubit/hotel_cubit.dart';
import 'package:travelia_dashboard/cubits/main_cubit/main_cubit.dart';
import 'package:travelia_dashboard/cubits/restaurant_cubit/restaurant_cubit.dart';
import 'package:travelia_dashboard/cubits/transporter_cubit/transporter_cubit.dart';
import 'package:travelia_dashboard/cubits/trip_organizer_cubit/trip_organizer_cubit.dart';
import 'package:travelia_dashboard/views/admin/create_tourist_area.dart';
import 'package:travelia_dashboard/views/admin/send_notification.dart';
import 'package:travelia_dashboard/views/auth/ip_port_screen.dart';
import 'package:travelia_dashboard/views/auth/login_screen.dart';
import 'package:travelia_dashboard/views/home/admin_home_screen.dart';
import 'package:travelia_dashboard/views/home/organizer_home_screen.dart';
import 'package:travelia_dashboard/views/hotel/available_rooms.dart';
import 'package:travelia_dashboard/views/transporter/choose_vehicle.dart';
import 'package:travelia_dashboard/views/transporter/choose_vehicle_air.dart';
import 'package:travelia_dashboard/views/transporter/create_route.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/reservation_details.dart';
import 'package:travelia_dashboard/views/trip%20orgnaizer/trips_history.dart';
import 'cubits/facility_create_cubits/facility_cubit.dart';


void main() async {
  await CacheService.init();
  Bloc.observer = SimpleBlocObserver();
  runApp(const TraveliaApp());

}

class TraveliaApp extends StatelessWidget {
  const TraveliaApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => FacilityCubit(),),
        BlocProvider(create: (context) => StepCubit(),),
        BlocProvider(create: (context) => AuthCubit()), BlocProvider(create: (context) => TripOrganizerCubit(),),
        BlocProvider(create: (context) => RestaurantCubit(),),
        BlocProvider(create: (context) => MainCubit(),),
        BlocProvider(create: (context) => HotelCubit(),),
        BlocProvider(create: (context) => TransporterCubit(),),
        BlocProvider(create: (context) => AdminCubit(),),
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(
                seedColor: MyColors.blue),
            useMaterial3: true,
            fontFamily: 'Poppins',
          ),
          home:  const IpPortScreen())
    );
  }
}
