import 'package:flutter/material.dart';

class MyColors {
  static const Color lightPink = Color(0xffEBC7E6);
  static const Color lightPurple = Color(0xffBFACE2);
  static const Color purple2 = Color(0xffA084DC);
  static const Color midpurple = Color(0xff745eab);
  static const Color purple = Color(0xff6750a4);
  static const Color blue = Color(0xff0278ae);
  static const Color blue2 = Color(0xff038ccc);
  static const Color blueCyan = Color(0xff00cbe1);
  static const Color scaffoldColor = Color(0xffe6e6e6);



}