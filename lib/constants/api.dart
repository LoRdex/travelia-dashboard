import 'package:dio/dio.dart';

class Api{
  static String ip ='127.0.0.1';
  static String port ='8000';

  static String baseUrl ='http://$ip:$port/api';
  static String imgUrl ='http://$ip:$port';
  static var dio = Dio();

}